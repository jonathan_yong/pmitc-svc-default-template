/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package approute

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/httperror"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/pclog"
)

const (
	EndpointsUrl = "/_pmitc/endpoints"
)

var router *AppRouter

func endpoints(r *AppRouter) {
	router = r
	r.SimpleGet(EndpointsUrl).Public().SimpleHandlerFunc(GetEndpoints)
}

func GetEndpoints(ctx context.Context, w http.ResponseWriter, _ *http.Request) {
	logger := pclog.GetRequestLogger(ctx)
	var eps []Endpoint
	for _, r := range router.requestRoutes {
		secure := r.GetValidator().IsSecure()
		if r.IsPublic() {
			secure = false
		}

		ep := Endpoint{
			IsPublic:     r.IsPublic(),
			Methods:      r.GetMethods(),
			PathTemplate: r.GetPathTemplate(),
			PathRegexp:   r.GetPathRegexp(),
			Validator:    fmt.Sprintf("%T", r.GetValidator()),
			IsSecure:     secure,
		}
		eps = append(eps, ep)
	}

	response, err := json.Marshal(&EndpointsResponse{IsGateway: router.IsGateway(), RegisteredEndpoints: eps})
	if err != nil {
		httperror.WriteServerError(logger, w, fmt.Sprintf("Failed to parse endpoints response: %v", err))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	_, _ = w.Write(response)
}

type Endpoint struct {
	IsPublic     bool
	Methods      []string
	PathTemplate string
	PathRegexp   string
	Validator    string
	IsSecure     bool
}

type EndpointsResponse struct {
	IsGateway           bool `json:"IsGateway,omitempty"`
	RegisteredEndpoints []Endpoint
}
