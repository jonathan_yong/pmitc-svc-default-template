/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package approute

import (
	"context"
	"net/http"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/serviceproperties"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/pmitccontext"
)

type SimpleRoute struct {
}

func NewSimpleRoute() *SimpleRoute {
	sr := &SimpleRoute{}
	return sr
}

func (sr *SimpleRoute) PrepareRequestRouteSpecificContent(ctx context.Context, r *http.Request) (context.Context, error) {
	ctx = pmitccontext.FromRequest(ctx, r)
	return ctx, nil
}

func (sr *SimpleRoute) GetAudience() []string {
	return []string{serviceproperties.GetFromEnv().ServiceName}
}
