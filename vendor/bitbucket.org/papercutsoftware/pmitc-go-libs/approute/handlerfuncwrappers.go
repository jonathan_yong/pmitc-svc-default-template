/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package approute

import (
	"net/http"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/httperror"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/pclog"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/pmitccontext"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/pubsubutil"
	"golang.org/x/net/context"
)

func (rb *RouteBuilder) HandlerFunc(handlerFunc func(context.Context, string, http.ResponseWriter, *http.Request)) {
	httpHandlerFunc := func(w http.ResponseWriter, r *http.Request) {
		ctx, err, statusCode := prepareRequest(rb, r, w)
		logger := pclog.GetRequestLogger(ctx)
		defer logger.Close()
		if err != nil {
			logger.Errorf("Error preparing request: %v", err)
			if statusCode == http.StatusForbidden {
				http.Error(w, http.StatusText(http.StatusForbidden), http.StatusForbidden)
				return
			}
			http.Error(w, err.Error(), statusCode)
			return
		}

		orgId := pmitccontext.GetOrgId(ctx)
		if orgId == "" {
			http.Error(w, "no orgId in context. Are you sure this is an /{orgId}/ route? ", http.StatusInternalServerError)
			return
		}
		handlerFunc(ctx, orgId, w, r)
	}

	rb.route.HandlerFunc(httpHandlerFunc)
}

func (rb *RouteBuilder) SimpleHandlerFunc(
	handlerFunc func(context.Context, http.ResponseWriter, *http.Request),
) {
	httpHandlerFunc := func(w http.ResponseWriter, r *http.Request) {
		ctx, err, statusCode := prepareRequest(rb, r, w)
		logger := pclog.GetRequestLogger(ctx)
		defer logger.Close()
		if err != nil {
			logger.Errorf("Error preparing request: %v", err)
			if statusCode == http.StatusForbidden {
				http.Error(w, http.StatusText(http.StatusForbidden), http.StatusForbidden)
				return
			}
			http.Error(w, err.Error(), statusCode)
			return
		}

		handlerFunc(ctx, w, r)
	}
	rb.route.HandlerFunc(httpHandlerFunc)
}

func (rb *RouteBuilder) PubsubHandlerFunc(
	handlerFunc func(context.Context, pubsubutil.PubsubMessage) (recoverable bool, err error),
) {

	httpHandlerFunc := func(w http.ResponseWriter, r *http.Request) {

		ctx, err, statusCode := prepareRequest(rb, r, w)
		logger := pclog.GetRequestLogger(ctx)
		defer logger.Close()
		if err != nil {
			if statusCode == http.StatusForbidden {
				logger.Errorf("Not authenticated: %v", err)
				return
			}
			logger.Errorf("%v", err)
			return
		}

		pubSubMsg := ctx.Value(pubsubMessage).(pubsubutil.PubsubMessage)
		if recoverable, err := handlerFunc(ctx, pubSubMsg); err != nil {
			if recoverable {
				httperror.WriteServerError(logger, w, err.Error())
				return
			}
			logger.Errorf(err.Error())
		}
	}
	rb.route.HandlerFunc(httpHandlerFunc)
}
