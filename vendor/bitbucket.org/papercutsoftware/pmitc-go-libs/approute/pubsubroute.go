/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package approute

import (
	"context"
	"fmt"
	"net/http"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/authentication"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/pmitccontext"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/pubsubutil"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/serviceproperties"
)

const (
	pubsubAudience = "pubsub"
	pubsubMessage  = "PMITC-PUBSUB-MESSAGE"
)

type PubsubRoute struct {
}

func NewPubsubRoute() *PubsubRoute {
	return &PubsubRoute{}
}

func (pr *PubsubRoute) PrepareRequestRouteSpecificContent(ctx context.Context, r *http.Request) (context.Context, error) {
	pubSubMsg, err := pubsubutil.UnMarshal(r)
	if err != nil {
		return ctx, fmt.Errorf("could not unmarshal message for request, are you sure this is a pubsub route?: %v", err)
	}
	ctx = pmitccontext.FromPubsubAttributes(ctx, pubSubMsg.Attributes())
	authentication.SetBearer(r, pmitccontext.GetValues(ctx).GetAuthToken())
	ctx = context.WithValue(ctx, pubsubMessage, pubSubMsg)
	return ctx, nil
}

func (pr *PubsubRoute) GetAudience() []string {
	return []string{serviceproperties.GetFromEnv().ServiceName, pubsubAudience}
}
