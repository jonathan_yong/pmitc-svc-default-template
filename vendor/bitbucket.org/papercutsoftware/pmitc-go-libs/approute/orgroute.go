/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package approute

import (
	"context"
	"fmt"
	"net/http"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/pmitccontext"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/serviceproperties"
	"github.com/gorilla/mux"
)

type OrgRoute struct {
}

func NewOrgRoute() *OrgRoute {
	return &OrgRoute{}
}

func (og *OrgRoute) PrepareRequestRouteSpecificContent(ctx context.Context, r *http.Request) (context.Context, error) {
	vars := mux.Vars(r)
	orgId, found := vars["orgId"]
	if !found {
		return ctx, fmt.Errorf("url does not contain org id: " + r.URL.String())
	}

	ctx = pmitccontext.FromRequest(ctx, r)
	ctx = pmitccontext.SetOrgId(ctx, orgId)
	return ctx, nil
}

func (og *OrgRoute) GetAudience() []string {
	return []string{serviceproperties.GetFromEnv().ServiceName}
}
