/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package approute

import (
	"context"
	"net/http"
)

const (
	HealthCheckURL = "/_ah/health"
)

func health(r *AppRouter) {
	r.SimpleGet(HealthCheckURL).Public().SimpleHandlerFunc(healthCheck)
}

func healthCheck(_ context.Context, w http.ResponseWriter, _ *http.Request) {
	_, _ = w.Write([]byte("service is running OK"))
}
