/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package approute

import (
	"context"
	"net/http"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/serviceproperties"
)

const (
	warmupPath = "/_ah/warmup"
)

func warmup(ar *AppRouter) {
	ar.SimpleGet(warmupPath).Public().SimpleHandlerFunc(warmedUp)
}

func warmedUp(_ context.Context, w http.ResponseWriter, _ *http.Request) {
	_, _ = w.Write([]byte(serviceproperties.GetFromEnv().ServiceName + " has been warmed up"))
}
