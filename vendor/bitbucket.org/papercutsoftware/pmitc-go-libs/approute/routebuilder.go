/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package approute

import (
	"context"
	"net/http"
	"sync"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/authentication/keymanager"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/authentication/alwaysfailingvalidator"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/pclog/stdlogger"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/pmitccontext"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/uniqueid"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/serviceproperties"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/standardheaders"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/authentication"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/pclog"
	"github.com/gorilla/mux"
)

type RouteBuilder struct {
	validator           authentication.Validator
	validatorProvider   authentication.ValidatorProvider
	route               *mux.Route
	router              *AppRouter
	correlationIdSource string
	isPublic            bool
	initAuthOnlyOnce    sync.Once
	customiser          Customiser
}

func NewRouteBuilder(route *mux.Route, router *AppRouter, customiser Customiser) *RouteBuilder {
	builder := &RouteBuilder{route: route, router: router, customiser: customiser}
	router.AddRequestRoute(builder)
	return builder
}

func (rb *RouteBuilder) GetAudience() []string {
	return rb.customiser.GetAudience()
}

func (rb *RouteBuilder) PrepareRequestRouteSpecificContent(ctx context.Context, r *http.Request) (context.Context, error) {
	return rb.customiser.PrepareRequestRouteSpecificContent(ctx, r)
}

func (rb *RouteBuilder) Path(path string) {
	rb.route.Path(path)
}

func (rb *RouteBuilder) Methods(methods ...string) *RouteBuilder {
	rb.route.Methods(methods...)
	return rb
}

func (rb *RouteBuilder) Public() *RouteBuilder {
	rb.isPublic = true
	if rb.validatorProvider != nil {
		stdlogger.StdLogger.Supportf("AUTH VALIDATOR WILL BE IGNORED BECAUSE ROUTE IS PUBLIC")
	}
	return rb
}

func (rb *RouteBuilder) ShouldGenerateCorrelationId(r *http.Request) bool {
	return rb.correlationIdSource != "" && r.Header.Get(standardheaders.CorrelationId) == ""
}

func (rb *RouteBuilder) AutoGenerateCorrelationId(source string) *RouteBuilder {
	rb.correlationIdSource = source
	return rb
}

func (rb *RouteBuilder) WithCustomAuthValidator(provider authentication.ValidatorProvider) *RouteBuilder {
	rb.validatorProvider = provider
	if rb.isPublic {
		stdlogger.StdLogger.Supportf("AUTH VALIDATOR WILL BE IGNORED BECAUSE ROUTE IS PUBLIC")
	}
	return rb
}

func (rb *RouteBuilder) InitialiseValidator(ctx context.Context) {
	rb.initAuthOnlyOnce.Do(func() {
		logger := pclog.GetRequestLogger(ctx)
		if rb.validatorProvider != nil {
			s, err := keymanager.GetKeyList(ctx)
			if err != nil {
				logger.Errorf("Failed to get secret keys %v, using alwaysfailingvalidator instead", err)
				rb.validator = alwaysfailingvalidator.NewAlwaysFailingValidator()
			}
			v, err := rb.validatorProvider.NewValidator(serviceproperties.GetFromEnv(), s)
			if err != nil {
				logger.Errorf("Failed to initialise custom auth validator route %v, using alwaysfailingvalidator instead", err)
				rb.validator = alwaysfailingvalidator.NewAlwaysFailingValidator()
			} else {
				rb.validator = v
			}
		} else {
			rb.validator = rb.router.defaultValidator
		}

		pathTemplate, _ := rb.route.GetPathTemplate()
		logger.Printf("Route %s auth initialised with %T", pathTemplate, rb.validator)
		if rb.validatorProvider != nil && rb.isPublic {
			logger.Supportf("AUTH VALIDATOR WILL BE IGNORED BECAUSE ROUTE IS PUBLIC")
		}
	})
}

func (rb *RouteBuilder) IsPublic() bool {
	return rb.isPublic
}

func (rb *RouteBuilder) GetValidator() authentication.Validator {
	return rb.validator
}

func (rb *RouteBuilder) NewRequestContext(r *http.Request) context.Context {
	if rb.ShouldGenerateCorrelationId(r) {
		r.Header.Set(standardheaders.CorrelationId, uniqueid.CreateCorrelationId(rb.correlationIdSource))
	}
	newContext := rb.router.ctxProvider.NewContext(r)
	return pmitccontext.FromRequest(newContext, r)
}

func (rb *RouteBuilder) NewRequestLogger(ctx context.Context, w http.ResponseWriter, r *http.Request) (pclog.RequestLogger, error) {
	return rb.router.requestLoggerProvider.NewRequestLogger(ctx, w, r)
}

func (rb *RouteBuilder) GetRouter() *AppRouter {
	return rb.router
}

func (rb *RouteBuilder) GetMethods() []string {
	methods, _ := rb.route.GetMethods()
	return methods
}

func (rb *RouteBuilder) GetPathTemplate() string {
	path, _ := rb.route.GetPathTemplate()
	return path
}

func (rb *RouteBuilder) GetPathRegexp() string {
	path, _ := rb.route.GetPathRegexp()
	return path
}

func (rb *RouteBuilder) WithOrg() {

}

type Customiser interface {
	PrepareRequestRouteSpecificContent(ctx context.Context, r *http.Request) (context.Context, error)
	GetAudience() []string
}
