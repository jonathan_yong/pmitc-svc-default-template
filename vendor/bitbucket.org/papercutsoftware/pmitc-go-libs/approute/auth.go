/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package approute

import (
	"context"
	"net/http"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/authentication/keymanager"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/authentication"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/authentication/alwaysfailingvalidator"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/authentication/secretkeys"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/pclog"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/serviceproperties"
)

func initialiseAuth(ctx context.Context, route RequestRoute) context.Context {
	logger := pclog.GetRequestLogger(ctx)
	router := route.GetRouter()

	// INIT here
	router.initAuthOnlyOnce.Do(func() {
		logger.Printf("Initialising authentication")
		sp := serviceproperties.GetFromEnv()

		//create defaultValidator
		var validator authentication.Validator
		//get keys
		secretList, err := keymanager.GetKeyList(ctx)
		if err != nil {
			logger.Errorf("error fetching secrets, overwrite with 'alwaysfailing' defaultValidator instead:  %v", err)
			validator, _ = alwaysfailingvalidator.NewProvider().NewValidator(sp, secretList)
		} else {
			validator, err = router.defaultValidatorProvider.NewValidator(serviceproperties.GetFromEnv(), secretList)
			if err != nil {
				logger.Errorf("Failed to create defaultValidator, overwrite with 'alwaysfailing' defaultValidator instead: %v", err)
				validator, _ = alwaysfailingvalidator.NewProvider().NewValidator(sp, secretList)
			}
		}
		router.defaultValidator = validator

		//initialise validators on all routes for first request
		secretKeyListCopy := *secretkeys.CreateSecretKeyList(secretList.GetAllSecretKeys()) // Create frescopy
		for _, r := range router.requestRoutes {
			// nolint
			r.InitialiseValidator(secretkeys.SetOnContext(ctx, secretKeyListCopy))
		}

		logger.Printf("Auth initialised")
	})

	if !route.GetValidator().IsSecure() {
		logger.Supportf("WARNING: INSECURE AUTHENTICATION VALIDATOR IN USE: %T", route.GetValidator())
	}

	// For now populate ctx, but clients should not use secrets from context
	// legacy behaviour was to put empty keylist on error
	secretList, err := keymanager.GetKeyList(ctx)
	if err != nil {
		logger.Supportf("could not get secretList. err: %v", err)
		// Duplicate existing behaviour
		secretList = &secretkeys.SecretKeyList{}
	}
	//nolint
	ctx = secretkeys.SetOnContext(ctx, *secretkeys.CreateSecretKeyList(secretList.GetAllSecretKeys()))
	return ctx
}

func validateRequest(ctx context.Context, validator authentication.Validator, r *http.Request, audience []string) (context.Context, error) {
	logger := pclog.GetRequestLogger(ctx)
	ctx, err := validator.ValidateRequest(ctx, r, audience)
	if err != nil {
		if err == authentication.OldSecretKeyFound {
			logger.Supportf("%v", err)
		} else {
			return ctx, err
		}
	}
	return ctx, nil
}
