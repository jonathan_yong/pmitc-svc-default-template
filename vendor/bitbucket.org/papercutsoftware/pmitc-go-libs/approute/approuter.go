/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

//Secured GET, PUT, POST, DELETE methods check orgId in request headers, Simple doesn't
package approute

import (
	"context"
	"net/http"
	"sync"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/serviceproperties"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/standardheaders"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/authentication"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/pclog"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/pmitccontext"

	"github.com/gorilla/mux"
)

type AppRouter struct {
	router                   *mux.Router
	requestRoutes            []RequestRoute
	ctxProvider              pmitccontext.RequestContextProvider
	defaultValidatorProvider authentication.ValidatorProvider
	requestLoggerProvider    pclog.RequestLoggerProvider
	defaultValidator         authentication.Validator
	initAuthOnlyOnce         sync.Once
	isGateway                bool
}

func (ar *AppRouter) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ar.router.ServeHTTP(w, r)
}

func NewAppRouter(r *mux.Router,
	ctxProvider pmitccontext.RequestContextProvider,
	jwtValidatorProvider authentication.ValidatorProvider,
	requestLoggerProvider pclog.RequestLoggerProvider) *AppRouter {

	ar := &AppRouter{
		router:                   r,
		ctxProvider:              ctxProvider,
		requestLoggerProvider:    requestLoggerProvider,
		defaultValidatorProvider: jwtValidatorProvider,
	}

	warmup(ar)
	health(ar)
	endpoints(ar)

	return ar
}

func (ar *AppRouter) Gateway() *AppRouter {
	ar.isGateway = true
	return ar
}

func (ar *AppRouter) IsGateway() bool {
	return ar.isGateway
}

func (ar *AppRouter) Get(path string) *RouteBuilder {
	return ar.Create(path, http.MethodGet)
}

func (ar *AppRouter) Post(path string) *RouteBuilder {
	return ar.Create(path, http.MethodPost)
}

func (ar *AppRouter) Put(path string) *RouteBuilder {
	return ar.Create(path, http.MethodPut)
}

func (ar *AppRouter) Delete(path string) *RouteBuilder {
	return ar.Create(path, http.MethodDelete)
}

func (ar *AppRouter) Path(path string) *RouteBuilder {
	route := NewRouteBuilder(ar.router.Path(path), ar, NewOrgRoute())
	return route
}

func (ar *AppRouter) PathPrefix(prefix string) *RouteBuilder {
	route := NewRouteBuilder(ar.router.PathPrefix(prefix), ar, NewOrgRoute())
	return route
}

func (ar *AppRouter) SimpleGet(path string) *RouteBuilder {
	return ar.CreateSimple(path, http.MethodGet)
}

func (ar *AppRouter) SimplePost(path string) *RouteBuilder {
	return ar.CreateSimple(path, http.MethodPost)
}

func (ar *AppRouter) SimplePut(path string) *RouteBuilder {
	return ar.CreateSimple(path, http.MethodPut)
}

func (ar *AppRouter) SimpleDelete(path string) *RouteBuilder {
	return ar.CreateSimple(path, http.MethodDelete)
}

func (ar *AppRouter) SimplePath(path string) *RouteBuilder {
	route := NewRouteBuilder(ar.router.Path(path), ar, NewSimpleRoute())
	return route
}

func (ar *AppRouter) Push(path string) *RouteBuilder {
	route := NewRouteBuilder(ar.router.Path(path).Methods(http.MethodPost), ar, NewPubsubRoute())
	return route
}

func (ar *AppRouter) Create(path string, method ...string) *RouteBuilder {
	orgRoute := ar.Path(path)
	orgRoute.Methods(method...)
	return orgRoute
}
func (ar *AppRouter) CreateSimple(path string, method ...string) *RouteBuilder {
	return ar.SimplePath(path).Methods(method...)
}

func (ar *AppRouter) AddRequestRoute(sr RequestRoute) {
	ar.requestRoutes = append(ar.requestRoutes, sr)
}

func logRequestInfo(ctx context.Context, r *http.Request) {
	logger := pclog.GetRequestLogger(ctx)
	values := pmitccontext.GetValues(ctx)
	origin := values.GetRequestOrigin()
	if origin == "" {
		origin = r.UserAgent()
	}
	serviceName := serviceproperties.GetFromEnv().ServiceName
	logger.Printf("Inbound request ORIGIN(%s) SERVICE(%s) ASYNC(%t)", origin, serviceName, values.IsAsyncCall())
}

type RequestRoute interface {
	GetRouter() *AppRouter
	GetValidator() authentication.Validator
	NewRequestContext(r *http.Request) context.Context
	NewRequestLogger(ctx context.Context, w http.ResponseWriter, r *http.Request) (pclog.RequestLogger, error)
	IsPublic() bool
	GetAudience() []string
	PrepareRequestRouteSpecificContent(ctx context.Context, r *http.Request) (context.Context, error)
	InitialiseValidator(ctx context.Context)
	GetMethods() []string
	GetPathTemplate() string
	GetPathRegexp() string
}

//this is down for each request, putting useful stuff in the context and doing auth
func prepareRequest(rp RequestRoute, r *http.Request, w http.ResponseWriter) (ctx context.Context, err error, statusCode int) {
	ctx = rp.NewRequestContext(r)

	//set up logger early, because, logging is helpful
	logger, err := rp.NewRequestLogger(ctx, w, r)
	if err != nil {
		return ctx, err, http.StatusInternalServerError
	}
	//defer logger.Close() - we don't close it here, we only want to close it after request is handled
	ctx = pclog.WithRequestLogger(ctx, logger)

	//prepare request specific stuff
	ctx, err = rp.PrepareRequestRouteSpecificContent(ctx, r)

	//creates defaultValidator and puts secret keys on context
	ctx = initialiseAuth(ctx, rp)

	//By this point we should have everything we need: cid, logger, defaultValidator, keys
	cid := pmitccontext.GetValues(ctx).GetCorrelationId()
	if cid == "" && !rp.IsPublic() { //getting hard core here!
		logger.Errorf("no %s found in request header", standardheaders.CorrelationId)
	} else {
		logger.Printf("%s(%s)", standardheaders.CorrelationId, cid)
	}

	logRequestInfo(ctx, r)

	if !rp.IsPublic() {
		ctx, err = validateRequest(ctx, rp.GetValidator(), r, rp.GetAudience())
		if err != nil {
			logger.Errorf("Failed to validate token: %v", err)
			return ctx, err, http.StatusForbidden
		}
	} else {
		logger.Devf("this route is public")
	}

	return ctx, err, 200
}
