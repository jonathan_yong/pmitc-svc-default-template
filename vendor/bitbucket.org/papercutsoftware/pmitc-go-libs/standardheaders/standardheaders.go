/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package standardheaders

const CorrelationId = "X-PMITC-Correlation-ID"
const Async = "X-PMITC-Async"
const RequestOrigin = "X-PMITC-Request-Origin"
const AdminProfileId = "X-Admin-Id"
