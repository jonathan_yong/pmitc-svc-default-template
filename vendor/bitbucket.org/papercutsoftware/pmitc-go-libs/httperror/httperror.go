package httperror

import (
	"net/http"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/pclog"
)

// Don't use in new go1.11
func Respond(logger pclog.RequestLogger, w http.ResponseWriter, errMsg string, code int) {
	logger.Errorf(errMsg)
	http.Error(w, errMsg, code)
}

func WriteBadRequest(logger pclog.RequestLogger, w http.ResponseWriter, errMsg string) {
	logger.Errorf(errMsg)
	http.Error(w, errMsg, http.StatusBadRequest)
}

func WriteServerError(logger pclog.RequestLogger, w http.ResponseWriter, errMsg string) {
	logger.Errorf(errMsg)
	http.Error(w, errMsg, http.StatusInternalServerError)
}

func WriteNotFound(logger pclog.RequestLogger, w http.ResponseWriter, errMsg string) {
	logger.Errorf(errMsg)
	http.Error(w, errMsg, http.StatusNotFound)
}

func WriteForbidden(logger pclog.RequestLogger, w http.ResponseWriter, errMsg string) {
	logger.Errorf(errMsg)
	http.Error(w, errMsg, http.StatusForbidden)
}

func WriteUnauthorized(logger pclog.RequestLogger, w http.ResponseWriter, errMsg string) {
	logger.Errorf(errMsg)
	http.Error(w, errMsg, http.StatusUnauthorized)
}
