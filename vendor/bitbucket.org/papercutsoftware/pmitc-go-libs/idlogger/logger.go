package idlogger

import (
	"encoding/json"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/pclog"

	"golang.org/x/net/context"
)

// Better name
type CommonIds struct {
	CorrelationId string   `json:",omitempty"`
	OrgId         string   `json:",omitempty"`
	JobIds        []string `json:",omitempty"`
	PrinterId     string   `json:",omitempty"`
	CoordinatorId []string `json:",omitempty"`
	UserId        string   `json:",omitempty"`
	AdminId       string   `json:",omitempty"`
}

func LogIds(ctx context.Context, ids *CommonIds) {
	// FUTURE auto populate CorrelationId from pmitcctx
	logger := pclog.GetRequestLogger(ctx)
	bytes, e := json.Marshal(ids)
	if e != nil {
		logger.Devf("Could not marshal ids err: %+v", e)
	}
	logger.Devf("%s", string(bytes))
}
