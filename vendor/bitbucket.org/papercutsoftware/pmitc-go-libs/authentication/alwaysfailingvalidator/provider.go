/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package alwaysfailingvalidator

import (
	"bitbucket.org/papercutsoftware/pmitc-go-libs/authentication"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/authentication/secretkeys"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/serviceproperties"
)

type alwaysfailingValidatorProvider struct{}

func (a *alwaysfailingValidatorProvider) NewValidator(serviceProperties serviceproperties.ServiceProperties, secretKeyList *secretkeys.SecretKeyList) (authentication.Validator, error) {
	return NewAlwaysFailingValidator(), nil
}

func NewProvider() authentication.ValidatorProvider {
	return &alwaysfailingValidatorProvider{}
}
