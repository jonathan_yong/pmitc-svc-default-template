/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package alwaysfailingvalidator

import (
	"context"
	"fmt"
	"net/http"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/authentication"
)

type alwaysFailValidator struct {
}

func (v *alwaysFailValidator) ValidateRequest(ctx context.Context, r *http.Request, expectedAudience []string) (context.Context, error) {
	return ctx, fmt.Errorf("THIS VALIDATOR WILL ALWAYS FAIL TOKEN VALIDATION. check auth initialisation")
}

func (v *alwaysFailValidator) IsSecure() bool {
	return false
}

func NewAlwaysFailingValidator() authentication.Validator {
	return &alwaysFailValidator{}
}
