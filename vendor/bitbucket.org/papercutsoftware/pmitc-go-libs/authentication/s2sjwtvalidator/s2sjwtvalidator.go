/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package s2sjwtvalidator

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/pclog"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/pmitccontext"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/authentication/secretkeys"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/serviceproperties"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/authentication"
	"github.com/SermoDigital/jose/crypto"
	"github.com/SermoDigital/jose/jws"
	"github.com/SermoDigital/jose/jwt"

	pmitcjwt "bitbucket.org/papercutsoftware/pmitc-go-libs/jwt"
)

func NewJwtKeyValidator(keyList *secretkeys.SecretKeyList, serviceProperties serviceproperties.ServiceProperties, additionalAudiences []string) authentication.Validator {
	return &s2sJwtValidator{keyList: keyList, serviceProperties: serviceProperties, additionalAudiences: additionalAudiences}
}

type s2sJwtValidator struct {
	serviceProperties   serviceproperties.ServiceProperties
	keyList             *secretkeys.SecretKeyList
	additionalAudiences []string
}

func (d *s2sJwtValidator) ValidateRequest(ctx context.Context, r *http.Request, validAudiences []string) (context.Context, error) {

	allValidAud := append(validAudiences, d.additionalAudiences...)
	orgId := pmitccontext.GetOrgId(ctx)
	logger := pclog.GetRequestLogger(ctx)
	authToken := authentication.ExtractBearer(r)
	if authToken == "" {
		logger.Errorf("NO AUTH TOKEN")
	}

	j, err := jws.ParseJWT([]byte(authToken))
	if err != nil {
		return ctx, err
	}

	kind := j.Claims().Get("kind")
	if kind == nil || kind.(string) != pmitcjwt.KindService2Service {
		logger.Errorf("NOT A SERVICE 2 SERVICE JWT")
	}

	expectationValidator := d.createExpectationValidator(allValidAud, orgId)

	//validate the signature first
	primaryValidated := true
	otherValidated := false
	err = j.Validate(d.keyList.GetPrimarySecretKey().GetSecret(), crypto.SigningMethodHS256, expectationValidator)
	if err != nil && err != crypto.ErrSignatureInvalid {
		return ctx, err
	}

	if err != nil {
		primaryValidated = false
		for _, k := range d.keyList.GetAllSecretKeys() {
			err = j.Validate(k.GetSecret(), crypto.SigningMethodHS256, expectationValidator)
			if err != nil && err != crypto.ErrSignatureInvalid {
				return ctx, err
			}

			if err == nil {
				otherValidated = true
				break
			}
		}
	}

	if !primaryValidated && !otherValidated {
		return ctx, authentication.NoValidSecretKeyFound
	}

	//some claims we don't validate now but will in the future, this ensures that we build with the correct structure
	err = validateStructure(j.Claims())
	if err != nil {
		return ctx, err
	}

	//because the library audience validator doesn't do what we want
	if !validateAudience(j.Claims(), allValidAud) {
		jwtAudiences, _ := j.Claims().Audience()
		logger.Errorf("Jwt audiences %v not found in %v", jwtAudiences, allValidAud)
		return ctx, authentication.InvalidAudience
	}

	if primaryValidated {
		return ctx, nil
	} else {
		return ctx, authentication.OldSecretKeyFound
	}
}

func validateAudience(claims jwt.Claims, validAudiences []string) bool {
	//if any of the the claims additionalAudiences exist in our valid additionalAudiences
	givenAudiences, ok := claims.Audience()
	if !ok {
		return false
	}
	for _, aud := range givenAudiences {
		for _, validAud := range validAudiences {
			if aud == validAud {
				//found one!
				return true
			}
		}
	}
	return false
}

func (d *s2sJwtValidator) IsSecure() bool {
	return true
}

func (d *s2sJwtValidator) createExpectationValidator(expectedAudience []string, expectedSub string) *jwt.Validator {
	expectedClaims := jws.Claims{}
	expectedClaims["kind"] = pmitcjwt.KindService2Service
	if expectedSub != "" {
		expectedClaims["sub"] = expectedSub
	}
	//expectedClaims["aud"] = expectedAudience
	return jws.NewValidator(expectedClaims, 15*time.Second, 15*time.Second, nil)
}

func validateStructure(claims jwt.Claims) error {
	//very basic for now
	_, ok := claims.Issuer()
	if !ok {
		return fmt.Errorf("iss claim is missing")
	}

	_, ok = claims.Subject()
	if !ok {
		return fmt.Errorf("sub claim is missing")
	}

	_, ok = claims.Audience()
	if !ok {
		return fmt.Errorf("aud claim is missing")
	}

	_, ok = claims.Expiration()
	if !ok {
		return fmt.Errorf("exp claim is missing")
	}

	_, ok = claims.IssuedAt()
	if !ok {
		return fmt.Errorf("iat claim is missing")
	}

	return nil
}
