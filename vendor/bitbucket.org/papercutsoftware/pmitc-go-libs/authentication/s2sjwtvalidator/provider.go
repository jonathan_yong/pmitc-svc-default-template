/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package s2sjwtvalidator

import (
	"errors"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/authentication/secretkeys"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/serviceproperties"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/authentication"
)

type s2sJwtValidatorProvider struct {
	additionalAudiences []string
}

func (g *s2sJwtValidatorProvider) NewValidator(serviceProperties serviceproperties.ServiceProperties, secretKeyList *secretkeys.SecretKeyList) (authentication.Validator, error) {
	if secretKeyList == nil || secretKeyList.GetPrimarySecretKey() == nil {
		return nil, errors.New("cannot create validator without secrets")
	}

	validator := NewJwtKeyValidator(secretKeyList, serviceProperties, g.additionalAudiences)

	return validator, nil
}

func NewProvider() authentication.ValidatorProvider {
	return &s2sJwtValidatorProvider{additionalAudiences: []string{}}
}

func NewProviderWithAdditionalAudiences(additionalAudiences []string) authentication.ValidatorProvider {
	return &s2sJwtValidatorProvider{additionalAudiences: additionalAudiences}
}
