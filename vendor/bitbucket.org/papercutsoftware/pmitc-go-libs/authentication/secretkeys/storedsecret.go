/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package secretkeys

import (
	"context"
	"fmt"
	"io/ioutil"
	"strings"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/pclog"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/serviceproperties"

	cloudkms "cloud.google.com/go/kms/apiv1"
	"cloud.google.com/go/storage"
	"google.golang.org/api/iterator"
	"google.golang.org/api/option"
	kmspb "google.golang.org/genproto/googleapis/cloud/kms/v1"
)

const (
	tokenBucket             = "pc-pmitc-authentication"
	key                     = "service-2-service"
	keyRing                 = "pmitc-secret"
	tokenFilePrefix         = "pmitc-secret/service-2-service/"
	CloudPlatformScope      = "https://www.googleapis.com/auth/cloud-platform"
	keyResourceNameTemplate = "projects/%s/locations/global/keyRings/%s/cryptoKeys/%s"
)

type storedSecretKeySource struct{}

func NewSecretKeySource() SecretKeySource {
	return &storedSecretKeySource{}
}

//if in integration test or localhost return a keysource that returns a known test key
func GetTestAwareKeySource() SecretKeySource {
	sp := serviceproperties.GetFromEnv()
	// TODO: Detect running in unit test context
	if sp.IsLocalHost || sp.IsIntegrationTestRunner {
		return NewTestSecretKeySource()
	} else {
		return NewSecretKeySource()
	}
}

func (s *storedSecretKeySource) GetSecretKeys(ctx context.Context) (*SecretKeyList, error) {
	logger := pclog.GetRequestLogger(ctx)

	client, err := storage.NewClient(ctx, option.WithScopes(storage.ScopeReadOnly))
	if err != nil {
		return nil, err
	}

	query := &storage.Query{
		Prefix:   tokenFilePrefix,
		Versions: false,
	}
	tb := client.Bucket(tokenBucket)
	tokenFiles := tb.Objects(ctx, query)
	var tokens []*SecretKey

	for {
		attrs, err := tokenFiles.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, err
		}

		logger.Printf("Found token file %s", attrs.Name)

		version := strings.Replace(strings.Replace(attrs.Name, tokenFilePrefix, "", 1), ".key", "", 1)
		logger.Printf("Version %s", version)

		r, err := tb.Object(attrs.Name).NewReader(ctx)
		if err != nil {
			logger.Printf("Failed to create reader from %s, %v", attrs.Name, err)
			return nil, err
		}

		ciphertext, err := ioutil.ReadAll(r)
		if err != nil {
			logger.Printf("Failed to ReadAll from %s, %v", attrs.Name, err)
			return nil, err
		}

		plaintext, err := s.decryptSymmetric(ctx, keyRing, key, ciphertext)
		if err != nil {
			return nil, err
		}

		tokens = append(tokens, CreateSecretKey(plaintext, version, attrs.Created))
	}

	secretKeyList := CreateSecretKeyList(tokens)
	return secretKeyList, nil
}

func (s *storedSecretKeySource) decryptSymmetric(ctx context.Context, keyRing, key string, ciphertext []byte) ([]byte, error) {
	logger := pclog.GetRequestLogger(ctx)
	client, err := cloudkms.NewKeyManagementClient(ctx)
	if err != nil {
		logger.Printf("Failed to create kms client: %v", err)
		return nil, err
	}

	keyResource := fmt.Sprintf(keyResourceNameTemplate, serviceproperties.GetFromEnv().ProjectId, keyRing, key)
	// Build the request.
	req := &kmspb.DecryptRequest{
		Name:       keyResource,
		Ciphertext: ciphertext,
	}
	// Call the API.
	resp, err := client.Decrypt(ctx, req)
	if err != nil {
		logger.Printf("Failed to decrypt with keyResource: %s", keyResource)
		return nil, err
	}
	return resp.Plaintext, nil
}
