/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package secretkeys

import (
	"context"
	"time"
)

var TestSecretKey *SecretKey

func init() {
	TestSecretKey = CreateSecretKey([]byte("test service"), "test", time.Now())
}

type TestSecretKeySource struct{}

func NewTestSecretKeySource() SecretKeySource {
	return &TestSecretKeySource{}
}

func (s *TestSecretKeySource) GetSecretKeys(ctx context.Context) (*SecretKeyList, error) {
	list := CreateSecretKeyList([]*SecretKey{TestSecretKey})
	return list, nil
}
