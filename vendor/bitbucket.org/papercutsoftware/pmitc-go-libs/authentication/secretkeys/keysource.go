/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package secretkeys

import (
	"context"
)

type SecretKeySource interface {
	GetSecretKeys(ctx context.Context) (*SecretKeyList, error)
}
