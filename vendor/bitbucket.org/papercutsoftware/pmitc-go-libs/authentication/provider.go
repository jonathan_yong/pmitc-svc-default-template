/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package authentication

import (
	"bitbucket.org/papercutsoftware/pmitc-go-libs/authentication/secretkeys"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/serviceproperties"
)

type ValidatorProvider interface {
	NewValidator(serviceProperties serviceproperties.ServiceProperties, secretKeyList *secretkeys.SecretKeyList) (Validator, error)
}
