/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package jwt

import (
	"fmt"

	"time"

	"github.com/SermoDigital/jose/crypto"
	"github.com/SermoDigital/jose/jws"
	"github.com/SermoDigital/jose/jwt"
)

const (
	KindApi             = "api"
	KindService2Service = "service-2-service"
)

func ValidateJwt(jwtValue []byte, jwtKey []byte) (jwt.Claims, error) {
	j, err := jws.ParseJWT(jwtValue)
	if err != nil {
		return nil, fmt.Errorf("JWT token parse error: %v", err)
	}

	err = j.Validate(jwtKey, crypto.SigningMethodHS256)
	if err != nil {
		return nil, err
	}

	return j.Claims(), nil
}

func NewAPIJwt(
	jwtKey []byte,
	issuer string,
	subject string,
	audience string,
	expirationTime time.Time,
	issuedAtTime time.Time,
	additionalClaims map[string]string,
) ([]byte, error) {

	claims := jws.Claims{}
	claims.SetIssuer(issuer)
	claims.SetSubject(subject)
	claims.SetAudience(audience)
	claims.SetExpiration(expirationTime)
	claims.SetIssuedAt(issuedAtTime)
	claims.SetNotBefore(issuedAtTime)
	claims.Set("kind", KindApi)

	for k, v := range additionalClaims {
		claims.Set(k, v)
	}

	newJwt := jws.NewJWT(claims, crypto.SigningMethodHS256)
	newJwtToken, err := newJwt.Serialize(jwtKey)
	if err != nil {
		return nil, err
	}
	return newJwtToken, nil
}

func NewServiceJwt(key []byte,
	callingService string,
	targetService string,
	subject string,
	issuedAt time.Time,
	duration time.Duration) ([]byte, error) {

	claims := jws.Claims{}
	claims.SetIssuer(callingService)
	claims.SetSubject(subject) //this would typically be the orgid or other specific identifier being operated on
	claims.SetAudience(targetService)
	claims.SetExpiration(issuedAt.Add(duration))
	claims.SetIssuedAt(issuedAt)
	claims.SetNotBefore(issuedAt)
	claims.Set("kind", KindService2Service)

	newJwt := jws.NewJWT(claims, crypto.SigningMethodHS256)
	newJwtToken, err := newJwt.Serialize(key)
	if err != nil {
		return nil, err
	}
	return newJwtToken, nil
}
