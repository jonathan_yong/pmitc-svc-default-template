/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package pclog

import (
	"context"
	"net/http"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/pclog/stdlogger"
)

const (
	requestLogKey = "PMITC-REQUEST-LOGGER"
)

/*
	request loggers collect log items over a request and group them together,
    they typically need to be closed to flush logs collected
*/

type RequestLogger interface {
	Errorf(format string, args ...interface{})
	Supportf(format string, args ...interface{})
	Devf(format string, args ...interface{})
	Printf(format string, args ...interface{})
	Close() error
}

func WithRequestLogger(ctx context.Context, logger RequestLogger) context.Context {
	return context.WithValue(ctx, requestLogKey, logger)
}

func GetRequestLogger(ctx context.Context) RequestLogger {
	if ctx == nil {
		return createStdLogger()
	}
	l := ctx.Value(requestLogKey)
	if l == nil {
		return createStdLogger()
	}

	rl, ok := l.(RequestLogger)
	if ok {
		return rl
	} else {
		return createStdLogger()
	}

}

func createStdLogger() RequestLogger {
	sl := stdlogger.StdRequestLogger
	sl.Supportf("Couldn't find request logger in context. Using StdRequestLogger instead")
	return sl
}

type stdRequestLoggerProvider struct{}

func NewRequestLoggerProvider() *stdRequestLoggerProvider {
	return &stdRequestLoggerProvider{}
}

func (p *stdRequestLoggerProvider) NewRequestLogger(ctx context.Context, w http.ResponseWriter, r *http.Request) (RequestLogger, error) {
	return stdlogger.StdRequestLogger, nil
}
