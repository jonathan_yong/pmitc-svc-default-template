package pclog

type Logger interface {
	Errorf(format string, args ...interface{})
	Supportf(format string, args ...interface{})
	Devf(format string, args ...interface{})
	Printf(format string, args ...interface{})
}
