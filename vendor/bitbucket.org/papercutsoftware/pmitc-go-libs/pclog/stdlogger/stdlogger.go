package stdlogger

import (
	"fmt"
	"time"
)

type stdLogger struct{}
type stdRequestLogger struct{}

var (
	// StdLogger prints to standard output
	StdLogger        = &stdLogger{}
	StdRequestLogger = &stdRequestLogger{}
)

func (l *stdLogger) Errorf(format string, args ...interface{}) {
	l.log("ERROR", format, args...)
}

func (l *stdLogger) Supportf(format string, args ...interface{}) {
	l.log("SUPPORT", format, args...)
}

func (l *stdLogger) Devf(format string, args ...interface{}) {
	l.log("DEV", format, args...)
}

func (l *stdLogger) Printf(format string, args ...interface{}) {
	l.log("INFO", format, args...)
}

func (l *stdLogger) log(level string, format string, args ...interface{}) {
	log(level, format, args...)
}

func (l *stdRequestLogger) Errorf(format string, args ...interface{}) {
	l.log("ERROR", format, args...)
}

func (l *stdRequestLogger) Supportf(format string, args ...interface{}) {
	l.log("SUPPORT", format, args...)
}

func (l *stdRequestLogger) Devf(format string, args ...interface{}) {
	l.log("DEV", format, args...)
}

func (l *stdRequestLogger) Printf(format string, args ...interface{}) {
	l.log("INFO", format, args...)
}

func (l *stdRequestLogger) Close() error {
	return nil
}

func (l *stdRequestLogger) log(level string, format string, args ...interface{}) {
	log(level, format, args...)
}

func log(level string, format string, args ...interface{}) {
	// Prepend time and level to start of args
	realargs := append([]interface{}{time.Now().Format(time.UnixDate), level}, args...)
	fmt.Printf("[%s] %s:\t"+format+"\n", realargs...)
}
