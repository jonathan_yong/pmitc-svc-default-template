/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package gaelogger

import (
	"context"

	"google.golang.org/appengine/log"
)

type gaeRequestLogger struct {
	ctx *context.Context
}

func NewGAELogger(ctx *context.Context) *gaeRequestLogger {
	return &gaeRequestLogger{ctx: ctx}
}

func (gl *gaeRequestLogger) Errorf(format string, args ...interface{}) {
	log.Errorf(*gl.ctx, format, args...)
}

func (gl *gaeRequestLogger) Supportf(format string, args ...interface{}) {
	log.Warningf(*gl.ctx, format, args...)
}
func (gl *gaeRequestLogger) Devf(format string, args ...interface{}) {
	log.Debugf(*gl.ctx, format, args...)
}
func (gl *gaeRequestLogger) Printf(format string, args ...interface{}) {
	log.Infof(*gl.ctx, format, args...)
}

func (gl *gaeRequestLogger) Close() error {
	return nil //noOp closer. appengine does some dark voodoo deeper in it's infrastructure
}
