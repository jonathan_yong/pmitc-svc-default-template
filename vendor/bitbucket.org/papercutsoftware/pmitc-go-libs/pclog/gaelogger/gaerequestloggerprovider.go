/*
 * //Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package gaelogger

import (
	"context"
	"net/http"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/pclog"
)

/*
 provider is injected into services so that it can create a request scoped logger per request
*/

type gaeRequestLoggerProvider struct{}

func NewRequestLoggerProvider() *gaeRequestLoggerProvider {
	return &gaeRequestLoggerProvider{}
}

func (p *gaeRequestLoggerProvider) NewRequestLogger(ctx context.Context, w http.ResponseWriter, r *http.Request) (pclog.RequestLogger, error) {
	return NewGAELogger(&ctx), nil
}
