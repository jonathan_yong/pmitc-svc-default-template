package pubsubutil

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"sync"

	"net/http/httptest"

	"testing"

	"os"
	"strings"

	"bytes"

	"math/rand"

	"time"

	"github.com/golang/protobuf/proto"
	"google.golang.org/api/pubsub/v1"
)

/*
Example
	stub := pubsubutil.TestSubscriber(t, "printer-registered")
	type payload struct {
		Name string
		Age  int
	}

	event := &payload{
		Name: "bob",
		Age:  30,
	}
	pubsubutil.TestPublish(t, "printer-registered", event)
	stub.WaitForMsgs(t, 1)
	stub.AssertReceivedCount(t, 1)
	stub.AssertReceived(t)

	pbsbMsgs := stub.GetBytesMsgs(t)

	receivedMsg := &payload{}
	json.Unmarshal(pbsbMsgs[0], receivedMsg)
	if receivedMsg.Name != event.Name || receivedMsg.Age != event.Age {
		t.Fatalf("Message did not correctly get through whole cycle, got %#v expected %#v", event, receivedMsg)
	}
*/

type stub struct {
	msgs         []*pushMessage
	topic        string
	subscription string
	closer       func()
	sync.RWMutex
}

func (m *stub) Close() {
	m.closer()
}

func (m *stub) Len() int {
	m.RLock()
	defer m.RUnlock()
	return len(m.msgs)
}

func (m *stub) AssertDidNotReceive(t *testing.T, waitTime *time.Duration) {
	if waitTime != nil {
		time.Sleep(*waitTime)
	} else {
		time.Sleep(time.Second)
	}
	m.RLock()
	defer m.RUnlock()
	if len(m.msgs) > 0 {
		t.Fatalf("Failed: Expected to recieve no messages, instead got msgs for sub: %s, msg %#v", m.subscription, m.GetRawMsgs())
	}
}

func (m *stub) AssertWaitForMsgs(t *testing.T, expect int) {
	retryCount := 50
	retryDuration := 100 * time.Millisecond
	for i := 1; i <= retryCount; i++ {
		m.RLock()
		length := len(m.msgs)
		m.RUnlock()
		if length == expect {
			return
		}
		if i == retryCount {
			t.Fatalf("Did not receive expected amount of msgs, %d, in 5 sec for sub: %s. msgs: %#v", expect, m.subscription, m.GetRawMsgs())
			return
		}
		time.Sleep(retryDuration)
	}
}

func (m *stub) GetRawMsgs() []*pushMessage {
	m.RLock()
	defer m.RUnlock()
	return m.msgs
}

func (m *stub) GetBytesMsgs(t *testing.T) [][]byte {
	m.RLock()
	defer m.RUnlock()

	decodedMsgs := [][]byte{}
	for _, msg := range m.msgs {
		data, err := msg.Data()
		if err != nil {
			t.Fatalf("Could not decode message for sub %s: %v %v", m.subscription, msg, err)
		}
		decodedMsgs = append(decodedMsgs, data)
	}
	return decodedMsgs
}

// Golang does not allow type casting inside array, get element via index and cast to proto.message
func (m *stub) GetProtoMsgs(t *testing.T, accessor func(index int) proto.Message) {
	m.RLock()
	defer m.RUnlock()

	for i, msg := range m.msgs {
		err := msg.DecodeProtobuf(context.TODO(), accessor(i))
		if err != nil {
			t.Fatalf("Could not decode proto for sub %s: %v %v", m.subscription, msg, err)
		}
	}
}

func (m *stub) GetFirstProtoMsg(t *testing.T, proto proto.Message) {
	m.RLock()
	defer m.RUnlock()

	if len(m.msgs) == 0 {
		t.Fatalf("No messages recived")
	}

	err := m.msgs[0].DecodeProtobuf(context.TODO(), proto)
	if err != nil {
		t.Fatalf("Could not decode proto for sub %s: %v %v", m.subscription, m.msgs[0], err)
	}
}

func TestSubscriber(t *testing.T, topic string) *stub {
	subscription := "testingSub-" + topic + "-" + randString(6)
	m := &stub{
		msgs:         []*pushMessage{},
		topic:        topic,
		subscription: subscription,
	}

	pushHandler := func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/health" {
			return
		}

		var msg pushMessage
		data, err := ioutil.ReadAll(r.Body)
		if err != nil {
			m.RLock()
			t.Fatalf("failed to read a pubsub message for %s: %v", m.subscription, err)
			m.RUnlock()
			return
		}

		err = json.Unmarshal(data, &msg)
		if err != nil {
			m.RLock()
			t.Fatalf("failed to unmarshal a pubsub message for %s: %v", m.subscription, err)
			m.RUnlock()
			return
		}
		m.Lock()
		m.msgs = append(m.msgs, &msg)
		t.Logf("Added a received message, now have %v: %#v", len(m.msgs), msg.Subscription)
		m.Unlock()
	}

	s := httptest.NewServer(http.HandlerFunc(pushHandler))

	resp, err := http.Get(s.URL + "/health")
	t.Logf("checking %v is up and running", s.URL+"/health")
	counter := 0
	for err != nil || resp.StatusCode != http.StatusOK {
		resp, err = http.Get(s.URL + "/health")
		if counter > 50 {
			t.Fatalf("stub subscription server for %s did not start. Err: %v", m.subscription, err)
		}
		time.Sleep(100 * time.Millisecond)
		counter++
	}
	t.Logf("stub subscription server for %s up and running", m.subscription)

	pubsubHost := os.Getenv("PUBSUB_EMULATOR_HOST")
	if pubsubHost == "" {
		t.Fatalf("PUBSUB_EMULATOR_HOST is not provided")
	}

	pubsubHostURL := fmt.Sprintf("http://%s/v1/projects/localhost/subscriptions/%s", pubsubHost, subscription)
	body := fmt.Sprintf("{\"topic\":\"projects/localhost/topics/%s\",\"pushConfig\":{\"pushEndpoint\":\"%s\"} }", topic, s.URL)
	r, err := http.NewRequest(http.MethodPut, pubsubHostURL, strings.NewReader(body))
	if err != nil {
		t.Fatalf("failed to create a request for subscription: %v", err)
	}

	_, err = http.DefaultClient.Do(r)
	if err != nil {
		t.Fatalf("failed to create a subscription: %v", err)
	}
	t.Logf("created subscriber for: %s on URL %s", topic, s.URL)

	m.Lock()
	m.closer = func() {
		r, err := http.NewRequest(http.MethodDelete, pubsubHostURL, nil)
		if err != nil {
			t.Fatalf("failed to create a request to delete subscription: %v", err)
		}
		_, err = http.DefaultClient.Do(r)
		if err != nil {
			t.Fatalf("failed to delete a subscription: %v", err)
		}
		s.Close()
		t.Logf("Closed subscription: %v", subscription)
	}
	m.Unlock()

	return m
}

func TestPublish(t *testing.T, topic string, val interface{}, attrs map[string]string) {

	var err error
	var b []byte
	switch v := val.(type) {
	case proto.Message:
		t.Logf("Marshalling ProtoBuf payload: %s %v", topic, val)
		b, err = proto.Marshal(v)
	default:
		t.Logf("Marshalling JSON payload: %s %v", topic, val)
		b, err = json.Marshal(val)
	}
	if err != nil {
		t.Fatalf("failed to marshal payload: %s %v", topic, err)
	}

	msg := &pubsub.PubsubMessage{
		Attributes: attrs,
		Data:       base64.StdEncoding.EncodeToString(b),
	}
	req := &pubsub.PublishRequest{
		Messages: []*pubsub.PubsubMessage{msg},
	}

	b, err = json.Marshal(req)
	if err != nil {
		t.Fatalf("failed to marshal payload: %s %v", topic, err)
	}

	pubsubHost := os.Getenv("PUBSUB_EMULATOR_HOST")
	if pubsubHost == "" {
		t.Fatalf("PUBSUB_EMULATOR_HOST is not provided")
	}
	pubsubHostURL := fmt.Sprintf("http://%s/v1/projects/localhost/topics/%s:publish", pubsubHost, topic)
	r, err := http.NewRequest(http.MethodPost, pubsubHostURL, bytes.NewReader(b))
	if err != nil {
		t.Fatalf("failed to create a request for publish: %s %v", topic, err)
	}

	_, err = http.DefaultClient.Do(r)
	if err != nil {
		t.Fatalf("failed to publish: %s %v", topic, err)
	}

}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func randString(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Int63()%int64(len(letterBytes))]
	}
	return string(b)
}
