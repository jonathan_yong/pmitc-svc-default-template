package pubsubutil

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/authentication/keymanager"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/authentication"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/jwt"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/serviceproperties"
	"google.golang.org/api/option"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/pclog"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/pmitccontext"
	"github.com/golang/protobuf/proto"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/pubsub/v1"
)

func Publish(ctx context.Context, topic string, val interface{}) error {
	logger := pclog.GetRequestLogger(ctx)
	var err error
	httpClient, err := google.DefaultClient(ctx, pubsub.PubsubScope)
	if err != nil {
		return err
	}
	client, err := pubsub.NewService(ctx, option.WithHTTPClient(httpClient))
	if err != nil {
		return err
	}

	if os.Getenv("PUBSUB_EMULATOR_HOST") != "" {
		logger.Devf("Using local emulator at " + os.Getenv("PUBSUB_EMULATOR_HOST"))
		client.BasePath = "http://" + os.Getenv("PUBSUB_EMULATOR_HOST")
	}

	var b []byte
	switch v := val.(type) {
	case proto.Message:
		b, err = proto.Marshal(v)
	default:
		b, err = json.Marshal(val)
	}
	if err != nil {
		return err
	}

	attrs := make(map[string]string)
	pmitccontext.SetPubsubAttributes(ctx, attrs)

	keys, err := keymanager.GetKeyList(ctx)
	if err != nil {
		return err
	}
	sp := serviceproperties.GetFromEnv()
	pubsubRetention := 7 * 24 * time.Hour
	serviceJwt, err := jwt.NewServiceJwt(keys.GetPrimarySecretKey().GetSecret(), sp.ServiceName, "pubsub", topic, time.Now(), pubsubRetention)
	if err != nil {
		return errors.New("Publishing ProtoBuf jwt creation failed: " + err.Error())
	}
	authentication.SetBearerOnAttributes(attrs, string(serviceJwt))

	msg := &pubsub.PubsubMessage{
		Data:       base64.StdEncoding.EncodeToString(b),
		Attributes: attrs,
	}
	req := &pubsub.PublishRequest{
		Messages: []*pubsub.PubsubMessage{msg},
	}

	publishCall := client.Projects.Topics.Publish(fullNameOf(topic), req)

	logger.Devf("Publishing pubsub message: %+v", req)
	res, err := publishCall.Do()
	if err != nil {
		return errors.New("Publishing ProtoBuf message failed: " + err.Error())
	}

	logger.Devf("Published to Topic: %s. err: %v response: %v", topic, err, res)
	return err
}

func fullNameOf(name string) string {
	return fmt.Sprintf("projects/%s/topics/%s", os.Getenv("PROJECT_ID"), name)
}

type pushMessage struct {
	Message struct {
		Attributes map[string]string `json:"attributes"`
		Data       string            `json:"data"`
		MessageID  string            `json:"message_id"`
	} `json:"message"`
	Subscription string `json:"subscription"`
}

func UnMarshal(r *http.Request) (*pushMessage, error) {

	var msg pushMessage

	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		//Log error
		return nil, err
	}

	err = json.Unmarshal(data, &msg)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshall content %v from message %v", string(data), msg)
	}

	return &msg, err

}

func (pm *pushMessage) Attributes() map[string]string {
	return pm.Message.Attributes
}

func (pm *pushMessage) Data() ([]byte, error) {

	return base64.StdEncoding.DecodeString(pm.Message.Data)

}

func (pm *pushMessage) DecodeProtobuf(ctx context.Context, msg proto.Message) error {
	logger := pclog.GetRequestLogger(ctx)
	data, err := pm.Data()
	if err != nil {
		logger.Errorf("failed to unmarshall message data %v from message %v", string(data), pm)
		return err
	}

	if err := proto.Unmarshal(data, msg); err != nil {
		logger.Errorf("failed to unmarshall protobuf content %v from message %v", string(data), pm)
		return err
	}

	return nil
}

type PubsubMessage interface {
	Attributes() map[string]string
	Data() ([]byte, error)
	DecodeProtobuf(ctx context.Context, msg proto.Message) error
}
