/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package serviceproperties

import (
	"os"
)

const (
	EnvVarServiceName = "PMITC_SERVICE_NAME"
	EnvVarProjectId   = "PROJECT_ID"
)

type ServiceProperties struct {
	ProjectId               string
	ServiceName             string
	IsLocalHost             bool
	IsIntegrationTestRunner bool
}

func GetFromEnv() ServiceProperties {
	projectId := os.Getenv(EnvVarProjectId)
	isLocalHost := os.Getenv("RUN_WITH_DEVAPPSERVER") != "" || os.Getenv("GOOGLE_CLOUD_PROJECT") == "localhost" || projectId == "localhost"
	isIntegrationTestRunner := os.Getenv("INTEGRATION_TEST_RUNNER") != ""
	if projectId == "" {
		projectId = "unknown - PROJECT_ID env var not set"
	}
	serviceName := os.Getenv(EnvVarServiceName)
	if serviceName == "" {
		serviceName = "unknown PMITC_SERVICE_NAME env var not set"
	}
	sp := ServiceProperties{
		ProjectId:               projectId,
		ServiceName:             serviceName,
		IsLocalHost:             isLocalHost,
		IsIntegrationTestRunner: isIntegrationTestRunner,
	}
	return sp
}
