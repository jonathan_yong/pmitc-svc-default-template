// Copyright (c) 2019 PaperCut Software Int. Pty. Ltd.
package pmitccontext

import (
	"context"
	"net/http"
)

type RequestContextProvider interface {
	NewContext(r *http.Request) context.Context
}

type requestContextProvider struct {
}

func (d *requestContextProvider) NewContext(r *http.Request) context.Context {
	return r.Context()
}

func NewDefaultRequestContextProvider() RequestContextProvider {
	return &requestContextProvider{}
}
