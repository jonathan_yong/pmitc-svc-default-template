/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package pmitccontext

import (
	"context"
	"net/http"
	"strconv"
	"strings"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/serviceproperties"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/authentication"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/standardheaders"
)

const (
	pmitcValues  = "PMITC-CONTEXT-VALUES"
	orgIdKey     = "orgId"
	userIdKey    = "userId"
	profileIdKey = "profileId"
)

/*
Transfers request headers to context and logs correlationId if found
*/
func FromRequest(ctx context.Context, r *http.Request) context.Context {

	contextValues := &values{
		correlationId: r.Header.Get(standardheaders.CorrelationId),
		authToken:     authentication.ExtractBearer(r),
		requestOrigin: r.Header.Get(standardheaders.RequestOrigin),
	}

	if strings.ToLower(r.Header.Get(standardheaders.Async)) == "true" {
		contextValues.async = true
	}

	return context.WithValue(ctx, pmitcValues, contextValues)
}

/*
Transfers a map of attributes to a context, intention is to be used collecting pubsub message attributes
*/
func FromPubsubAttributes(ctx context.Context, attributes map[string]string) context.Context {

	contextValues := &values{
		correlationId: attributes[standardheaders.CorrelationId],
		async:         true,
		authToken:     authentication.ExtractBearerFromAttributes(attributes),
		requestOrigin: attributes[standardheaders.RequestOrigin],
	}

	ctx = context.WithValue(ctx, pmitcValues, contextValues)
	return ctx
}

/*
Populates pmitc standard values as headers in case of task queue and outbound http requests
*/
func SetHeaders(ctx context.Context, async bool, r *http.Request) {
	pmitcValues := GetValues(ctx)
	r.Header.Set(standardheaders.CorrelationId, pmitcValues.GetCorrelationId())
	r.Header.Set(standardheaders.Async, strconv.FormatBool(async))
	r.Header.Set(standardheaders.RequestOrigin, serviceproperties.GetFromEnv().ServiceName)
}

/*
Populates pmitc standard values as headers in case of task queue and outbound http requests
*/
func SetPubsubAttributes(ctx context.Context, attrs map[string]string) {
	pmitcValues := GetValues(ctx)
	attrs[standardheaders.CorrelationId] = pmitcValues.GetCorrelationId()
	attrs[standardheaders.Async] = "true"
	attrs[standardheaders.RequestOrigin] = serviceproperties.GetFromEnv().ServiceName
}

func GetValues(ctx context.Context) Values {
	original, ok := ctx.Value(pmitcValues).(Values)
	if ok {
		return original
	} else {
		return &values{}
	}
}

//Deprecated: blind access to headers isn't great
func GetHeader(ctx context.Context) http.Header {
	return http.Header{}
}

func GetOrgId(ctx context.Context) string {
	value := ctx.Value(orgIdKey)
	if value == nil {
		return ""
	}
	return value.(string)
}

func SetOrgId(ctx context.Context, orgId string) context.Context {
	return context.WithValue(ctx, orgIdKey, orgId)
}

func GetUserId(ctx context.Context) string {
	value := ctx.Value(userIdKey)
	if value == nil {
		return ""
	}
	return value.(string)
}

func SetUserId(ctx context.Context, userId string) context.Context {
	return context.WithValue(ctx, userIdKey, userId)
}

func GetProfileId(ctx context.Context) string {
	value := ctx.Value(profileIdKey)
	if value == nil {
		return ""
	}
	return value.(string)
}

func SetProfileId(ctx context.Context, profileId string) context.Context {
	return context.WithValue(ctx, profileIdKey, profileId)
}

type Values interface {
	GetCorrelationId() string
	IsAsyncCall() bool
	GetRequestOrigin() string
	GetAuthToken() string
	//Deprecated: will always be empty
	GetHeader() http.Header
}

type values struct {
	correlationId string
	async         bool
	authToken     string
	requestOrigin string
}

func (v *values) GetCorrelationId() string {
	return v.correlationId
}
func (v *values) IsAsyncCall() bool {
	return v.async
}
func (v *values) GetAuthToken() string {
	return v.authToken
}
func (v *values) GetRequestOrigin() string {
	return v.requestOrigin
}

func SetCorrelationId(ctx context.Context, cid string) context.Context {
	original, ok := ctx.Value(pmitcValues).(Values)
	if ok {
		return context.WithValue(ctx, pmitcValues, &values{
			correlationId: cid,
			async:         original.IsAsyncCall(),
			authToken:     original.GetAuthToken(),
			requestOrigin: original.GetRequestOrigin(),
		})
	} else {
		return context.WithValue(ctx, pmitcValues, &values{
			correlationId: cid,
		})
	}
}

func SetAuthToken(ctx context.Context, token string) context.Context {
	original, ok := ctx.Value(pmitcValues).(Values)
	if ok {
		return context.WithValue(ctx, pmitcValues, &values{
			correlationId: original.GetCorrelationId(),
			async:         original.IsAsyncCall(),
			authToken:     token,
			requestOrigin: original.GetRequestOrigin(),
		})
	} else {
		return context.WithValue(ctx, pmitcValues, &values{
			authToken: token,
		})
	}
}

//Deprecated: will always be empty
func (v *values) GetHeader() http.Header {
	return http.Header{}
}
