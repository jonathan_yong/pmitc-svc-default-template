// Copyright (c) 2017 PaperCut Software Int. Pty. Ltd.

package httputils

import (
	"net"
	"net/http"
	"net/url"
	"os"
	"strings"
)

// ForceProxyOnDefaultHTTPClient will force the default HTTP client to use a proxy if one is required for a HTTP request.
// Golang's http.ProxyFromEnvironment unfortunately caches environment vars so
// changing after loading causes issues!
// FUTURE: Move into the open source proxy library we are going to make.
func ForceProxyOnDefaultHTTPClient(proxy string) {
	if transport, ok := http.DefaultTransport.(*http.Transport); ok {

		if !strings.HasPrefix(proxy, "http") && proxy != "" {
			proxy = "http://" + proxy
		}
		os.Setenv("HTTP_PROXY", proxy)
		proxyURL, _ := url.Parse(proxy)

		transport.Proxy = func(req *http.Request) (*url.URL, error) {
			if proxy == "" {
				return nil, nil
			}
			// No proxy on localhost
			if strings.Contains(req.URL.Host, "localhost") || strings.Contains(req.URL.Host, "127.0.0.1") {
				return nil, nil
			}
			// No proxy on IP addresses - assume local
			// FUTURE: Maybe we ignore internal IP's only by default?
			host, _, err := net.SplitHostPort(req.URL.Host)
			if err != nil {
				host, _, err = net.SplitHostPort(req.URL.Host + ":80")
			}
			if err != nil {
				if ip := net.ParseIP(host); ip != nil {
					return nil, nil
				}
			}
			return proxyURL, nil
		}
	}
}
