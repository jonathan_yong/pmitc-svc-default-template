// Copyright (c) 2016 PaperCut Software International Pty. Ltd.

// Important:  Plan is to open source this.  Treat is as such and
// don't put in "business logic" in this layer.

package httputils

import (
	"bytes"
	"errors"
	"io"
	"io/ioutil"
	"math"
	"math/rand"
	"net/http"
	"sync"
	"time"
)

var (
	// ErrRequestTimeout is called when the request time out due to exceeding RetryClient.Timeout
	ErrRequestTimeout = errors.New("HTTPRetryClient: request timed out")

	// ErrMaxAttemptsExceeded is called when the request time out due to exceeding RetryClient.Timeout
	ErrMaxAttemptsExceeded = errors.New("HTTPRetryClient: max attempts exceeded")

	// ErrRequestCancelled is returned if the request is cancelled by calling close(cancel)
	ErrRequestCancelled = errors.New("HTTPRetryClient: request cancelled")

	randSrc      = rand.New(rand.NewSource(time.Now().UTC().UnixNano()))
	randSrcMutex sync.Mutex
)

// RetryClient is a retrying version of http.Client supporting only the Do method.
type RetryClient struct {
	// HTTPClient is a HTTP client. If it is nil, http.DefaultClient is used. This means a retry doesn't
	// occur if HTTPClient is nil and a server doesn't respond somehow.
	HTTPClient *http.Client

	// Timeout specifies a time limit for retries made by this retry-client. The retry-client
	// retries the request until Timeout elapses or ShouldRetryResponseFunc returns false.
	// If Timeout is zero, a retry occurs according to the result of ShouldRetryResponseFunc.
	Timeout time.Duration

	// MaxAttempts specifies an attempt limit for retries made by this retry-client. The retry-client
	// retries the request for MaxAttempts times or ShouldRetryResponseFunc returns false.
	// If MaxAttempts is zero, a retry occurs according to the result of ShouldRetryResponseFunc.
	MaxAttempts int

	// Cancel is an optional channel whose closure indicates that the retry-client request should be
	// regarded as cancelled.
	Cancel <-chan struct{}

	// OnAttemptFunc (advanced) is a function that is called before each call attempt.
	// It allows the request (e.g. headers) to be optionally modified and is called
	// before the first attempt. attempts=0 for first attempt, and 1+ on retry.
	OnAttemptFunc func(req *http.Request, attempts int)

	// OnResponseFunc (advanced) is a function that is called after each call to Do().
	// This function may be used to assist with logging or intercepting during retries. There is no
	// default implementation.
	OnResponseFunc func(resp *http.Response, err error, attempts int)

	// BackoffDelayFunc (advanced) is a function that controls the delay between retry requests.
	// The default implementation is DefaultExponentialJitteredBackoffDelay.
	BackoffDelayFunc func(attempts int) time.Duration

	// ShouldRetryResponseFunc (advanced) is a function that controls if a request should retried, or
	// should return as an error. The default implementation is DefaultShouldRetryResponseFunc.
	ShouldRetryResponseFunc func(resp *http.Response) bool
}

// DefaultExponentialJitteredBackoffDelay is the default backoff delay function.
func DefaultExponentialJitteredBackoffDelay(attempts int) time.Duration {
	// Exponential increase with jitter of max 30%
	const minRetryIntervalMs = 1000           // 1 seconds
	const maxRetryIntervalMs = 30 * 60 * 1000 // 30 min
	const jitterRatio = 0.3

	if attempts < 0 {
		attempts = 0
	}

	randSrcMutex.Lock()
	jitter := randSrc.Intn(int(jitterRatio * float64(minRetryIntervalMs)))
	randSrcMutex.Unlock()

	r := minRetryIntervalMs + jitter
	delayMs := float64(r) * math.Exp2(float64(attempts))
	d := time.Duration(delayMs) * time.Millisecond

	if d > maxRetryIntervalMs*time.Millisecond {
		d = maxRetryIntervalMs * time.Millisecond
	}
	return d
}

// DefaultShouldRetryResponseFunc is the default function that determins if a response is retried.
func DefaultShouldRetryResponseFunc(resp *http.Response) bool {
	if resp == nil {
		return true
	}

	// 200 range always OK
	if code := resp.StatusCode; code/100 == 2 {
		return false
	}
	// Never retry on 400 or 422 (Bad Request)
	switch resp.StatusCode {
	case http.StatusBadRequest, 422:
		return false
	}

	// All others are retried
	return true
}

func (c *RetryClient) httpClient() *http.Client {
	if c.HTTPClient == nil {
		return http.DefaultClient
	}
	return c.HTTPClient
}

func (c *RetryClient) shouldRetryResponseFunc() func(*http.Response) bool {
	if c.ShouldRetryResponseFunc == nil {
		return DefaultShouldRetryResponseFunc
	}
	return c.ShouldRetryResponseFunc
}

func (c *RetryClient) backoffDelayFunc() func(attempts int) time.Duration {
	if c.BackoffDelayFunc == nil {
		return DefaultExponentialJitteredBackoffDelay
	}
	return c.BackoffDelayFunc
}

// Do behaves the same a HTTPClient.Do() with retry support
func (c *RetryClient) Do(req *http.Request) (resp *http.Response, err error) {
	httpClient := c.httpClient()

	// We need to save copy of body if we are to retry
	var origBody []byte
	if req.Body != nil {
		origBody, err = ioutil.ReadAll(req.Body)
		req.Body.Close()
		if err != nil {
			return nil, err
		}
	}

	// FIXME: Existing value of req.Cancel ???
	cancel := make(chan struct{})
	req.Cancel = cancel

	type responseAndError struct {
		resp *http.Response
		err  error
	}
	result := make(chan responseAndError, 1)
	maxAttemptsExceeded := make(chan bool, 1)

	go func() {
		defer close(result)

		attempts := 0
		for {
			if c.MaxAttempts > 0 && attempts >= c.MaxAttempts {
				maxAttemptsExceeded <- true
				return
			}

			if req.Body != nil {
				req.Body = ioutil.NopCloser(bytes.NewBuffer(origBody))
			}
			if c.OnAttemptFunc != nil {
				c.OnAttemptFunc(req, attempts)
			}
			resp, err := httpClient.Do(req)
			if c.OnResponseFunc != nil {
				c.OnResponseFunc(resp, err, attempts)
			}
			if err == nil {
				if !c.shouldRetryResponseFunc()(resp) {
					result <- responseAndError{resp, nil}
					return
				}
				//Best effort attempt to cleaning up
				_, _ = io.Copy(ioutil.Discard, resp.Body)
				_ = resp.Body.Close()
			}

			delay := c.backoffDelayFunc()(attempts)
			attempts++

			select {
			case <-time.After(delay):
				continue
			case <-cancel:
				result <- responseAndError{resp, err}
				return
			}
		}
	}()

	cancelAndCleanup := func() {
		close(cancel)
		go func() {
			if r := <-result; r.resp != nil {
				//Best effort attempt to cleaning up
				_, _ = io.Copy(ioutil.Discard, r.resp.Body)
				_ = r.resp.Body.Close()
			}
		}()
	}

	timeout := make(<-chan time.Time)
	if c.Timeout > 0 {
		timeout = time.After(c.Timeout)
	}

	select {
	case <-c.Cancel:
		cancelAndCleanup()
		return nil, ErrRequestCancelled
	case <-timeout:
		cancelAndCleanup()
		return nil, ErrRequestTimeout
	case <-maxAttemptsExceeded:
		cancelAndCleanup()
		return nil, ErrMaxAttemptsExceeded
	case r := <-result:
		return r.resp, r.err
	}
}

// FUTURE - Add Post(), PostForm(), and Get() methods?
