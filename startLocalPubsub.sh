#!/usr/bin/env bash

echo
echo Starting local pubsub service:
gcloud beta emulators pubsub start --host-port localhost:8010 &
sleep 2
$(gcloud beta emulators pubsub env-init)
