#!/usr/bin/env bash

readonly TEST_RESULTS_DIR=${WORKSPACE}/test-results
readonly MAIN_DIR=app

# Detect Intelj gopath override an re route tools to tmp file
# FIXME PATH broken
#if [ -z "$_INTELLIJ_FORCE_SET_GOPATH" ]; then
  readonly ORIGINAL_GOPATH=${GOPATH}
#else
#  echo "Running in Intelj shell redirecting ORIGINAL_GOPATH to ${TMPDIR}go"
#  mkdir -p ${TMPDIR}go
#  readonly ORIGINAL_GOPATH=${TMPDIR}go
#fi

#export GOPATH=${WORKSPACE}:${WORKSPACE}/vendor
echo "GOPATH=$GOPATH"
export GOFLAGS=-mod=vendor
export GOPRIVATE=bitbucket.org/papercutsoftware

###############################################################################
# CORE COMMANDS
###############################################################################

usage() {
  echo "Usage: ./make.sh <command> [arguments]

Commands:
usage:               Show this usage information.
generate:            Generate protobufs.
test:                Run unit tests. Arguments:
  - testpattern:     (optional) partial match of test function name(s)
integration-test/it: Run integration tests. Arguments:
  - testpattern:     (optional) partial match of test function name(s)
static-analysis/sa:  Run linting. Add new/n to only report new errors
deploy:              Deploys project to Google App Engine. Arguments:
  - projectid:       (required) ID of the GCP project.
serve:               Serve the application locally.
vendor-update:       Updates the vendors which match the optional name pattern
  - pattern:         (optional) A pattern to specify the packages to be updated
vendor-clean-fetch:  Deletes all packages from vendor and manifest, then refetches them all. This is VERY slow; use cautiously.
"
}

generate() {
    #gen_protos
    if [ -z "$CI" ]; then
        go mod vendor
    fi
    return 0
}

test() {
    local readonly rawtestname=$1
    local readonly coverresults_dir=${WORKSPACE}/coverage-results
    local readonly unit_test_out=${WORKSPACE}/unitTestOut.txt

    # Locate the package for the test if a test name was provided
    local testpattern=
    local testpackage=
    if [[ "${rawtestname}" != "" ]]; then
        testpattern="-run=^(${rawtestname})$"
        testpackage=$(grep -rl "${rawtestname}" "${WORKSPACE}" | xargs dirname | sed -e "s|${WORKSPACE}/||")
    fi

    setup_env
    exec_wrapped_function generate
    run_format

    go clean -testcache
    mkdir -p ${coverresults_dir}
    rm -rf ${coverresults_dir}/*

    echo "Running unit tests..."

    pushd ${WORKSPACE}/${modulename} > /dev/null
    go test -v -covermode=atomic -timeout 3m ${testpackage} ${testpattern} -coverprofile=${coverresults_dir}/cover.out ./... 2>&1 | tee ${unit_test_out}
    local readonly test_exit_code=${PIPESTATUS[0]}
    defer "echo Delete unit test output && rm -v ${unit_test_out}"
    popd > /dev/null

    echo "* unit tests finished"
    defer "echo Tests finished with code: ${test_exit_code}"
    go tool cover -html=${coverresults_dir}/cover.out -o ${coverresults_dir}/cover.html

    echo "* Writing report"
    mkdir -p ${TEST_RESULTS_DIR}
    cat ${unit_test_out} | go-junit-report > ${TEST_RESULTS_DIR}/unit-report.xml

    # Save the commit hash and the exit code, for use in the pre-push hook
    echo "export UNIT_TEST_HASH=$(hash_project)" > ${UNIT_TEST_STATS}
    echo "export UNIT_TEST_RESULT=${test_exit_code}" >> ${UNIT_TEST_STATS}

    return "${test_exit_code}"
}

integration_test() {
    setup_env
    exec_wrapped_function generate
    run_format
    local readonly integration_test_dir=${WORKSPACE}/integration_test

    if [ ! -d ${integration_test_dir} ]; then
        echo "No Integration Tests found; fake the stat file"
        # When there are no integration tests, write a dummy stat file
        # Save the commit hash and the exit code, for use in the pre-push hook
        echo "export INTEGRATION_TEST_HASH=$(hash_project)" > ${INTEGRATION_TEST_STATS}
        echo "export INTEGRATION_TEST_RESULT=0" >> ${INTEGRATION_TEST_STATS}
        return 0
    fi

    source ${WORKSPACE}/startLocalPubsub.sh
    defer 'echo Stopping pubsub && ${WORKSPACE}/stopLocalPubsub.sh'

    source ${WORKSPACE}/startLocalDatastore.sh
    defer 'echo Stopping datastore && ${WORKSPACE}/stopLocalDatastore.sh'

    exec_wrapped_function write_app_config "localhost"

    echo "* Starting a service..."
    # Note: The Jenkins machines don't have lsof installed, and because we have
    # the '-e' flag set, the script will faile as the lsof command is not found.
    # We need to find a better way detect a running instance of the application.
    # if [ $(lsof -i :8080 -t | wc -l) -ne 0 ]; then
    #   echo "An application is already running on port 8080. Run \"kill -9 \$(lsof -i :8080 -t)\" to terminate it"
    #   return 1
    # fi
    RUN_WITH_DEVAPPSERVER=true go run service/app/main.go &
    #defer 'echo Kill service && kill -9 $(lsof -i :8080 -t)'

    # upcheck
    ${ORIGINAL_GOPATH}/bin/upcheck -timeout 15 -urlSource - << ENDURLS
        http://localhost:8080/_ah/health
        http://${PUBSUB_EMULATOR_HOST}/v1/projects/localhost/subscriptions
        http://${DATASTORE_EMULATOR_HOST}/
ENDURLS
    local readonly upcheck_result=$?
    if [ ${upcheck_result} -ne 0 ]; then
        echo "* Service failed to start"
        return ${upcheck_result}
    fi

    create_topics_and_subs

    echo "* Service started"
    echo "Running integration tests. $1"

    local readonly integration_test_out=${WORKSPACE}/integrationTestOut.txt

    pushd ${integration_test_dir} > /dev/null
    go clean -testcache && INTEGRATION_TEST_RUNNER=true go test --run=$1 -v ./... 2>&1 | tee ${integration_test_out}
    local readonly test_exit_code=${PIPESTATUS[0]}
    defer "echo Delete unit test output && rm -v ${integration_test_out}"
    popd > /dev/null

    echo "* Integration tests finished"
    defer "echo Integration tests finished with code: ${test_exit_code}"
    echo "* Writing report"

    mkdir -p ${TEST_RESULTS_DIR}
    cat ${integration_test_out} | go-junit-report > ${TEST_RESULTS_DIR}/integration-report.xml

    # Save the commit hash and the exit code, for use in the pre-push hook
    echo "export INTEGRATION_TEST_HASH=$(hash_project)" > ${INTEGRATION_TEST_STATS}
    echo "export INTEGRATION_TEST_RESULT=${test_exit_code}" >> ${INTEGRATION_TEST_STATS}

    return ${test_exit_code}
}

# Alias for integration-test
it() {
    exec_wrapped_function integration_test $@
}

deploy() {
    local readonly project_id=${1}
    if [[ "${project_id}" == "" ]]; then
        echo "ERROR: project_id must be supplied as argument"
        return 1
    fi

    setup_env
    exec_wrapped_function generate

    exec_wrapped_function write_app_config ${project_id}
    create_topics_and_subs ${project_id}

    shift
    echo "* Deploying application to the project (${project_id})"

    # The App Engine builder doesn't support providing credentials for fetching from private repos. To work around this,
    # we use a GOPATH (not go mod) style deployment, which involves creating a suitable GOPATH.
    readonly staging=/tmp/staging
    readonly deploy_gopath=$staging/gopath:$staging/vendor
    readonly git_hash=$(git rev-parse --short HEAD)

    defer "echo Deleting staging directory && rm -rf $staging"

    mkdir -p $staging/gopath/src/$goModule
    mkdir -p $staging/vendor/src
    cp -R ./vendor/* $staging/vendor/src/
    rsync -a ./$modulename $staging/gopath/src/$goModule/

    cd $staging/gopath/src/$goModule
    export GOFLAGS=-mod=vendor
    GOPATH=${deploy_gopath} gcloud beta -q app deploy --project ${project_id} "$@" -v $git_hash $staging/gopath/src/$goModule/$modulename/app/app.yaml

    if [ "$?" -ne 0 ]; then
        echo "failed to deploy"
        return 1
    fi
}

promote() {
    local readonly project_id=${1}
    if [[ "${project_id}" == "" ]]; then
        echo "ERROR: project_id must be supplied as argument"
        return 1
    fi

    if [[ "${serviceName}" == "" ]]; then
        echo "ERROR: serviceName must be supplied in app.sh to promote canary"
        return 1
    fi

    echo "* Promoting application version $(git rev-parse --short HEAD) to the project (${project_id})"
    gcloud app -q --project ${project_id} services set-traffic ${serviceName} --splits $(git rev-parse --short HEAD)=1
    if [ "$?" -ne 0 ]; then
        echo "failed to deploy"
        return 1
    fi
}


canary_deploy() {
    local readonly project_id=${1}
    if [[ "${project_id}" == "" ]]; then
        echo "ERROR: project_id must be supplied as argument"
        return 1
    fi

    if [[ "${serviceName}" == "" ]]; then
        echo "ERROR: serviceName must be supplied in app.sh to deploy canary"
        return 1
    fi

    exec_wrapped_function deploy "$project_id" "--no-promote"
    set_canary_orgs
    set_canary_orgs_new
}

alphaOrgs=(
#   Tracer Bullet
    'd984c1d8'
#    Dev Org
    '4725fffa'
#    E2E
    'f1fa9d79'
    '8eeaba5f'
)

#  FIXME do this properly maybe small go binary?
set_canary_orgs() {
  echo "Setting Canary key for Canary.${serviceName} to version $(git rev-parse --short HEAD)"
    for org in "${alphaOrgs[@]}"
    do
    echo "Setting ${org}"
        curl -d "{ \"Key\": { \"Id\": \"Canary.${serviceName}\", \"OrgId\": \"${org}\"}, \"Value\": \"$(git rev-parse --short HEAD)\"}" -H "Content-Type: application/json" -H "Accept: application/json" -X POST https://config-dot-pc-pmitc.appspot.com/internal/config/set-config/v1
    done
}

set_canary_orgs_new() {
    echo "Setting Alpha channel for ${serviceName} to $(git rev-parse --short HEAD)"
        curl -d "{ \"channel\": \"Alpha\", \"serviceName\": \"${serviceName}\", \"version\": \"$(git rev-parse --short HEAD)\"}" -H "Authorization: Bearer VJ2QY6mWZjhi0fafrekCyIdG0Zm1UNSyvPSfAxqD7ZCqNQ2sq2" -H "Content-Type: application/json" -H "Accept: application/json" -X POST https://test-gateway-dot-pc-pmitc.appspot.com/test/internal/set-channel-version/v1
}

static_analysis() {
  setup_env
  exec_wrapped_function generate

  local analysis_results_dir=${WORKSPACE}/analysis-results
  if [ -z "$CI" ]; then
    export GO111MODULE=off
    GOPATH=${ORIGINAL_GOPATH} go get -u github.com/golangci/golangci-lint/cmd/golangci-lint
    unset GO111MODULE
    golangci-lint run --skip-dirs vendor --fix $@
    local analysis_exit_code=$?
    defer "echo Static analysis finished with code: ${analysis_exit_code}"

    # Save the commit hash and the exit code, for use in the pre-push hook
    echo "export STATIC_ANALYSIS_HASH=$(hash_project)" > ${STATIC_ANALYSIS_STATS}
    echo "export STATIC_ANALYSIS_RESULT=${analysis_exit_code}" >> ${STATIC_ANALYSIS_STATS}
  else
    curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s latest
    mkdir -p ${analysis_results_dir}
    rm -rf ${analysis_results_dir}/*
#    for now dont fail on static analysis
    export GO111MODULE=off
    ./bin/golangci-lint run --out-format=checkstyle $@ > ${analysis_results_dir}/checkstyle-result.xml || echo "THIS LINTING FAILURE WILL FAIL THE BUILD IN THE FUTURE"
  fi
}

# Alias for static-analysis
sa() {
    exec_wrapped_function static_analysis $@
}
static_analysis_new() {
 static_analysis --new-from-rev=HEAD~ $@
}
san() {
  static_analysis_new $@
}

publish() {
    if [ -z "${PROTO_NAME}" ]; then
        echo "No proto to publish"
    else
        pmitc-proto-publisher -name=${PROTO_NAME} -version=${PROTO_VERSION} -publish
    fi
}

serve() {
    setup_env
    exec_wrapped_function generate

    source ${WORKSPACE}/startLocalDatastore.sh
    defer "echo Stopping datastore && ${WORKSPACE}/stopLocalDatastore.sh"

    source ${WORKSPACE}/startLocalPubsub.sh
    defer "echo Stopping pubsub && ${WORKSPACE}/stopLocalPubsub.sh"

    exec_wrapped_function write_app_config "localhost"
    create_topics_and_subs

    go run service/app/main.go
}

# After update, we need to copy the go1.9 specific git-hooks
post_update() {
  cp -rf ${LOCAL_REPO_DIR}/generators/app/templates/go9/git-hooks .
}

###############################################################################
# CUSTOM COMMANDS
###############################################################################

vendor_update() {
    cat vendor/manifest| grep importpath | awk '{print $2}' | sed -e "s|[\",]||g" | grep "$1" | tee /dev/tty | xargs -n 1 -P 20 -I % sh -c 'gb vendor update % || echo failed to update %'
    gb vendor purge
}

vendor_clean_fetch() {
    gb vendor purge

    # Delete everything from the manifest, and fetch it all again, which will allow dependencies to be 'updated'
    local readonly all_packages=($(cat vendor/manifest| grep importpath | awk '{print $2}' | sed -e "s|[\",]||g"))
    mv vendor vendor_old
    for p in ${all_packages[@]}; do
        # This is where we can update any changed repository names
        p=${p//pmitc-activity-log/pmitc-svc-activity-log}

        # Check that the package we want to vendor isn't already vendored
        if grep "${p}" vendor/manifest > /dev/null; then
            echo "Already vendored: ${p}"
            continue
        fi

        echo "* Fetching: ${p}"
        gb vendor fetch ${p}
    done
}

create_topics_and_subs() {
    local readonly project_id=$1

    # Define topics and subscribers like:
    # topic subscriptionName subscriberPushURL
    if [ "${pubsub}" = false ]; then
        return
    fi

    # Get current list of topics and subscriptions up front if running in cloud
    local gcp_topics=
    local gcp_subscriptions=
    if [[ "${project_id}" != "" ]]; then
        gcloud config set project ${project_id}
#        gcp_topics=$(gcloud beta pubsub topics list | grep name)
#        gcp_subscriptions=$(gcloud beta pubsub subscriptions list | grep name)
    fi


    echo "* creating topics and subscriptions: ${project_id}"

    for topicAndSub in "${topicsAndSubs[@]}"
    do
        sub=(${topicAndSub})

        topic=${sub[0]}
        subscriber=${sub[1]}
        pushURL=${sub[2]}

        echo
        echo "- topic      : [${topic}]"
        echo "- subscriber : [${subscriber}]"
        echo "- pushURL    : [${pushURL}]"
        echo

        if [[ "${PUBSUB_EMULATOR_HOST}" != "" ]]; then
            localTopic="${PUBSUB_EMULATOR_HOST}/v1/projects/localhost/topics/${topic}"
            echo "* creating local topic at: ${localTopic}"
            curl -X PUT ${localTopic}
            echo "* Topic [${topic}] created"


            # Only create subscription if subscriber and pushURL are defined
            if [ -n "${subscriber}" ] && [ -n "${pushURL}" ]; then
                localSub="${PUBSUB_EMULATOR_HOST}/v1/projects/localhost/subscriptions/${subscriber}"
                echo "* local subscription request: ${localSub}"
                localPushURL="http://localhost:8080${pushURL}"
                subBody="{\"pushConfig\":{\"pushEndpoint\":\"${localPushURL}\"},\"topic\":\"projects/localhost/topics/${topic}\"}"
                curl -X PUT ${localSub} -H 'Content-Type: application/json' -d ${subBody}

                echo "* Local subscription ${subscriber} created. pushURL: ${localPushURL}"
            fi
        else
            gcloud config set project ${project_id}

            if [ ! $(echo ${gcp_topics} | grep "${topic}") ]; then
                echo "* Creating topic on project ${project_id}"
                gcloud beta pubsub topics create ${topic}
                echo "* Topic [${topic}] created"
            else
                echo "* Topic [${topic}] already exists"
            fi

            # Only create subscription if subscriber and pushURL are defined
            if [ -n "${subscriber}" ] && [ -n "${pushURL}" ]; then
                if [ ! $(echo ${gcp_subscriptions} | grep "${subscriber}") ]; then
                    gcloud beta pubsub subscriptions create ${subscriber} \
                    --topic=${topic} \
                    --push-endpoint=https://${serviceName}-dot-${project_id}.appspot.com${pushURL}
                    --expiration-period=never
                    echo "* Subscription ${subscriber} created. pushURL: ${pushURL}"
                else
                    echo "* Subscription [${subscriber}] already exists"
                fi
            fi
        fi
    done
}

check_deps() {
    # operating system
    log::info "OS: "`uname -a`

    # go version check
    go version
    if [ "$?" -gt 0 ]; then
        log::error "go sdk is required"
        exit 1
    fi

    # gcloud version check
    if ! command -v gcloud > /dev/null; then
        log::error "gcloud is required."
        exit 1
    fi

    log::info "build version: ${BUILD_VERS}"
}

get_deps() {
      export GO111MODULE=off
  GOPATH=${ORIGINAL_GOPATH} go get golang.org/x/tools/cmd/goimports
  GOPATH=${ORIGINAL_GOPATH} go get github.com/golang/protobuf/protoc-gen-go
  GOPATH=${ORIGINAL_GOPATH} go get github.com/jstemmer/go-junit-report
  GOPATH=${ORIGINAL_GOPATH} go get bitbucket.org/aprilayres/upcheck
      export GO111MODULE=on
}


setup_env() {
  echo "running check and get deps..."
  check_deps
  get_deps
}

write_app_config() {
    local project_id=$1

    if [[ "${project_id}" == "" ]]; then
        echo "RUNNING LOCAL APP"
        project_id="localhost"
    fi

    local readonly datastore_project_id="${project_id}"

    echo "Writing application configuration"
    echo "- PROJECT_ID: ${project_id}"
    echo "- PMITC_SERVICE_NAME: ${serviceName}"
    echo "- PMITC_SERVICE_VERSION: ${serviceVersion}"

    if [[ "$PUBSUB_EMULATOR_HOST" == *"localhost"* ]]; then
        echo "- PUBSUB_EMULATOR_HOST: ${PUBSUB_EMULATOR_HOST}"
    fi

    if [[ "$DATASTORE_EMULATOR_HOST" == *"localhost"* ]]; then
        echo "- DATASTORE_EMULATOR_HOST: ${DATASTORE_EMULATOR_HOST}"
    fi

    echo

    local tmp=$(cat ${APP_YAML_TEMPLATE})

    if [[ "$PUBSUB_EMULATOR_HOST" == "" ]]; then
        # Remove emulator host entry
        tmp=$(echo "$tmp" | sed '/PUBSUB_EMULATOR_HOST/d')
    fi

    if [[ "${datastore_project_id}" != "localhost" ]]; then
        # Remove datastore emulator host entries when running on App Engine
        tmp=$(echo "$tmp" | sed '/DATASTORE_EMULATOR_HOST/d')
    fi

    local template=$(echo "$tmp")
    echo "$template" \
        | sed -e "s|@PROJECT_ID@|${project_id}|g" \
        | sed -e "s|@PMITC_SERVICE_NAME@|${serviceName}|g" \
        | sed -e "s|@PMITC_SERVICE_VERSION@|${serviceVersion}|g" \
        | sed -e "s|@DATASTORE_EMULATOR_HOST@|${DATASTORE_EMULATOR_HOST}|g" \
        | sed -e "s|@PUBSUB_EMULATOR_HOST@|${PUBSUB_EMULATOR_HOST}|g" \
        | sed -e "s|@DATASTORE_PROJECT_ID@|${datastore_project_id}|g" \
        | sed -e "s|@MEMORYSTORE_INSTANCE_ID@|${memorystoreInstanceId}|g" \
        | sed -e "s|@PMITC_SERVICE_LOCATION@|${serviceLocation}|g" \
        | sed -e "s|@VPC_CONNECTOR_NAME@|${vpcConnectorName}|g" \
        > ${APP_YAML}

    echo
    echo "app.yaml configuration:"
    echo "---------------------------------------------------"
    cat ${APP_YAML}
    echo "---------------------------------------------------"
}

run_format() {
    echo "* Running goimports..."
        find ${WORKSPACE} -name "*.pb.go" -prune -o -name "*.go"  -not -path "*/vendor/*" -exec goimports -w {} ";"

    local commentsToFind="Deprecated|FIXME|TODO"
    echo "* Scanning comments (${commentsToFind})..."
    echo
    find ${WORKSPACE} -name "*.pb.go" -prune -o -name "*.go" -not -path "*/vendor/*" | xargs egrep --colour=always -n -A 2 -i "${commentsToFind}" | awk '{$1=$1;print}'
    exitCode=$?

    echo "-----------------------------------------------------"
    if [ ${exitCode} -gt 1 ]; then
        return $?
    elif [ ${exitCode} -eq 1 ]; then
        echo "- Nothing found."
    else
        echo "- Some comments found."
        echo "- Please have a look at them before pushing your changes"
    fi
    echo "-----------------------------------------------------"
}

gen_protos() {
    if [[ ${PROTO_NAME} == "" ]]; then
        echo "PROTO_NAME not specified, so skipping generation"
        return 0
    fi

    if [[ "${PROTO_IN_DIR}" == "" || "${PROTO_OUT_DIR}" == "" ]]; then
        echo "PROTO_IN_DIR and PROTO_OUT_DIR must be defined if PROTO_NAME is defined"
        return 1
    fi
    mkdir -p "${PROTO_OUT_DIR}"

    # Run protoc on each file individually. This allows us to have multiple proto files in different packages; in this
    # module, we have 'api' protobuf and 'events' protobuf files. We must process each individually as the protoc
    # compiler for GO requires all the protobuf files passed to it to have the same 'go_package' option; processing
    # the proto files individually bypasses this.
    for proto_file in `find ${PROTO_IN_DIR} -name *.proto`; do
        protoc --go_out=${PROTO_OUT_DIR} \
            --proto_path=${PROTO_IN_DIR} \
            ${proto_file}
    done

    echo "* Generated go struct from proto resources"
}

# Sets up git hooks, if they haven't already been set up
setup_git_hooks() {
    pushd ${WORKSPACE} > /dev/null

    local git_hooks_path=($(find . -maxdepth 1 -name git-hooks))
    if [ ${#git_hooks_path[*]} -ne 1 ]; then
        echo "Expected to find 1 git-hooks directory, but didn't"
        return 0
    fi
    # The hooksPath needs to be relative to the repo root.
    local prefix=$(git rev-parse --show-prefix)
    local hooks_path=${prefix}${git_hooks_path[0]}
    git config core.hooksPath "${hooks_path}"
    echo "Git hooks set to [${hooks_path}]"

    popd > /dev/null
    return 0
}

intljSetup() {
echo $'<application>
    <component name="GoLibraries">
        <option name="urls">
            <list>
                <option value="file://$PROJECT_DIR$" />
                <option value="file://$PROJECT_DIR$/vendor" />
            </list>
        </option>
        <option name="useGoPathFromSystemEnvironment" value="false" />
    </component>
</application>' > ${WORKSPACE}/.idea/goLibraries.xml
}

is() {
    exec_wrapped_function intljSetup $@
}

# Generate a hash for the current project source + vendor directories
hash_project() {
  _md5=
    if which md5 > /dev/null; then
        # Mac OS
      _md5='md5 -q'
    elif which md5sum > /dev/null; then
      # Ubuntu
      _md5="md5sum | awk '{print \$1}'"
    else
      log::error "no suitable md5 program was found"
      return 1
    fi
    # This line:
    # - Find all files in service and vendor, separating entries with a NUL character
    # - Sorts all fond files using NUL as separator
    # - Calculate an md5 hash for each file (only outputting the hash, without the filename)
    # - Calculate an md5 hash from the hashes in the previous step
    eval "find service vendor -type f -print0 | sort -z | xargs -0 $_md5 | $_md5"
}

################################################################################
# EXECUTION
################################################################################

setup_git_hooks

# Minor workaround for 'monorepos' that contains dataflow and 'service' directory
if [ "$modulename" = "" ]; then
    modulename=service
fi

readonly APP_DIR=${WORKSPACE}/${modulename}/${MAIN_DIR}
readonly APP_YAML_TEMPLATE=${APP_DIR}/app.yaml.template
readonly APP_YAML=${APP_DIR}/app.yaml

# Vars used to force tests and static-analysis before pushing
readonly UNIT_TEST_STATS=${WORKSPACE}/.pmitc.unittest.stats
readonly INTEGRATION_TEST_STATS=${WORKSPACE}/.pmitc.integrationtest.stats
readonly STATIC_ANALYSIS_STATS=${WORKSPACE}/.pmitc.staticanalysis.stats

# Add .gitignore rule for .pmitc files
if ! grep "\.pmitc*" .gitignore; then
    echo "Adding .gitignore rules for PMITC build files"
    echo -e "\n.pmitc*" >> .gitignore
fi

function_exists "topics_and_subs" && topics_and_subs || echo "No topics_and_subs in app.sh"
if [ -z "$topicsAndSubs" ]; then
    echo "No pub/sub subscriptions set"
else
    pubsub=true
fi
