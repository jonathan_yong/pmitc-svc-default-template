module bitbucket.org/papercutsoftware/pmitc-svc-default-template

go 1.12

require (
	bitbucket.org/papercutsoftware/gopapercut v1.0.17
	bitbucket.org/papercutsoftware/pmitc-go-libs v0.0.0-20190824125555-1e3665b0ce0b
  bitbucket.org/papercutsoftware/pmitc-svc-default-template/src/api v0.0.0
	cloud.google.com/go/datastore v1.0.0
	github.com/golang/protobuf v1.3.2
	github.com/gorilla/mux v1.7.3
	golang.org/x/net v0.0.0-20190813141303-74dc4d7220e7
	google.golang.org/appengine v1.6.1
)

  replace bitbucket.org/papercutsoftware/pmitc-svc-default-template/src/api =>  ./src/api
