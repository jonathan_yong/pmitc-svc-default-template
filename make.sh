#!/bin/bash

# Fail on any error by default
set -e

# These commands MUST be implemented.
readonly CORE_COMMANDS=(usage generate test integration-test static-analysis deploy serve)

# Repo directory used for updates
readonly LOCAL_REPO_DIR=~/.pmitc/pmitc-go-template

###############################################################################
# GENERAL PURPOSE FUNCTIONS
###############################################################################

# This function checks that all the core commands have been implemented
core_commands_implemented() {
    # Assume all implemented to start with
    local result=0
    for c in ${CORE_COMMANDS[*]}; do
        if ! function_exists ${c//-/_}; then
            echo "Missing core function: ${c//-/_}"
            result=1
        fi
    done
    return ${result}
}

_DEFER=()
# A rough approximation to Golang's defer, this function will run command[s] on termination of the process
defer() {
    _DEFER+=("$@")
}
cleanup() {
    echo "Running all deferred statements"
    for l in "${_DEFER[@]}"; do
        echo  "Running deferred statement: ${l}"
        # LOL eval is evil, but who cares
        eval "${l}"
    done

}
trap 'cleanup' INT TERM EXIT

function_exists() {
    declare -f -F $1 > /dev/null
    return $?
}

# Executes a function including any pre_ and post_ hooks
exec_wrapped_function() {
    local readonly fn=${1}
    shift
    local args=${@}

    if function_exists pre_${fn}; then
        exec_function pre_${fn} ${args}
        if [ $? -ne 0 ]; then
            return 1
        fi
    fi

    exec_function ${fn} ${args}
    if [ $? -ne 0 ]; then
        return 1
    fi

    if function_exists post_${fn}; then
        exec_function post_${fn} ${args}
        if [ $? -ne 0 ]; then
            return 1
        fi
    fi
}

# Executes a function, logging the function itself and any arguments
exec_function() {
    local readonly fn=$1
    shift
    local readonly args=$@

    echo "Running [${fn}] with args [${args}]"
    ${fn} ${args}
    local readonly returnCode=$?
    echo "Finished running [${fn}]"

    return ${returnCode}
}



# Special function that gets the latest *.sh files from the template, and
# applies then to the current this directory
update() {
    if [ ! -e "${LOCAL_REPO_DIR}" ]; then
        mkdir -p "${LOCAL_REPO_DIR}"
        git clone --depth 1 git@bitbucket.org:papercutsoftware/pmitc-go-template.git ${LOCAL_REPO_DIR} && \
            pushd ${LOCAL_REPO_DIR} && \
            git checkout origin/master && \
            popd
    else
        pushd "${LOCAL_REPO_DIR}" && \
            git fetch && \
            git checkout origin/master && \
            popd
    fi

    for f in $(find "${LOCAL_REPO_DIR}/generators/app/templates" -name "*.sh" -maxdepth 1); do
        cp -v ${f} .
    done
}



###############################################################################
# EXECUTION
###############################################################################

readonly WORKSPACE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
pushd ${WORKSPACE} > /dev/null
defer 'echo Restore directory && popd > /dev/null'

if [ -z "$CI" ]; then
    echo "Running locally"
else
    echo "Running on CI"
fi

# Consume the first argument, or default to 'usage' as command
RAWCOMMAND="usage"
if [ "$#" -ge 1 ]; then
    RAWCOMMAND=$1
    shift
fi
readonly RAWCOMMAND
readonly ARGS=($@)

# actual function will use underscore instead of hyphen
readonly COMMAND=${RAWCOMMAND//-/_}

echo "
-----------------------------------------------------
 * workspace: ${WORKSPACE}
 * command:   ${RAWCOMMAND}
 * arguments: ${ARGS[*]}
-----------------------------------------------------
"

# Configuration from app.sh
source ${WORKSPACE}/app.sh
case ${projectType} in
    goService)
        # Check for go 1.9/1.11
        case ${goRuntime} in
            1.12)
                source ${WORKSPACE}/go_1_12.sh
                ;;
            1.11)
                source ${WORKSPACE}/go_1_11.sh
                ;;
            1.11gb)
                #like 1.11 but using gb
                source ${WORKSPACE}/go_1_11gb.sh
                ;;
            *)
                echo "goRuntime not specified; assuming Go 1.9"
                source ${WORKSPACE}/go_1_9.sh
                ;;
        esac
        ;;
    *)
        echo "Don't know how to handle projectType ${projectType}"
        exit 1
        ;;
esac

# update is a special command
if [[ "${COMMAND}" == "update" ]]; then
    exec_wrapped_function update
    exit 0
fi

if ! core_commands_implemented; then
    echo "All core commands must have implementations"
    exit 1
fi

if ! function_exists ${COMMAND}; then
    echo "ERROR: Unknown command '${COMMAND}'"
    usage
    exit 1
elif [[ "${COMMAND}" == "usage" ]]; then
    # Special case for usage, we don't wrap it with pre_ and _post hooks
    usage
    exit 0
else
    exec_wrapped_function ${COMMAND} ${ARGS[*]}
    exit $?
fi
