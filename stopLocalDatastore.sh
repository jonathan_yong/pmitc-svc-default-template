#!/usr/bin/env bash

echo
echo Check local datastore service:
ps -ef|egrep "emulators datastore|datastore-emulator"|grep -v grep

ps -ef|egrep "emulators datastore|datastore-emulator"|grep -v grep|awk '{print $2}'|xargs kill -9
echo
echo Killed the local datastore services:
ps -ef|egrep "emulators datastore|datastore-emulator"|grep -v grep

unset DATASTORE_DATASET
unset DATASTORE_EMULATOR_HOST
unset DATASTORE_EMULATOR_HOST_PATH
unset DATASTORE_HOST
unset DATASTORE_PROJECT_ID
