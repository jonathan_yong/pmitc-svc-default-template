module bitbucket.org/papercutsoftware/pmitc-svc-default-template/src/api

go 1.12

require (
	bitbucket.org/papercutsoftware/pmitc-go-libs v0.0.0-20190824125555-1e3665b0ce0b
	github.com/golang/protobuf v1.3.2
	golang.org/x/net v0.0.0-20190813141303-74dc4d7220e7
	google.golang.org/appengine v1.6.1
)
