/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package svcdefaulttemplate

import (
	"io/ioutil"
	"net/http"
	"strings"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/servicediscovery"

	"bitbucket.org/papercutsoftware/pmitc-svc-default-template/src/api/svcdefaulttemplateapi"
	"google.golang.org/appengine/urlfetch"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/clientutils"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/pubsubutil"

	"golang.org/x/net/context"
)

const (
	clientTargetService = "svc-default-template"

	createTemplate   = "/internal/svc-default-template/org/{orgId}/create-example/v1"
	getTemplate      = "/internal/svc-default-template/org/{orgId}/get-example/example/{exampleId}/v1"
	getMultiTemplate = "/internal/svc-default-template/org/{orgId}/get-all/v1"
	updateTemplate   = "/internal/svc-default-template/org/{orgId}/update-example/v1"
	deleteTemplate   = "/internal/svc-default-template/org/{orgId}/delete-example/example/{exampleId}/v1"

	getTimeUrl = "/public/svc-default-template/public-get-time-example/v1"

	pubsubCreateTopic = "example-create"
)

func NewClient() *Client {
	return &Client{}
}

type Client struct{}

func (c *Client) PubSubCreate(ctx context.Context, orgId, id, data string) error {
	req := &svcdefaulttemplateapi.PubSubCreateExampleRequest{
		OrgId:     orgId,
		ExampleId: id,
		MoreData:  data,
	}
	if err := pubsubutil.Publish(ctx, pubsubCreateTopic, req); err != nil {
		return err
	}
	return nil
}

func (c *Client) Create(ctx context.Context, orgId, data string) (string, error) {
	helper := clientutils.NewClientHelper(ctx, clientTargetService)

	pReq := &svcdefaulttemplateapi.CreateExampleRequest{
		MoreData: data,
	}
	pResp := &svcdefaulttemplateapi.CreateExampleResponse{}
	err := helper.ExecutePostRequest(orgId, createTemplate, pReq, pResp)
	if err != nil {
		return "", err
	}
	return pResp.GetExampleId(), nil
}

func (c *Client) Get(ctx context.Context, orgId string, exampleId string) (*svcdefaulttemplateapi.GetExampleResponse, error) {
	helper := clientutils.NewClientHelper(ctx, clientTargetService)

	resp := &svcdefaulttemplateapi.GetExampleResponse{}
	url := strings.Replace(getTemplate, "{exampleId}", exampleId, 1)
	err := helper.ExecuteGetRequest(orgId, url, resp)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (c *Client) GetMulti(ctx context.Context, orgId string) (*svcdefaulttemplateapi.GetExamplesResponse, error) {
	helper := clientutils.NewClientHelper(ctx, clientTargetService)

	resp := &svcdefaulttemplateapi.GetExamplesResponse{}
	err := helper.ExecuteGetRequest(orgId, getMultiTemplate, resp)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (c *Client) Update(ctx context.Context, orgId, id, data string) error {
	helper := clientutils.NewClientHelper(ctx, clientTargetService)

	pReq := &svcdefaulttemplateapi.UpdateExampleRequest{
		ExampleId: id,
		MoreData:  data,
	}
	return helper.ExecutePostRequest(orgId, updateTemplate, pReq, nil)
}

func (c *Client) Delete(ctx context.Context, orgId, exampleId string) error {
	helper := clientutils.NewClientHelper(ctx, clientTargetService)
	url := strings.Replace(deleteTemplate, "{exampleId}", exampleId, 1)
	return helper.ExecutePostRequest(orgId, url, nil, nil)
}

// Example of raw request, prefer clientutils
func (c *Client) GetServerTime(ctx context.Context) (string, error) {
	req, e := http.NewRequest(http.MethodGet, servicediscovery.Get(ctx, clientTargetService)+getTimeUrl, nil)
	if e != nil {
		return "", e
	}

	resp, e := urlfetch.Client(ctx).Do(req)
	if e != nil {
		return "", e
	}

	bytes, e := ioutil.ReadAll(resp.Body)
	if e != nil {
		return "", e
	}

	return string(bytes), nil
}
