// Copyright (c) 2014 PaperCut Software Int. Pty. Ltd.

// Package random contains various helper functions to help generate
// random (crypto-level random) data/strings.
package random

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/binary"
	"encoding/hex"
	"io"
	"math/big"
	"strings"
)

const alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
const numeric = "0123456789"
const alphaNumeric = alpha + numeric

func URLSafeString(length int) string {
	n := 3*length/4 + 1
	return Base64(n)[:length]
}

func Base64String(length int) string {
	return URLSafeString(length)
}

func HexString(length int) string {
	n := (length + 1) / 2
	return Hex(n)[:length]
}

func AlphanumericString(length int) string {
	return stringFromSet(length, alphaNumeric)
}

func AlphaString(length int) string {
	return stringFromSet(length, alpha)
}

func NumericString(length int) string {
	return stringFromSet(length, numeric)
}

func stringFromSet(length int, set string) string {
	// avoids mod bias!
	ret := make([]byte, length)
	setlen := big.NewInt(int64(len(set)))
	for i := 0; i < length; i++ {
		b, err := rand.Int(rand.Reader, setlen)
		if err != nil {
			panic(err)
		}
		ret[i] = set[int(b.Int64())]
	}
	return string(ret)
}

func Base64(bytes int) string {
	return strings.TrimRight(base64.URLEncoding.EncodeToString(Bytes(bytes)), "=")
}

func Hex(bytes int) string {
	return hex.EncodeToString(Bytes(bytes))
}

func Bytes(n int) []byte {
	data := make([]byte, n)
	_, err := io.ReadFull(rand.Reader, data)
	if err != nil {
		panic(err)
	}
	return data
}

// Int returns a random int (can be negative)
func Int() int {
	var n int64
	binary.Read(rand.Reader, binary.LittleEndian, &n)
	return int(n)
}

// IntRange return a random int inclusive min and max
func IntRange(min int, max int) int {
	if min < 0 {
		panic("invalid min")
	}
	if min > max {
		panic("invalid min and max range")
	}
	if min == max {
		return min
	}
	maxRand := max - min + 1
	b, _ := rand.Int(rand.Reader, big.NewInt(int64(maxRand)))
	return min + int(b.Int64())
}
