/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package canary

import (
	"fmt"
	"time"

	"bitbucket.org/papercutsoftware/pmitc-svc-canary/src/api/svccanaryapi"
	"golang.org/x/net/context"
)

const (
	clientTargetService = "canary"

	getChannelTemplate       = "/internal/canary/org/{orgId}/get-org-channel/v1"
	setChannelTemplate       = "/internal/canary/org/{orgId}/set-org-channel/v1"
	getVersionTemplate       = "/internal/canary/org/{orgId}/get-version/v1"
	setCustomVersionTemplate = "/internal/canary/org/{orgId}/set-custom-version/v1"

	setChannelVersionTemplate = "/internal/canary/set-channel-version/v1"

	getTimeout  = 30 * time.Millisecond
	httpTimeout = 100 * time.Millisecond
	attempts    = 2

	GeneralAvailability = "GA"
	Beta                = "Beta"
	Alpha               = "Alpha"
)

func NewClient() *Client {
	return &Client{}
}

type Client struct{}
type Context struct {
	ctx context.Context
}
type Channel struct {
	channel string
	ctx     context.Context
}
type Org struct {
	orgId string
	ctx   context.Context
}

func (c *Client) WithContext(ctx context.Context) *Context {
	return &Context{
		ctx: ctx,
	}
}

func (c *Context) ForOrg(orgId string) *Org {
	return &Org{
		orgId: orgId,
		ctx:   c.ctx,
	}
}

func (c *Context) ForChannel(channel string) *Channel {
	return &Channel{
		channel: channel,
		ctx:     c.ctx,
	}
}

func (o *Org) SetChannel(channel string) error {
	if o.orgId == "" {
		return fmt.Errorf("empty orgId provided")
	}
	helper := NewClientHelper(o.ctx, clientTargetService).WithRetry(attempts, httpTimeout)

	pReq := &svccanaryapi.SetOrgChannelRequest{
		Channel: channel,
	}
	return helper.ExecutePostRequest(o.orgId, setChannelTemplate, pReq, nil)
}

func (o *Org) GetChannel() (string, error) {
	if o.orgId == "" {
		return "", fmt.Errorf("empty orgId provided")
	}
	helper := NewClientHelper(o.ctx, clientTargetService).WithRetry(attempts, getTimeout)

	pResp := &svccanaryapi.GetChannelResponse{}
	err := helper.ExecutePostRequest(o.orgId, getChannelTemplate, nil, pResp)
	if err != nil {
		return "", err
	}
	return pResp.GetChannel(), nil
}

func (o *Org) GetVersion(serviceName string) (string, error) {
	if o.orgId == "" {
		return "", fmt.Errorf("empty orgId provided")
	}

	helper := NewClientHelper(o.ctx, clientTargetService).WithRetry(attempts, getTimeout)

	pReq := &svccanaryapi.GetVersionRequest{
		ServiceName: serviceName,
	}
	pResp := &svccanaryapi.GetVersionResponse{}
	err := helper.ExecutePostRequest(o.orgId, getVersionTemplate, pReq, pResp)
	if err != nil {
		return "", err
	}

	return pResp.GetVersion(), nil
}

func (o *Org) SetCustomVersion(serviceName, version string) error {
	if o.orgId == "" {
		return fmt.Errorf("empty orgId provided")
	}
	helper := NewClientHelper(o.ctx, clientTargetService).WithRetry(attempts, httpTimeout)

	pReq := &svccanaryapi.SetCustomVersionRequest{
		ServiceName: serviceName,
		Version:     version,
	}
	return helper.ExecutePostRequest(o.orgId, setCustomVersionTemplate, pReq, nil)
}

// Note: This will set version for all orgs in that channel
func (c *Channel) SetVersion(serviceName, version string) error {
	helper := NewClientHelper(c.ctx, clientTargetService).WithRetry(attempts, httpTimeout)

	pReq := &svccanaryapi.SetChannelVersionRequest{
		Channel:     c.channel,
		ServiceName: serviceName,
		Version:     version,
	}
	return helper.ExecutePostRequest("", setChannelVersionTemplate, pReq, nil)
}
