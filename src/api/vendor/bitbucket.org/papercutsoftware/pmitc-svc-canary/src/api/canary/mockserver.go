package canary

import (
	"net"
	"net/http"
	"net/http/httptest"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/servicediscovery"
)

func NewMockServer(handler func(http.ResponseWriter, *http.Request)) *httptest.Server {

	server := httptest.NewUnstartedServer(http.HandlerFunc(handler))
	listener, _ := net.Listen("tcp", "localhost:"+servicediscovery.GetIntegrationTestPort(clientTargetService))

	server.Listener = listener
	server.Start()

	return server
}
