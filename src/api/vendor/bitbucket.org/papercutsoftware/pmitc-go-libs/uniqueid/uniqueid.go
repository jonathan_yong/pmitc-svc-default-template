package uniqueid

import (
	"fmt"

	"bitbucket.org/papercutsoftware/gopapercut/random"
)

// createCompositeId generates ids with a prefix, sequence and a random component
// Eg. job-0000000001-vjuewgiopfg903iojv89e3
func CreateCompositeId(kind string, sequence int, randomCount int) string {

	return fmt.Sprintf(
		"%s-%.10d-%s",
		random.AlphanumericString(randomCount),
		sequence,
		kind,
	)

}

const randomCountForCorrelationID = 20

// CreateCorrelationId generates a correlationId in the following
// Eg. PRINT-CLIENT|vjuewgiopfg903iojv89e3
// Eg. COORDINATOR|vjuewgiopfg903iojv89e3
func CreateCorrelationId(source string) string {
	return fmt.Sprintf(
		"%s|%s",
		source,
		random.AlphanumericString(randomCountForCorrelationID),
	)
}
