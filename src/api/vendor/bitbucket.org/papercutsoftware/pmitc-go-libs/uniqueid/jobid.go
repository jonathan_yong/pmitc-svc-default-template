package uniqueid

import (
	"fmt"
	"time"

	"bitbucket.org/papercutsoftware/gopapercut/random"
)

const (
	jobTimeFormat = "20060102150405"
	randomLength  = 16
)

const (
	JobIDSourceClient       = "print-client"
	JobIDSourceEdgenode     = "edgenode"
	JobIDSourceSubmitter    = "job-submitter"
	JobIDSourceStateMachine = "state-machine"
)

func CreatePrintClientRequestId() string {
	return fmt.Sprintf("%s-%s", random.AlphanumericString(randomLength), time.Now().UTC().Format(jobTimeFormat))
}

func CreateJobId(source string) string {
	return fmt.Sprintf("%s-%s-%s-job",
		random.AlphanumericString(randomLength),
		time.Now().UTC().Format(jobTimeFormat),
		toSourceMarker(source),
	)
}

func CreateJobIdFromRequest(reqId, source string) string {
	return fmt.Sprintf("%s-%s-job", reqId, toSourceMarker(source))
}

func toSourceMarker(source string) string {
	switch source {
	case "print-client":
		return "c"
	case "edgenode":
		return "e"
	case "job-submitter":
		return "s"
	case "state-machine":
		return "m"
	}

	return "u"
}
