package httpreq

import (
	"bytes"
	"fmt"
	"net/http"
	"strings"

	"bitbucket.org/papercutsoftware/gopapercut/httputils"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/standardheaders"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/uniqueid"
	"github.com/golang/protobuf/jsonpb"
	"github.com/golang/protobuf/proto"
)

type httpReq struct {
	Actual *http.Request
	Error  error
}

func NewPostProtoJSON(url string, pb proto.Message) *httpReq {
	m := &jsonpb.Marshaler{}
	s, err := m.MarshalToString(pb)
	if err != nil {
		return &httpReq{Error: fmt.Errorf("failed to marshal protobuf message to json: %v. %v", pb, err)}
	}

	r, err := http.NewRequest(http.MethodPost, url, strings.NewReader(s))
	req := &httpReq{
		Actual: r,
		Error:  err,
	}
	return req.jsonContentType()
}

func NewPostProto(url string, pb proto.Message) *httpReq {
	b, err := proto.Marshal(pb)
	if err != nil {
		return &httpReq{Error: fmt.Errorf("failed to marshal protobuf message: %v. %v", pb, err)}
	}

	r, err := http.NewRequest(http.MethodPost, url, bytes.NewReader(b))
	req := &httpReq{
		Actual: r,
		Error:  err,
	}
	return req.protoContentType()
}

func NewGet(url string) *httpReq {
	r, err := http.NewRequest(http.MethodGet, url, nil)
	return &httpReq{
		Actual: r,
		Error:  err,
	}
}

func (r *httpReq) SourceForCorrelationId(source string) *httpReq {
	if r.Actual == nil {
		return r
	}
	correlationId := uniqueid.CreateCorrelationId(source)
	r.Actual.Header.Set(standardheaders.CorrelationId, correlationId)
	return r
}

func (r *httpReq) jsonContentType() *httpReq {
	if r.Actual == nil {
		return r
	}
	r.Actual.Header.Set("Content-Type", "application/json; charset=UTF-8")
	return r
}

func (r *httpReq) protoContentType() *httpReq {
	if r.Actual == nil {
		return r
	}
	r.Actual.Header.Set("Content-Type", "application/x-protobuf; charset=UTF-8")
	return r
}

func (r *httpReq) BearerAuth(token string) *httpReq {
	if r.Actual == nil {
		return r
	}
	r.Actual.Header.Set("Authorization", "Bearer "+token)
	return r
}

func (r *httpReq) Unwrap() (*http.Request, error) {
	return r.Actual, r.Error
}

type HttpClient interface {
	Do(request *http.Request) (resp *http.Response, err error)
}

func RetryHttpClient(c *http.Client, maxAttempts int) HttpClient {
	return &httputils.RetryClient{
		HTTPClient:  c,
		MaxAttempts: maxAttempts,
	}
}
