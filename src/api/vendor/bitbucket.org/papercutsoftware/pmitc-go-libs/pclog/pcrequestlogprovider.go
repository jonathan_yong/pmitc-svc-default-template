/*
 * //Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package pclog

import (
	"context"
	"net/http"
)

/*
 provider is injected into services so that it can create a request scoped logger per request
*/
type RequestLoggerProvider interface {
	NewRequestLogger(ctx context.Context, w http.ResponseWriter, r *http.Request) (RequestLogger, error)
}
