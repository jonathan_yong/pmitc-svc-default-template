package basic

import (
	"context"
	"fmt"
)

const serviceURLTemplate = "https://%s-dot-%s.appspot.com"

type Resolver struct {
}

func (s *Resolver) Get(ctx context.Context, service string) string {
	// FIXME Find a way to get this without depending on appengine package
	project := "pc-pmitc"
	return fmt.Sprintf(serviceURLTemplate, service, project)
}
