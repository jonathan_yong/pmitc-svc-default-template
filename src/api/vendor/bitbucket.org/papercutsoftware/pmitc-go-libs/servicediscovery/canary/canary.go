package canary

import (
	"context"
	"fmt"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/pclog"

	"bitbucket.org/papercutsoftware/pmitc-svc-canary/src/api/canary"
)

const serviceURLTemplate = "%s-dot-%s.appspot.com"

type Resolver struct {
	orgId string
}

func (d *Resolver) Get(ctx context.Context, service string) string {
	// Find a way to get this without being tied to go.19 appengine
	project := "pc-pmitc"
	serviceURL := fmt.Sprintf(serviceURLTemplate, service, project)
	if d.orgId == "" {
		pclog.GetRequestLogger(ctx).Errorf("SetContext (orgId) not set in service discovery, falling back to GA.")
		return "https://" + serviceURL
	}
	client := canary.NewClient().WithContext(ctx)
	version, e := client.ForOrg(d.orgId).GetVersion(service)
	if e != nil {
		pclog.GetRequestLogger(ctx).Errorf("could not get version from canary service, falling back to GA. Err %v", e)
	}
	if version != "" {
		serviceURL = version + "-dot-" + serviceURL
	}
	return "https://" + serviceURL
}

func (d *Resolver) SetContext(ctx context.Context, orgId string) {
	d.orgId = orgId
}
