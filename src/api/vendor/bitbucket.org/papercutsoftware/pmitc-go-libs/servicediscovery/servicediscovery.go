package servicediscovery

import (
	"context"
	"fmt"
	"hash/fnv"
	"math"
	"os"
	"strconv"
	"strings"
	"sync"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/serviceproperties"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/servicediscovery/basic"
)

const (
	MockServerTemplate = "%s_mock_server"
)

// FIXME This is overly complicated, simplify
// Main restriction is avoiding a circular dependency in config -> service discovery
var DefaultServiceDiscovery Resolver = &DiscovererClient{}

type Resolver interface {
	Get(ctx context.Context, service string) string
}

func Get(ctx context.Context, service string) string {
	return DefaultServiceDiscovery.Get(ctx, service)
}

type DiscovererClient struct {
	Resolver Resolver
}

func New(resolver Resolver) *DiscovererClient {
	return &DiscovererClient{
		Resolver: resolver,
	}
}

func (s *DiscovererClient) Get(ctx context.Context, service string) string {
	if testUrl := TestingResolver(service); testUrl != "" {
		return testUrl
	}

	r := s.Resolver
	if r == nil {
		r = &basic.Resolver{}
	}
	return r.Get(ctx, service)
}

func TestingResolver(service string) string {
	if IsLocalServer() {
		mockServerEnvKey := fmt.Sprintf(MockServerTemplate, strings.Replace(service, "-", "_", -1))
		if value, ok := os.LookupEnv(mockServerEnvKey); ok {
			return value
		}
		return "http://localhost:" + GetIntegrationTestPort(service)
	}
	if IsIntegrationTestRunner() {
		return "http://localhost:8080"
	}
	return ""
}

func GetIntegrationTestPort(serviceName string) string {
	// Generate hash based on the id
	h := fnv.New64()
	_, _ = h.Write([]byte(serviceName))
	hashInt := h.Sum64()
	// [20000-30000]
	port := int(hashInt/(math.MaxUint64/10000)) + 20000
	return strconv.Itoa(port)
}

// Pull into separate package?
var once sync.Once
var isLocalServer = false
var isIntegrationTestRunner = false

func IsLocalServer() bool {
	once.Do(readEnv)
	return isLocalServer
}

func IsIntegrationTestRunner() bool {
	once.Do(readEnv)
	return isIntegrationTestRunner
}

// We don't want a dependency on appengine package, so grab internal env vars
func readEnv() {
	serviceProperties := serviceproperties.GetFromEnv()
	isLocalServer = serviceProperties.IsLocalHost
	isIntegrationTestRunner = serviceProperties.IsIntegrationTestRunner
}
