package pubsubutil

import "context"

func NewClient() *client {
	return &client{}
}

type client struct{}

func (c *client) Publish(ctx context.Context, topic string, val interface{}) error {
	return Publish(ctx, topic, val)
}
