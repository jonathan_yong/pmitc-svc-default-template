package protoutil

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/httpreq"

	"github.com/golang/protobuf/jsonpb"

	"github.com/golang/protobuf/proto"
)

// FUTURE: if we ever need large requests we should use io.Pipe()
// Note: protobuf is meant for <1MB
func MarshalWriter(w io.Writer, msg proto.Message) error {
	b, err := proto.Marshal(msg)
	if err != nil {
		return err
	}
	_, _ = w.Write(b)
	return nil
}

func MarshalReader(msg proto.Message) (*bytes.Reader, error) {
	b, err := proto.Marshal(msg)
	if err != nil {
		return nil, err
	}
	return bytes.NewReader(b), nil
}

func UnmarshalReader(r io.Reader, msg proto.Message) error {
	b, err := ioutil.ReadAll(r)
	if err != nil {
		return fmt.Errorf("failed to read from Reader. %v", err)
	}
	err = proto.Unmarshal(b, msg)
	if err != nil {
		return fmt.Errorf("failed to unmarshal bytes into protobuffer. bytes: %s, %v", b, err)
	}
	return nil
}

func GetReq(r *http.Request, pb proto.Message) error {
	switch ct := strings.ToLower(r.Header.Get("Content-Type")); {
	case strings.Contains(ct, "application/json"):
		if err := jsonpb.Unmarshal(r.Body, pb); err != nil {
			return fmt.Errorf("could not unmarshal protobuf as json. %v", err)
		}
	default:
		// Find a way to put warning msg
		fallthrough
	case strings.Contains(ct, "application/x-protobuf"):
		if err := UnmarshalReader(r.Body, pb); err != nil {
			return fmt.Errorf("could not unmarshal protobuf as proto binary. %v", err)
		}
	}
	return nil
}

func GetRes(resp *http.Response, pb proto.Message) error {
	switch ct := strings.ToLower(resp.Header.Get("Content-Type")); {
	case strings.Contains(ct, "application/json"):
		if err := jsonpb.Unmarshal(resp.Body, pb); err != nil {
			return fmt.Errorf("could not marshal protobuf as json. %v", err)
		}
	default:
		// Find a way to put warning msg
		fallthrough
	case strings.Contains(ct, "application/x-protobuf"):
		if err := UnmarshalReader(resp.Body, pb); err != nil {
			return fmt.Errorf("could not marshal protobuf as proto binary. %v", err)
		}
	}
	return nil
}

func SendResp(r *http.Request, w http.ResponseWriter, msg proto.Message) error {
	// Technically we should look at quality score
	// https://github.com/golang/go/issues/19307
	switch a := strings.ToLower(r.Header.Get("Accept")); {
	case strings.Contains(a, "application/json") && !strings.Contains(a, "application/x-protobuf"):
		w.Header().Set("Content-Type", "application/json")
		m := jsonpb.Marshaler{
			Indent: " ",
		}
		if err := m.Marshal(w, msg); err != nil {
			return err
		}
	default:
		// Find a way to put warning msg
		fallthrough
	case strings.Contains(a, "application/x-protobuf"):
		w.Header().Set("Content-Type", "application/x-protobuf")
		if err := MarshalWriter(w, msg); err != nil {
			return err
		}
	}
	return nil
}

var NotOk = errors.New("not 2xx Response")

func DefaultStatusCodeValidator(statusCode int) error {
	if statusCode >= 300 {
		return NotOk
	}
	return nil
}

// We override the request body with the marshaled reqMsg
// Deprecated: Use
func DoProtoRequest(client httpreq.HttpClient, r *http.Request, reqMsg, respMsg proto.Message) error {
	return Do(client, nil, r, reqMsg, respMsg)
}

func DoProto(client httpreq.HttpClient, statusCodeValidator func(statusCode int) error, r *http.Request, reqMsg, respMsg proto.Message) error {
	r.Header.Set("Content-Type", "application/x-protobuf")
	return Do(client, statusCodeValidator, r, reqMsg, respMsg)
}

func DoJSON(client httpreq.HttpClient, statusCodeValidator func(statusCode int) error, r *http.Request, reqMsg, respMsg proto.Message) error {
	r.Header.Set("Content-Type", "application/json")
	return Do(client, statusCodeValidator, r, reqMsg, respMsg)
}

// We override the request body with the marshaled reqMsg
//Deprecated use DoProto or DoJSON depending on the type of your request, no need to set content type headers there
func Do(client httpreq.HttpClient, statusCodeValidator func(statusCode int) error, r *http.Request, reqMsg, respMsg proto.Message) error {
	e := PrepareProtoRequest(r, reqMsg)
	if e != nil {
		return e
	}

	response, e := client.Do(r)
	if response != nil {
		defer response.Body.Close()
	}
	if e != nil {
		return e
	}
	sv := DefaultStatusCodeValidator
	if statusCodeValidator != nil {
		sv = statusCodeValidator
	}
	if err := sv(response.StatusCode); err != nil {
		return fmt.Errorf("got status code %v err: %v", response.StatusCode, err)
	}
	if respMsg != nil {
		switch ct := strings.ToLower(response.Header.Get("Content-Type")); {
		case strings.Contains(ct, "application/json"):
			if err := jsonpb.Unmarshal(response.Body, respMsg); err != nil {
				return fmt.Errorf("could not marshal json protobuf. %v", err)
			}
		case strings.Contains(ct, "application/x-protobuf"):
			if err := UnmarshalReader(response.Body, respMsg); err != nil {
				return fmt.Errorf("could not marshal binary protobuf. %v", err)
			}
		default:
			return fmt.Errorf("we can not proccess this content-type %s", response.Header.Get("Content-Type"))
		}
	}
	return nil
}

//is case we want to execute the request later
func PrepareProtoRequest(r *http.Request, reqMsg proto.Message) error {
	if reqMsg != nil {
		switch rct := strings.ToLower(r.Header.Get("Content-Type")); {
		case strings.Contains(rct, "application/json"):
			m := jsonpb.Marshaler{
				Indent: " ",
			}
			s, e := m.MarshalToString(reqMsg)
			if e != nil {
				return e
			}

			bdy := strings.NewReader(s)
			r.Body = ioutil.NopCloser(bdy)
			r.ContentLength = int64(bdy.Len())
		default:
			r.Header.Set("Content-Type", "application/x-protobuf")
			fallthrough
		case strings.Contains(rct, "application/x-protobuf"):
			bdy, err := MarshalReader(reqMsg)
			if err != nil {
				return err
			}
			r.Body = ioutil.NopCloser(bdy)
			r.ContentLength = int64(bdy.Len())
		}
	}

	if r.Header.Get("Accept") == "" {
		r.Header.Set("Accept", "application/x-protobuf, application/json;q=0.9")
	}
	return nil
}
