/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package clientutils

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/papercutsoftware/gopapercut/httputils"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/authentication/keymanager"
	"google.golang.org/appengine/urlfetch"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/authentication"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/jwt"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/pclog"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/pmitccontext"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/protoutil"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/servicediscovery"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/servicediscovery/canary"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/serviceproperties"
	"github.com/golang/protobuf/proto"
)

const defaultJwtDuration = 30 * time.Minute

type PmitcHttpClient struct {
	ctx                 context.Context
	targetService       string
	jwtExpiryDuration   time.Duration
	async               bool
	httpClient          *http.Client
	customHeaders       map[string]string
	customResolver      servicediscovery.Resolver
	retryAttempts       int
	retryTimeout        time.Duration
	customAuthToken     string
	statusCodeValidator func(statusCode int) error
}

func NewClientHelper(ctx context.Context, targetService string) *PmitcHttpClient {
	customHeaders := make(map[string]string)
	return &PmitcHttpClient{ctx: ctx,
		targetService:     targetService,
		jwtExpiryDuration: defaultJwtDuration,
		async:             false,
		httpClient:        urlfetch.Client(ctx),
		customHeaders:     customHeaders,
		retryAttempts:     3,
		retryTimeout:      1 * time.Second,
	}
}

//NewRequest is used to prepare a generic request with headers that can be manipulated later
func (c *PmitcHttpClient) NewRequest(method, urlTemplate, orgId string, bodyReader io.Reader) (*http.Request, error) {
	url := c.GetOrgAwareURL(urlTemplate, orgId)
	req, err := http.NewRequest(method, url, bodyReader)
	if err != nil {
		return nil, fmt.Errorf("failed to create a request. error: %v", err)
	}

	for k, v := range c.customHeaders {
		req.Header.Set(k, v)
	}

	err = c.SetPmitcRequestHeaders(req, orgId)
	if err != nil {
		return nil, err
	}
	return req, nil
}

func (c *PmitcHttpClient) HttpClient() *http.Client {
	return c.httpClient
}

//Use this if you need more control over the request, ExecuteGetRequest and ExecutePostRequest are way simpler
func (c *PmitcHttpClient) CreateProtoRequest(method, urlTemplate, orgId string, pReq proto.Message) (*http.Request, error) {

	req, err := c.NewRequest(method, urlTemplate, orgId, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create a proto request. error: %v", err)
	}

	req.Header.Set("Content-Type", "application/x-protobuf")
	req.Header.Set("Accept", "application/x-protobuf")

	err = protoutil.PrepareProtoRequest(req, pReq)
	return req, err
}

func (c *PmitcHttpClient) WithJwtDuration(duration time.Duration) *PmitcHttpClient {
	c.jwtExpiryDuration = duration
	return c
}

func (c *PmitcHttpClient) WithCustomAuthToken(token string) *PmitcHttpClient {
	c.customAuthToken = token
	return c
}

func (c *PmitcHttpClient) IsAsync(async bool) *PmitcHttpClient {
	c.async = async
	return c
}

func (c *PmitcHttpClient) WithHttpClient(httpClient *http.Client) *PmitcHttpClient {
	c.httpClient = httpClient
	return c
}

func (c *PmitcHttpClient) WithCustomHeaders(customHeaders map[string]string) *PmitcHttpClient {
	c.customHeaders = customHeaders
	return c
}

func (c *PmitcHttpClient) WithCustomResolver(customResolver servicediscovery.Resolver) *PmitcHttpClient {
	c.customResolver = customResolver
	return c
}

func (c *PmitcHttpClient) WithRetry(maxAttempts int, timeout time.Duration) *PmitcHttpClient {
	c.retryAttempts = maxAttempts
	c.retryTimeout = timeout
	return c
}

func (c *PmitcHttpClient) WithCustomStatusCodeValidator(validator func(statusCode int) error) *PmitcHttpClient {
	c.statusCodeValidator = validator
	return c
}

func (c *PmitcHttpClient) SetPmitcRequestHeaders(req *http.Request, subject string) error {
	keys, err := keymanager.GetKeyList(c.ctx)
	if err != nil {
		return err
	}

	if c.customAuthToken != "" {
		authentication.SetBearer(req, c.customAuthToken)
	} else {
		sp := serviceproperties.GetFromEnv()
		serviceJwt, err := jwt.NewServiceJwt(keys.GetPrimarySecretKey().GetSecret(), sp.ServiceName, c.targetService, subject, time.Now(), c.jwtExpiryDuration)
		if err != nil {
			return fmt.Errorf("failed to create service Jwt. error: %v", err)
		}

		authentication.SetBearer(req, string(serviceJwt))
	}
	pmitccontext.SetHeaders(c.ctx, c.async, req)
	return nil
}

func (c *PmitcHttpClient) CreatePmitcAsyncAttributes(subject string) (map[string]string, error) {
	attrs := make(map[string]string)
	err := c.SetPmitcAsyncAttributes(attrs, subject)
	return attrs, err
}

//for use with cloud task and pubsub
func (c *PmitcHttpClient) SetPmitcAsyncAttributes(attrs map[string]string, subject string) error {
	keys, err := keymanager.GetKeyList(c.ctx)
	if err != nil {
		return err
	}

	sp := serviceproperties.GetFromEnv()
	serviceJwt, err := jwt.NewServiceJwt(keys.GetPrimarySecretKey().GetSecret(),
		sp.ServiceName,
		c.targetService,
		subject, time.Now(), c.jwtExpiryDuration)

	if err != nil {
		return fmt.Errorf("failed to create service Jwt. error: %v", err)
	}

	authentication.SetBearerOnAttributes(attrs, string(serviceJwt))
	pmitccontext.SetPubsubAttributes(c.ctx, attrs)
	return nil
}

func (c *PmitcHttpClient) GetOrgAwareURL(uriTemplate string, orgId string) string {
	var resolver servicediscovery.Resolver
	if c.customResolver != nil {
		resolver = c.customResolver
	} else {
		r := &canary.Resolver{}
		r.SetContext(c.ctx, orgId)
		resolver = r
	}
	d := servicediscovery.New(resolver)
	u := d.Get(c.ctx, c.targetService) + strings.Replace(uriTemplate, "{orgId}", orgId, -1)
	return u
}

type HttpClient interface {
	Do(request *http.Request) (resp *http.Response, err error)
}

func (c *PmitcHttpClient) ExecuteGetRequest(
	orgID,
	urlTemplate string,
	pResp proto.Message,
) error {
	return c.executeRequest(http.MethodGet, orgID, urlTemplate, nil, pResp)
}

func (c *PmitcHttpClient) ExecutePostRequest(
	orgID,
	urlTemplate string,
	pReq,
	pResp proto.Message,
) error {
	return c.executeRequest(http.MethodPost, orgID, urlTemplate, pReq, pResp)
}

func (c *PmitcHttpClient) executeRequest(
	method string,
	orgID,
	urlTemplate string,
	pReq, pResp proto.Message,
) error {

	req, err := c.NewRequest(method, urlTemplate, orgID, nil)
	if err != nil {
		return fmt.Errorf("failed to create a request. error: %v", err)
	}

	if pReq != nil {
		req.Header.Set("Content-Type", "application/x-protobuf")
	}
	if pResp != nil {
		req.Header.Set("Accept", "application/x-protobuf")
	}

	if err = protoutil.Do(c.retryClient(), c.statusCodeValidator, req, pReq, pResp); err != nil {
		return fmt.Errorf("failed to call %s err: %v", req.URL.String(), err)
	}
	return nil
}

func (c *PmitcHttpClient) retryClient() *httputils.RetryClient {
	c.httpClient.Timeout = c.retryTimeout
	attempts := time.Duration(c.retryAttempts)
	//												Add some time for processing
	totalDuration := (attempts * c.retryTimeout) + (attempts * 5 * time.Millisecond)
	return &httputils.RetryClient{
		Timeout:          totalDuration,
		HTTPClient:       c.httpClient,
		MaxAttempts:      c.retryAttempts,
		BackoffDelayFunc: func(_ int) time.Duration { return time.Duration(0) },
		OnResponseFunc: func(resp *http.Response, err error, attempts int) {
			if err != nil {
				pclog.GetRequestLogger(c.ctx).Devf("Failed with %v for attempt %v", err, attempts)
				if resp != nil {
					pclog.GetRequestLogger(c.ctx).Devf("Code %v", resp.StatusCode)
				}
			}
		},
	}
}
