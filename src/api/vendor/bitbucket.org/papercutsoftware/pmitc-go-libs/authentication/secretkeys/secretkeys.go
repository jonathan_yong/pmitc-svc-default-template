/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package secretkeys

import (
	"context"
	"errors"
	"sync"
	"time"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/pclog"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/serviceproperties"
)

const contextKey = "PMITC-SECRET-KEYS"

type SecretKeyList struct {
	secretKeys       []*SecretKey
	primarySecretKey *SecretKey
	// Not really needed yet, but care due to use in singleton
	sync.RWMutex
}

//this is the key that should be used for signing
func (tl *SecretKeyList) GetPrimarySecretKey() *SecretKey {
	tl.RLock()
	defer tl.RUnlock()
	return tl.primarySecretKey
}

//all keys can be used to validate tokens
func (tl *SecretKeyList) GetAllSecretKeys() []*SecretKey {
	tl.RLock()
	defer tl.RUnlock()
	if tl.primarySecretKey != nil && len(tl.primarySecretKey.GetSecret()) > 0 {
		return append([]*SecretKey{tl.primarySecretKey}, tl.secretKeys...)
	}
	return tl.secretKeys
}

type SecretKey struct {
	secret       []byte
	version      string
	creationTime time.Time
	// Not really needed yet, but care due to use in singleton
	sync.RWMutex
}

func (t *SecretKey) GetSecret() []byte {
	t.RLock()
	defer t.RUnlock()
	return t.secret
}
func (t *SecretKey) GetVersion() string {
	t.RLock()
	defer t.RUnlock()
	return t.version
}

func CreateSecretKeyList(secrets []*SecretKey) *SecretKeyList {
	skl := SecretKeyList{}
	skl.primarySecretKey = &SecretKey{}
	skl.secretKeys = secrets
	for _, s := range secrets {
		if skl.primarySecretKey.creationTime.Before(s.creationTime) {
			skl.primarySecretKey = s
		}
	}
	return &skl
}

func CreateSecretKey(secret []byte, version string, creationTime time.Time) *SecretKey {
	return &SecretKey{secret: secret, version: version, creationTime: creationTime}
}

// Deprecated: Use keymanager.GetKeyList
func GetFromContext(ctx context.Context) (SecretKeyList, error) {
	sp := serviceproperties.GetFromEnv()
	value := ctx.Value(contextKey)
	if value == nil {
		if sp.IsLocalHost {
			list, _ := NewTestSecretKeySource().GetSecretKeys(ctx)
			pclog.GetRequestLogger(ctx).Supportf("GENERATING WELL KNOWN NOT SECRET TEST KEY FOR INTEGRATION ENVIRONMENT")
			//nolint
			return *CreateSecretKeyList(list.GetAllSecretKeys()), nil
		}
		return SecretKeyList{}, errors.New("secret keys not found in context")
	}
	//nolint
	return value.(SecretKeyList), nil
}

// Deprecated: Use keymanager.SetKeyList
//nolint
func SetOnContext(ctx context.Context, secretKeys SecretKeyList) context.Context {
	//nolint
	return context.WithValue(ctx, contextKey, secretKeys)
}
