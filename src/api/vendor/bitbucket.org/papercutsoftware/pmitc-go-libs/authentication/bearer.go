/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package authentication

import (
	"fmt"
	"net/http"
	"strings"
)

func ExtractBearer(r *http.Request) string {
	bearerToken := r.Header.Get("Authorization")
	if bearerToken == "" {
		return ""
	}
	return extractToken(bearerToken)
}

func ExtractBearerFromAttributes(attrs map[string]string) string {
	bearerToken := attrs["Authorization"]
	if bearerToken == "" {
		return ""
	}
	return extractToken(bearerToken)
}

func extractToken(bearer string) string {
	return strings.TrimPrefix(bearer, "Bearer ")
}

func SetBearer(r *http.Request, token string) {
	r.Header.Set("Authorization", fmt.Sprintf("Bearer %v", token))
}

func SetBearerOnAttributes(attrs map[string]string, token string) {
	attrs["Authorization"] = fmt.Sprintf("Bearer %v", token)
}
