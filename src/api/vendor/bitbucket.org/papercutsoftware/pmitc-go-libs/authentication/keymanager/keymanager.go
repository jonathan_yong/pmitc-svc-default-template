package keymanager

import (
	"context"
	"fmt"
	"sync"

	"bitbucket.org/papercutsoftware/pmitc-go-libs/authentication/secretkeys"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/pclog"
)

const keyLoadAttempts = 3

var DefaultKeyManager = &keyManager{}
var DefaultKeySource secretkeys.SecretKeySource

func init() {
	// FUTURE try key factory with context.background
	DefaultKeyManager.secretKeyList = &secretkeys.SecretKeyList{}
	DefaultKeySource = secretkeys.GetTestAwareKeySource()
}

func GetKeyList(ctx context.Context) (*secretkeys.SecretKeyList, error) {
	return DefaultKeyManager.GetKeyList(ctx)
}

func SetKeyList(new *secretkeys.SecretKeyList) {
	DefaultKeyManager.SetKeyList(new)
}

func NewKeyManager(keyFactory secretkeys.SecretKeySource) *keyManager {
	return &keyManager{
		keyFactory:    keyFactory,
		secretKeyList: &secretkeys.SecretKeyList{},
	}
}

// FUTURE, might want to disable sets to protect data
type keyManager struct {
	secretKeyList *secretkeys.SecretKeyList
	keyFactory    secretkeys.SecretKeySource
	populateOnce  sync.Once
	sync.RWMutex
}

func (km *keyManager) GetKeyList(ctx context.Context) (*secretkeys.SecretKeyList, error) {
	km.RLock()
	k := km.secretKeyList
	kf := km.keyFactory
	km.RUnlock()
	// Future: periodically get new keys
	if k == nil || len(k.GetAllSecretKeys()) == 0 {
		if kf == nil {
			kf = DefaultKeySource
		}
		if err := km.populateKeyList(ctx, kf); err != nil {
			return &secretkeys.SecretKeyList{}, err
		}
	}
	km.RLock()
	defer km.RUnlock()
	if km.secretKeyList == nil {
		return nil, fmt.Errorf("something went wrong setting up keys, Keylist is nil")
	}
	return km.secretKeyList, nil
}

func (km *keyManager) SetKeyList(new *secretkeys.SecretKeyList) {
	km.Lock()
	defer km.Unlock()
	km.secretKeyList = new
}

func (km *keyManager) SetKeySource(ks secretkeys.SecretKeySource) {
	km.Lock()
	defer km.Unlock()
	km.keyFactory = ks
}

func (km *keyManager) populateKeyList(ctx context.Context, kf secretkeys.SecretKeySource) error {
	logger := pclog.GetRequestLogger(ctx)
	var err error
	km.populateOnce.Do(func() {
		var secretList *secretkeys.SecretKeyList
		for i := 0; i < keyLoadAttempts; i++ {
			logger.Devf("Attempt %d to load keys", i)
			secretList, err = kf.GetSecretKeys(ctx)
			if err == nil {
				logger.Devf("keys loaded successfully")
				break
			} else {
				logger.Errorf("attempt to load keys failed %v", err)
			}
		}
		if err != nil {
			err = fmt.Errorf("could not load keys %v", err)
		}
		km.SetKeyList(secretList)
	})
	return err
}
