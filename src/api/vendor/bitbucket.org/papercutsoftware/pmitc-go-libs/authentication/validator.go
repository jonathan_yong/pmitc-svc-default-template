/*
 * Copyright (c) 2019. PaperCut Software Int. Pty. Ltd.
 */

package authentication

import (
	"context"
	"errors"
	"net/http"
)

var (
	OldSecretKeyFound     = errors.New("an old secret key was used to sign this token")
	NoValidSecretKeyFound = errors.New("the signature of the auth token didn't match any of the secret keys")
	InvalidAudience       = errors.New("token doesn't contain a valid audience for this service")
)

//Validator implementations are intended to authenticate tokens, generally JWTs.
//Properties can also be populated on the context returned. This is useful  for JWTs that contain session info etc.
type Validator interface {
	ValidateRequest(ctx context.Context, r *http.Request, expectedAudience []string) (context.Context, error)
	IsSecure() bool
}
