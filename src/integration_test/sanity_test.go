package integration_test

import (
	"net/http"
	"testing"
)

const defaultServer = "http://localhost:8080"

func TestServerIsRunning_HealthCheck(t *testing.T) {
	resp, err := http.Get(defaultServer + "/_ah/health")

	if err != nil {
		t.Fatal(err)
	}

	if resp.StatusCode != http.StatusOK {
		t.Errorf("Received invalid status code: %v", resp.StatusCode)
	}
}

func TestServerIsRunning_WarmUp(t *testing.T) {
	resp, err := http.Get(defaultServer + "/_ah/warmup")

	if err != nil {
		t.Fatal(err)
	}

	if resp.StatusCode != http.StatusOK {
		t.Errorf("Received invalid status code: %v", resp.StatusCode)
	}
}
