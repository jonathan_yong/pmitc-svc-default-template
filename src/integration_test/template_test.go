package integration_test

import (
	"bitbucket.org/papercutsoftware/pmitc-svc-default-template/src/api/svcdefaulttemplate"
	"context"
	"testing"

	"bitbucket.org/papercutsoftware/pmitc-svc-default-template/src/api/svcdefaulttemplateapi"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/pubsubutil"

	"google.golang.org/appengine/aetest"

	"bitbucket.org/papercutsoftware/gopapercut/random"
)

// Create, Update, Get, Delete, Confirm Delete
func Test_Sanity(t *testing.T) {
	randId := random.AlphanumericString(10)
	orgId := "testOrg" + randId
	dummyData := "abc123"
	modifiedData := dummyData + " modified"
	ctx, done := newContext()
	defer done()

	client := svcdefaulttemplate.NewClient()

	id, e := client.Create(ctx, orgId, dummyData)
	if e != nil {
		t.Fatalf("Could not create %v", e)
	}

	if e := client.Update(ctx, orgId, id, modifiedData); e != nil {
		t.Fatalf("Could not update %v", e)
	}

	msg, e := client.Get(ctx, orgId, id)
	if e != nil {
		t.Fatalf("Could not get %v", e)
	}

	if msg.GetMoreData() != modifiedData {
		t.Fatalf("expected data from service %v to match %v", msg.GetMoreData(), modifiedData)
	}

	if e := client.Delete(ctx, orgId, id); e != nil {
		t.Fatalf("Could not delete %v", e)
	}

	// Confirm Delete
	msg, e = client.Get(ctx, orgId, id)
	if e == nil {
		t.Fatalf("Expected error instead got entity %+v", msg)
	}
}

func Test_Public_GetTime(t *testing.T) {
	ctx, done := newContext()
	defer done()

	time, err := svcdefaulttemplate.NewClient().GetServerTime(ctx)
	if err != nil {
		t.Fatalf("Could not get time %v", err)
	}
	t.Logf("server time is %s", time)
}

func Test_CreateViaPubSub(t *testing.T) {
  t.Skip("Permission issue in authorising a test publish, waiting for james' fix")
	randId := random.AlphanumericString(10)
	orgId := "testOrg" + randId
	id := "id" + randId
	dummyData := "abc123"
	ctx, done := newContext()
	defer done()

	sub := pubsubutil.TestSubscriber(t, "example-create")
	defer sub.Close()

	client := svcdefaulttemplate.NewClient()
	// Could have used pubsubutil TestPublish to fake event if we didn't have client
	e := client.PubSubCreate(ctx, orgId, id, dummyData)
	if e != nil {
		t.Fatalf("Could not create %v", e)
	}

	sub.AssertWaitForMsgs(t, 1)

	msg, e := client.Get(ctx, orgId, id)
	if e != nil {
		t.Fatalf("Could not get %v", e)
	}

	if msg.GetMoreData() != dummyData {
		t.Fatalf("expected data from service %v to match %v", msg.GetMoreData(), dummyData)
	}

	expectedMsg := &svcdefaulttemplateapi.PubSubCreateExampleRequest{}
	sub.GetFirstProtoMsg(t, expectedMsg)

	if msg.GetMoreData() != dummyData {
		t.Fatalf("expected data from service %v to match data from pubsub msg %v", msg.GetMoreData(), expectedMsg.GetMoreData())
	}
}

func Test_GetMulti(t *testing.T) {
	randId := random.AlphanumericString(10)
	orgId := "testOrg" + randId
	firstData := "abc123"
	secondData := "second"
	ctx, done := newContext()
	defer done()

	client := svcdefaulttemplate.NewClient()

	firstId, e := client.Create(ctx, orgId, firstData)
	if e != nil {
		t.Fatalf("Could not create %v", e)
	}

	secondId, e := client.Create(ctx, orgId, secondData)
	if e != nil {
		t.Fatalf("Could not create %v", e)
	}

	pRes, e := client.GetMulti(ctx, orgId)
	if e != nil {
		t.Fatalf("Could not get multi %v", e)
	}

	if len(pRes.GetExamples()) != 2 {
		t.Fatalf("Expected 2 records instead got %v %v", len(pRes.GetExamples()), pRes.GetExamples())
	}

	records := map[string]string{}
	for _, v := range pRes.GetExamples() {
		records[v.GetId()] = v.GetMoreData()
	}

	v, ok := records[firstId]
	t.Logf("first %s %v", v, ok)
	if !ok {
		t.Fatalf("expected first id to exsist %s", firstId)
	}
	if v != firstData {
		t.Fatalf("expected record from server %s to match %s", v, firstData)
	}

	v, ok = records[secondId]
	t.Logf("sec %s %v", v, ok)
	if !ok {
		t.Fatalf("expected second id to exsist %s", secondId)
	}
	if v != secondData {
		t.Fatalf("expected record from server %s to match %s", v, secondData)
	}
}

func newContext() (context.Context, func()) {
	ctx, done, _ := aetest.NewContext()
	//ctx = pmitccontext.Enrich(ctx, map[string]string{standardheaders.CorrelationId: "test-correlation-" + random.AlphanumericString(10)})
	return ctx, done
}
