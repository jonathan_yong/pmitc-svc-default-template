package examplestore

import (
 "bitbucket.org/papercutsoftware/gopapercut/random"
  "bitbucket.org/papercutsoftware/pmitc-go-libs/pclog"
  "cloud.google.com/go/datastore"
  "context"
	"fmt"
)

const exampleKind = "paper-mgmt-example"

type ExampleEntity struct {
	Id       string
	MoreData string
}

type Store interface {
	Save(ctx context.Context, orgId string, entity *ExampleEntity) (string, error)
	SaveById(ctx context.Context, orgId string, entity *ExampleEntity) error
	GetByID(ctx context.Context, orgId string, exampleId string) (*ExampleEntity, error)
	UpdateByID(ctx context.Context, orgId string, exampleId string, updater func(*ExampleEntity) error) (*ExampleEntity, error)
	DeleteByID(ctx context.Context, orgId string, exampleId string) error
	GetByOrgID(ctx context.Context, orgId string) ([]*ExampleEntity, error)
}

type store struct{
}

func NewStore() *store {
	return &store{}
}

// Save will save the entity with a new, unique key
func (s *store) Save(ctx context.Context, orgId string, entity *ExampleEntity) (string, error) {
  logger := pclog.GetRequestLogger(ctx)
	// Find a unique ID for the entity
	exampleId := "example-" + random.AlphanumericString(10)
	key := datastore.NameKey(exampleKind, exampleId, nil)
	key.Namespace = orgId

  client, err := googleresource.DefaultDatastoreClient()
  if err != nil {
    return "nil", err
  }

	for client.Get(ctx, key, &ExampleEntity{}) != datastore.ErrNoSuchEntity {
		exampleId = "example-" + random.AlphanumericString(10)
		key = datastore.NameKey(exampleKind, exampleId, nil)
	}
	entity.Id = exampleId

	logger.Printf( "saving entity %#v", entity)
	_, err = client.Put(ctx, key, entity)
	return exampleId, err
}

func (s *store) SaveById(ctx context.Context, orgId string, entity *ExampleEntity) error {
  logger := pclog.GetRequestLogger(ctx)
	key := datastore.NameKey(exampleKind, entity.Id, nil)

  client, err := googleresource.DefaultDatastoreClient()
  if err != nil {
    return err
  }

	logger.Printf( "saving entity %#v", entity)
	_, err = client.Put(ctx, key, entity)
	return err
}

func (s *store) GetByID(ctx context.Context, orgId, exampleId string) (*ExampleEntity, error) {
	var u ExampleEntity
	key := datastore.NameKey(exampleKind, exampleId, nil)
	key.Namespace = orgId

  client, err := googleresource.DefaultDatastoreClient()
  if err != nil {
    return nil, err
  }

	err = client.Get(ctx, key, &u)
	if err != nil {
		return nil, err
	}
	return &u, nil
}

func (s *store) UpdateByID(ctx context.Context, orgId, exampleId string, updater func(*ExampleEntity) error) (*ExampleEntity, error) {
	var returnValue *ExampleEntity
	transaction := func(tx *datastore.Transaction) error {
		key := datastore.NameKey(exampleKind, exampleId, nil)
		key.Namespace = orgId

		entity := &ExampleEntity{}

		err := tx.Get(key, entity)
		if err != nil {
			return fmt.Errorf("failed to get entity with id %v. error: %v", exampleId, err)
		}

		err = updater(entity)
		if err != nil {
			return fmt.Errorf("failed to run updater function for entity. error: %v", err)
		}

		_, err = tx.Put(key, entity)

		returnValue = entity

		return err
	}

  client, err := googleresource.DefaultDatastoreClient()
  if err != nil {
    return nil, err
  }

	_, err = client.RunInTransaction(ctx, transaction)
	if err != nil {
		return nil, fmt.Errorf("failed to update entity state. error: %v", err)
	}
	return returnValue, nil
}

func (s *store) DeleteByID(ctx context.Context, orgId, exampleId string) error {
  logger := pclog.GetRequestLogger(ctx)
	logger.Printf( "deleting entity %#v", exampleId)

	key := datastore.NameKey(exampleKind, exampleId, nil)
	key.Namespace = orgId

  client, err := googleresource.DefaultDatastoreClient()
  if err != nil {
    return  err
  }

	err := client.Delete(ctx, key)
	return err
}

func (s *store) GetByOrgID(ctx context.Context, orgId string) ([]*ExampleEntity, error) {
	var entities []*ExampleEntity

	q := datastore.NewQuery(exampleKind).Namespace(orgId)

  client, err := googleresource.DefaultDatastoreClient()
  if err != nil {
    return nil, err
  }

	_, err = client.GetAll(ctx, q, &entities)
	if err != nil {
		return nil, err
	}
	return entities, nil
}
