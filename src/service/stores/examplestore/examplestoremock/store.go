package examplestoremock

import (
	"fmt"
   "bitbucket.org/papercutsoftware/pmitc-svc-default-template/src/service/stores/examplestore"

	"golang.org/x/net/context"

	"bitbucket.org/papercutsoftware/gopapercut/random"
)

// NewStore creates a new mock store. This store does NOT use orgs to separate entities.
func NewStore() *store {
	return &store{
		entities: map[string]*examplestore.ExampleEntity{},
	}
}

type store struct {
	entities map[string]*examplestore.ExampleEntity
}

func (s *store) Save(ctx context.Context, orgId string, entity *examplestore.ExampleEntity) (string, error) {
	entityId := "test-entity-" + random.AlphanumericString(10)
	s.entities[entityId] = entity
	return entityId, nil
}

func (s *store) SaveById(ctx context.Context, orgId string, entity *examplestore.ExampleEntity) error {
	s.entities[entity.Id] = entity
	return nil
}

func (s *store) GetByID(ctx context.Context, orgId, exampleId string) (*examplestore.ExampleEntity, error) {
	entity, ok := s.entities[exampleId]
	if ok {
		return entity, nil
	}
	return nil, fmt.Errorf("entity not found")
}

func (s *store) UpdateByID(ctx context.Context, orgId, exampleId string, updater func(*examplestore.ExampleEntity) error) (*examplestore.ExampleEntity, error) {
	entity, ok := s.entities[exampleId]
	if ok {
		err := updater(entity)
		if err == nil {
			return entity, nil
		}
		return nil, err
	}
	return nil, fmt.Errorf("failed to update; entity not found")
}

func (s *store) DeleteByID(ctx context.Context, orgId, exampleId string) error {
	_, ok := s.entities[exampleId]
	if !ok {
		return fmt.Errorf("entity not found")
	}
	delete(s.entities, exampleId)
	return nil
}

func (s *store) GetByOrgID(ctx context.Context, orgId string) ([]*examplestore.ExampleEntity, error) {
	entities := make([]*examplestore.ExampleEntity, 0)
	for _, e := range s.entities {
		entities = append(entities, e)
	}
	return entities, nil
}
