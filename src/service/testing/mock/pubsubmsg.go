package mock

import (
	"github.com/golang/protobuf/proto"
	"golang.org/x/net/context"
)

type MockPubsubMsg struct {
	decodeMsg func(msg proto.Message) error
}


func NewPubsubMsg(decodeMsg func(msg proto.Message) error) *MockPubsubMsg {
	return &MockPubsubMsg{
		decodeMsg: decodeMsg,
	}
}

func (m *MockPubsubMsg) DecodeProtobuf(ctx context.Context, msg proto.Message) error {
	return m.decodeMsg(msg)
}

func (m *MockPubsubMsg) Attributes() map[string]string {
	return make(map[string]string)
}

func (m *MockPubsubMsg) Data() ([]byte, error) {
	return nil, nil
}