package appserver

import (
	"fmt"
	"net/http"

	"golang.org/x/net/context"
	"google.golang.org/appengine"
	"google.golang.org/appengine/aetest"
)

type DevAppServer interface {
	NewContext() (context.Context, error)
	aetest.Instance
}

type devAppServer struct {
	aetest.Instance
}

func NewInstance() (*devAppServer, error) {
	opts := &aetest.Options{AppID: "localhost", StronglyConsistentDatastore: true, SuppressDevAppServerLog: true}
	inst, err := aetest.NewInstance(opts)
	if err != nil {
		return nil, fmt.Errorf("failed to start app instance for testing. %v", err)
	}
	return &devAppServer{inst}, nil
}

func (s *devAppServer) NewContext() (context.Context, error) {
	r, err := s.NewRequest(http.MethodGet, "_path_", nil)
	if err != nil {
		return nil, err
	}
	return appengine.NewContext(r), nil
}
