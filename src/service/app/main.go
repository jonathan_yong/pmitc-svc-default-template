package main

import (
  "bitbucket.org/papercutsoftware/pmitc-go-libs/googleresource"
  "bitbucket.org/papercutsoftware/pmitc-go-libs/authentication/s2sjwtvalidator"
  "context"
  "net/http"

  "bitbucket.org/papercutsoftware/pmitc-svc-default-template/src/service/template"
  "bitbucket.org/papercutsoftware/pmitc-svc-default-template/src/service/stores/examplestore"

  "bitbucket.org/papercutsoftware/pmitc-go-libs/approute"
  "bitbucket.org/papercutsoftware/pmitc-go-libs/pclog/gaelogger"
  "bitbucket.org/papercutsoftware/pmitc-go-libs/pmitccontext"
  "github.com/gorilla/mux"
  "google.golang.org/appengine"
  "log"
)


func main() {
  defer googleresource.Close()

  r := approute.NewAppRouter(mux.NewRouter(),
    pmitccontext.NewDefaultRequestContextProvider(),
    s2sjwtvalidator.NewProvider(),  // standard s2s validator
    gaelogger.NewRequestLoggerProvider(), //appengine logger still used in go1.11. far superior to alternatives
  )

  ctx := context.Background()

  //initialise google client libraries here to prevent performance issues. Always use cloud.google.com/go clients
  //see here: https://github.com/googleapis/google-cloud-go/issues/1295


  store := examplestore.NewStore()

  template.NewAppHandler(store).Route(r)

  http.Handle("/", r)

  appengine.Main()
}
