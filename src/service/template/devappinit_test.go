package template_test

import (
  "io"
  "net/http"
  "os"
  "bitbucket.org/papercutsoftware/pmitc-svc-default-template/src/service/testing/appserver"
  "testing"
)

var devapp appserver.DevAppServer

func TestMain(m *testing.M) {
	inst, err := appserver.NewInstance()
	if err != nil {
    println(err)
    os.Exit(1)
	}
	devapp = inst
  code := m.Run()
  inst.Close()
  os.Exit(code)
}

//func mustGetContext() context.Context {
//	ctx, err := devapp.NewContext()
//	if err != nil {
//		panic(err)
//	}
//	return ctx
//}
//
func newRequest(method, urlStr string, body io.Reader) *http.Request {
	r, err := devapp.NewRequest(method, urlStr, body)
	if err != nil {
		panic(err)
	}
	return r
}
