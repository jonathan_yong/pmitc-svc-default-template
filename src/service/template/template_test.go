package template_test

import (
	"bitbucket.org/papercutsoftware/pmitc-svc-default-template/src/api/svcdefaulttemplateapi"
	"bytes"
	"context"
	"net/http"
	"net/http/httptest"
	"bitbucket.org/papercutsoftware/pmitc-svc-default-template/src/service/template"
	"bitbucket.org/papercutsoftware/pmitc-svc-default-template/src/service/stores/examplestore"
	"bitbucket.org/papercutsoftware/pmitc-svc-default-template/src/service/stores/examplestore/examplestoremock"
	"testing"

	"bitbucket.org/papercutsoftware/gopapercut/random"
	"bitbucket.org/papercutsoftware/pmitc-go-libs/protoutil"
	"github.com/gorilla/mux"
)

const defaultServer = "http://localhost:8080"
const storeEndpoint = "/internal/svc-default-template/org/{orgId}/create-example/v1"
const updateEndpoint = "/internal/svc-default-template/org/{orgId}/update-example/v1"
const GetEndpoint = "/internal/svc-default-template/org/{orgId}/get-example/example/{exampleId}/v1"
const deleteEndpoint = "/internal/svc-default-template/org/{orgId}/delete-example/example/{exampleId}/v1"
const getAllEndpoint = "/internal/svc-default-template/org/{orgId}/get-all/v1"


func TestExample_CreateExample_ExpectOk(t *testing.T) {

  randId := random.AlphanumericString(10)
  testOrgId := "test-org-" + randId

  exampleStore := examplestoremock.NewStore()
  appHandler := template.NewAppHandler(exampleStore)

  preq := &svcdefaulttemplateapi.CreateExampleRequest{
    MoreData: randId,
  }

  buffer := bytes.NewBuffer(nil)
  err := protoutil.MarshalWriter(buffer, preq)
  if err != nil {
    t.Fatalf("failed to marshal proto message")
  }

  url := defaultServer + storeEndpoint
  req := newRequest(http.MethodPost, url, buffer)
  req = mux.SetURLVars(req, map[string]string{"orgId": testOrgId})

  w := httptest.NewRecorder()
  appHandler.CreateExample(req.Context(), testOrgId, w, req)

  res := w.Result()
  if res.StatusCode != http.StatusOK {
    t.Fatalf("expected statusCode is StatusOK but got %v", res.StatusCode)
  }

  storeExampleResponse := &svcdefaulttemplateapi.CreateExampleResponse{}
  if err := protoutil.UnmarshalReader(w.Body, storeExampleResponse); err != nil {
    t.Fatalf("unable to unmarshal response: %v", err)
  }

  if storeExampleResponse.ExampleId == "" {
    t.Fatalf("Expected an id returned, got nothing")
  }

}

func TestExample_CreateExample_ExpectBadRequest(t *testing.T) {

  randId := random.AlphanumericString(10)
  testOrgId := "test-org-" + randId

  exampleStore := examplestoremock.NewStore()
  appHandler := template.NewAppHandler(exampleStore)

  url := defaultServer + storeEndpoint
  req := newRequest(http.MethodPost, url, bytes.NewBuffer([]byte{0, 0}))
  req = mux.SetURLVars(req, map[string]string{"orgId": testOrgId})

  w := httptest.NewRecorder()
  appHandler.CreateExample(req.Context(), testOrgId, w, req)

  res := w.Result()
  if res.StatusCode != http.StatusBadRequest {
    t.Fatalf("expected statusCode is StatusBadRequest but got %v", res.StatusCode)
  }

}

func TestExample_UpdateExample_ExpectOk(t *testing.T) {

  ctx := context.Background()

  randId := random.AlphanumericString(10)
  newRandId := random.AlphanumericString(10)
  testOrgId := "test-org-" + randId

  exampleStore := examplestoremock.NewStore()
  appHandler := template.NewAppHandler(exampleStore)

  exampleId, err := exampleStore.Save(ctx, testOrgId, &examplestore.ExampleEntity{MoreData: randId})
  if err != nil {
    t.Fatalf("failed to seed store value: %v", err)
  }

  preq := &svcdefaulttemplateapi.UpdateExampleRequest{
    ExampleId: exampleId,
    MoreData: newRandId,
  }

  buffer := bytes.NewBuffer(nil)
  err = protoutil.MarshalWriter(buffer, preq)
  if err != nil {
    t.Fatalf("failed to marshal proto message")
  }

  url := defaultServer + updateEndpoint
  req := newRequest(http.MethodPost, url, buffer)
  req = mux.SetURLVars(req, map[string]string{"orgId": testOrgId})

  w := httptest.NewRecorder()
  appHandler.UpdateExample(req.Context(), testOrgId, w, req)

  res := w.Result()
  if res.StatusCode != http.StatusOK {
    t.Fatalf("expected statusCode is StatusOK but got %v", res.StatusCode)
  }

  ex, err := exampleStore.GetByID(ctx, testOrgId, exampleId)
  if err != nil {
    t.Fatalf("failed to retrieve example %s from store: %v", exampleId, err)
  }

  if ex.MoreData != newRandId {
    t.Fatalf("expected MoreData to be %s, but got %s", newRandId, ex.MoreData)
  }

}

func TestExample_UpdateExample_ExpectBadRequest(t *testing.T) {

  ctx := context.Background()

  randId := random.AlphanumericString(10)
  testOrgId := "test-org-" + randId

  exampleStore := examplestoremock.NewStore()
  appHandler := template.NewAppHandler(exampleStore)

  exampleId, err := exampleStore.Save(ctx, testOrgId, &examplestore.ExampleEntity{MoreData: randId})
  if err != nil {
    t.Fatalf("failed to seed store value: %v", err)
  }

  url := defaultServer + updateEndpoint
  req := newRequest(http.MethodPost, url, bytes.NewBuffer([]byte{0, 0}))
  req = mux.SetURLVars(req, map[string]string{"orgId": testOrgId, "exampleId": exampleId})

  w := httptest.NewRecorder()
  appHandler.UpdateExample(req.Context(), testOrgId, w, req)

  res := w.Result()
  if res.StatusCode != http.StatusBadRequest {
    t.Fatalf("expected statusCode is StatusBadRequest but got %v", res.StatusCode)
  }

}

func TestExample_GetExample_ExpectOk(t *testing.T) {

  ctx := context.Background()

  randId := random.AlphanumericString(10)
  testOrgId := "test-org-" + randId

  exampleStore := examplestoremock.NewStore()
  appHandler := template.NewAppHandler(exampleStore)

  exampleId, err := exampleStore.Save(ctx, testOrgId, &examplestore.ExampleEntity{MoreData: randId})
  if err != nil {
    t.Fatalf("failed to seed store value: %v", err)
  }

  url := defaultServer + GetEndpoint
  req := newRequest(http.MethodGet, url, nil)
  req = mux.SetURLVars(req, map[string]string{"orgId": testOrgId, "exampleId": exampleId})

  w := httptest.NewRecorder()
  appHandler.GetExample(req.Context(), testOrgId, w, req)

  res := w.Result()
  if res.StatusCode != http.StatusOK {
    t.Fatalf("expected statusCode is StatusOK but got %v", res.StatusCode)
  }

  getExampleResponse := &svcdefaulttemplateapi.GetExampleResponse{}
  if err := protoutil.UnmarshalReader(w.Body, getExampleResponse); err != nil {
    t.Fatalf("unable to unmarshal response: %v", err)
  }

  if getExampleResponse.MoreData != randId {
    t.Fatalf("Expected %s, got %s", randId, getExampleResponse.MoreData)
  }

}

func TestExample_GetExample_ExpectNotFound(t *testing.T) {

  randId := random.AlphanumericString(10)
  testOrgId := "test-org-" + randId

  exampleStore := examplestoremock.NewStore()
  appHandler := template.NewAppHandler(exampleStore)

  url := defaultServer + GetEndpoint
  req := newRequest(http.MethodPost, url, bytes.NewBuffer([]byte{0, 0}))
  req = mux.SetURLVars(req, map[string]string{"orgId": testOrgId, "exampleId": randId})

  w := httptest.NewRecorder()
  appHandler.GetExample(req.Context(), testOrgId, w, req)

  res := w.Result()
  if res.StatusCode != http.StatusNotFound {
    t.Fatalf("expected statusCode is StatusNotFound but got %v", res.StatusCode)
  }

}

func TestExample_DeleteExample_ExpectOk(t *testing.T) {

  ctx := context.Background()

  randId := random.AlphanumericString(10)
  testOrgId := "test-org-" + randId
  exampleStore := examplestoremock.NewStore()
  appHandler := template.NewAppHandler(exampleStore)

  exampleId, err := exampleStore.Save(ctx, testOrgId, &examplestore.ExampleEntity{MoreData: randId})
  if err != nil {
    t.Fatalf("failed to seed store value: %v", err)
  }

  url := defaultServer + deleteEndpoint
  req := newRequest(http.MethodPost, url, nil)
  req = mux.SetURLVars(req, map[string]string{"orgId": testOrgId, "exampleId": exampleId})

  w := httptest.NewRecorder()
  appHandler.DeleteExample(req.Context(), testOrgId, w, req)

  res := w.Result()
  if res.StatusCode != http.StatusOK {
    t.Fatalf("expected statusCode is StatusOK but got %v", res.StatusCode)
  }

  _, err = exampleStore.GetByID(ctx, testOrgId, exampleId)
  if err == nil {
    t.Fatalf("expected record to not exist in store, but it exists")
  }

}

func TestExample_DeleteExample_ExpectNotFound(t *testing.T) {

  randId := random.AlphanumericString(10)
  testOrgId := "test-org-" + randId

  exampleStore := examplestoremock.NewStore()
  appHandler := template.NewAppHandler(exampleStore)

  url := defaultServer + deleteEndpoint
  req := newRequest(http.MethodPost, url, bytes.NewBuffer([]byte{0, 0}))
  req = mux.SetURLVars(req, map[string]string{"orgId": testOrgId, "exampleId": randId})

  w := httptest.NewRecorder()
  appHandler.DeleteExample(req.Context(), testOrgId, w, req)

  res := w.Result()
  if res.StatusCode != http.StatusNotFound {
    t.Fatalf("expected statusCode is StatusNotFound but got %v", res.StatusCode)
  }

}

func TestExample_GetAll_ExpectOk(t *testing.T) {

  ctx := context.Background()

  randId := random.AlphanumericString(10)
  testOrgId := "test-org-" + randId

  exampleStore := examplestoremock.NewStore()
  appHandler := template.NewAppHandler(exampleStore)

  numEntities := 5

  var err error
  for i := 0; i < numEntities; i++ {
    _, err = exampleStore.Save(ctx, testOrgId, &examplestore.ExampleEntity{MoreData: random.AlphanumericString(10)})
    if err != nil {
      t.Fatalf("failed to seed store value: %v", err)
    }
  }

  url := defaultServer + getAllEndpoint
  req := newRequest(http.MethodGet, url, nil)
  req = mux.SetURLVars(req, map[string]string{"orgId": testOrgId})

  w := httptest.NewRecorder()
  appHandler.GetAllByOrg(req.Context(), testOrgId, w, req)

  res := w.Result()
  if res.StatusCode != http.StatusOK {
    t.Fatalf("expected statusCode is StatusOK but got %v", res.StatusCode)
  }

  getExamplesResponse := &svcdefaulttemplateapi.GetExamplesResponse{}
  if err := protoutil.UnmarshalReader(w.Body, getExamplesResponse); err != nil {
    t.Fatalf("unable to unmarshal response: %v", err)
  }

  if len(getExamplesResponse.Examples) != numEntities {
    t.Fatalf("Expected %d records, got %d", numEntities, len(getExamplesResponse.Examples))
  }

}
