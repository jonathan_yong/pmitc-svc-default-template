package template

import (
  "bitbucket.org/papercutsoftware/pmitc-go-libs/idlogger"
  "bitbucket.org/papercutsoftware/pmitc-go-libs/pmitccontext"
  "bitbucket.org/papercutsoftware/pmitc-go-libs/pubsubutil"
  "net/http"
  "bitbucket.org/papercutsoftware/pmitc-svc-default-template/src/api/svcdefaulttemplateapi"
  "time"

  "context"
  "fmt"
   "bitbucket.org/papercutsoftware/pmitc-svc-default-template/src/service/stores/examplestore"

  "bitbucket.org/papercutsoftware/pmitc-go-libs/approute"
  "bitbucket.org/papercutsoftware/pmitc-go-libs/httperror"
  "bitbucket.org/papercutsoftware/pmitc-go-libs/pclog"
  "bitbucket.org/papercutsoftware/pmitc-go-libs/protoutil"
  "cloud.google.com/go/datastore"
  "github.com/gorilla/mux"
)

func NewAppHandler(
  exampleStore examplestore.Store,
) *appHandler {
return &appHandler{
exampleStore: exampleStore,
}
}

type appHandler struct {
  exampleStore examplestore.Store
}

func (h *appHandler) Route(r *approute.AppRouter) {
  // Public Handler
  r.SimpleGet("/public/svc-default-template/public-get-time-example/v1").Public().SimpleHandlerFunc(h.PublicGetTimeExample)
  // PUB/SUB
  r.Push("/_ah/push-handlers/svc-default-template/pubsubhandler/v1").PubsubHandlerFunc(h.PubsubCreateExample)
  // Private Handlers
  r.Post("/internal/svc-default-template/org/{orgId}/create-example/v1").HandlerFunc(h.CreateExample)
  r.Post("/internal/svc-default-template/org/{orgId}/update-example/v1").HandlerFunc(h.UpdateExample)
  r.Get("/internal/svc-default-template/org/{orgId}/get-all/v1").HandlerFunc(h.GetAllByOrg)
  r.Get("/internal/svc-default-template/org/{orgId}/get-example/example/{exampleId}/v1").HandlerFunc(h.GetExample)
  r.Post("/internal/svc-default-template/org/{orgId}/delete-example/example/{exampleId}/v1").HandlerFunc(h.DeleteExample)
}

//nolint:interfacer
func (h *appHandler) PublicGetTimeExample(ctx context.Context, w http.ResponseWriter, r *http.Request) {
  logger := pclog.GetRequestLogger(ctx)
  idlogger.LogIds(ctx, &idlogger.CommonIds{
    CoordinatorId: []string{pmitccontext.GetValues(ctx).GetCorrelationId()},
  })

  logger.Printf("Start: %v", time.Now().UnixNano())
  _, _ = w.Write([]byte(time.Now().String()))
  logger.Printf("End: %v", time.Now().UnixNano())
}

func (h *appHandler) PubsubCreateExample(ctx context.Context, pubSubMsg pubsubutil.PubsubMessage) (bool, error) {
  logger := pclog.GetRequestLogger(ctx)

  req := &svcdefaulttemplateapi.PubSubCreateExampleRequest{}
  if err := pubSubMsg.DecodeProtobuf(ctx, req); err != nil {
    return false, fmt.Errorf("invalid pubsub message payload %v, err: %v", pubSubMsg, err)
  }

  logger.Printf("Saving data from pubsub %+v", req)

  orgId := req.GetOrgId()
  if orgId == "" {
    err := fmt.Errorf("orgId was not supplied")
    logger.Devf("%s", err.Error())
    return false, err
  }

  idlogger.LogIds(ctx, &idlogger.CommonIds{
    CoordinatorId: []string{pmitccontext.GetValues(ctx).GetCorrelationId()},
    OrgId:         orgId,
  })

  exampleId := req.GetExampleId()
  if orgId == "" {
    err := fmt.Errorf("orgId was not supplied")
    logger.Devf("%s", err.Error())
    return false, err
  }

  moreData := req.GetMoreData()
  if moreData == "" {
    err := fmt.Errorf("moreData was not supplied")
    logger.Devf("%s", err.Error())
    return false, err
  }

  err := h.exampleStore.SaveById(ctx, orgId, &examplestore.ExampleEntity{
    Id:       exampleId,
    MoreData: moreData,
  })
  if err != nil {
    err := fmt.Errorf("could not save payload %v, err: %v", pubSubMsg, err)
    logger.Devf("%s", err.Error())
    return true, err
  }
  return false, nil
}

func (h *appHandler) CreateExample(ctx context.Context, orgId string, w http.ResponseWriter, r *http.Request) {
  logger := pclog.GetRequestLogger(ctx)
  idlogger.LogIds(ctx, &idlogger.CommonIds{
    CoordinatorId: []string{pmitccontext.GetValues(ctx).GetCorrelationId()},
    OrgId:         orgId,
  })

  createExampleRequest := &svcdefaulttemplateapi.CreateExampleRequest{}
  if err := protoutil.GetReq(r, createExampleRequest); err != nil {
    httperror.WriteBadRequest(logger, w, fmt.Sprintf("could not marshal protobuf. %v", err))
    return
  }

  moreData := createExampleRequest.GetMoreData()
  if moreData == "" {
    httperror.WriteBadRequest(logger, w, "moreData was not supplied")
    return
  }

  id, err := h.exampleStore.Save(ctx, orgId, &examplestore.ExampleEntity{MoreData: moreData})
  if err != nil {
    httperror.WriteServerError(logger, w, err.Error())
    return
  }

  res := &svcdefaulttemplateapi.CreateExampleResponse{
    ExampleId: id,
  }

  w.Header().Set("Content-Type", "application/json")

  err = protoutil.SendResp(r, w, res)
  if err != nil {
    httperror.WriteServerError(logger, w, err.Error())
  }

  logger.Printf( "Default example handler, please replace")
  logger.Printf( "stored enitity id: %s for org %s", id, orgId)
}

func (h *appHandler) UpdateExample(ctx context.Context, orgId string, w http.ResponseWriter, r *http.Request) {
  logger := pclog.GetRequestLogger(ctx)
  idlogger.LogIds(ctx, &idlogger.CommonIds{
    CoordinatorId: []string{pmitccontext.GetValues(ctx).GetCorrelationId()},
    OrgId:         orgId,
  })

  updateExampleRequest := &svcdefaulttemplateapi.UpdateExampleRequest{}
  if err := protoutil.GetReq(r, updateExampleRequest); err != nil {
    httperror.WriteBadRequest(logger, w, fmt.Sprintf("could not marshal protobuf. %v", err))
    return
  }

  moreData := updateExampleRequest.GetMoreData()
  if moreData == "" {
    httperror.WriteBadRequest(logger, w, "moreData was not supplied")
    return
  }

  exampleId := updateExampleRequest.GetExampleId()
  if moreData == "" {
    httperror.WriteBadRequest(logger, w, "exampleId was not supplied")
    return
  }

  _, err := h.exampleStore.UpdateByID(ctx, orgId, exampleId, func(exampleEntity *examplestore.ExampleEntity) error {
    exampleEntity.MoreData = moreData
    return nil
  })
  if err != nil {
    httperror.WriteServerError(logger, w, err.Error())
    return
  }

  logger.Printf( "Default example handler, please replace")
  logger.Printf( "updated enitity id: %s for org %s", exampleId, orgId)
}

func (h *appHandler) GetExample(ctx context.Context, orgId string, w http.ResponseWriter, r *http.Request) {
  logger := pclog.GetRequestLogger(ctx)
  idlogger.LogIds(ctx, &idlogger.CommonIds{
    CoordinatorId: []string{pmitccontext.GetValues(ctx).GetCorrelationId()},
    OrgId:         orgId,
  })

  vars := mux.Vars(r)
  exampleId := vars["exampleId"]

  example, err := h.exampleStore.GetByID(ctx, orgId, exampleId)
  if err != nil {
    httperror.WriteNotFound(logger, w, "Not Found")
    return
  }

  res := &svcdefaulttemplateapi.GetExampleResponse{
    MoreData: example.MoreData,
  }

  // Note: Internal we prefer protobuf
  w.Header().Set("Content-Type", "application/json")

  err = protoutil.SendResp(r, w, res)
  if err != nil {
    httperror.WriteServerError(logger, w, err.Error())
  }

  logger.Printf( "Default example handler, please replace")
  logger.Printf( "fetched entity id: %s for org %s", exampleId, orgId)
}

// FUTURE: Consider adding query string to example
func (h *appHandler) GetAllByOrg(ctx context.Context, orgId string, w http.ResponseWriter, r *http.Request) {
  logger := pclog.GetRequestLogger(ctx)
  idlogger.LogIds(ctx, &idlogger.CommonIds{
    CoordinatorId: []string{pmitccontext.GetValues(ctx).GetCorrelationId()},
    OrgId:         orgId,
  })

  examples, err := h.exampleStore.GetByOrgID(ctx, orgId)
  if err != nil && err != datastore.ErrNoSuchEntity {
    httperror.WriteBadRequest(logger, w, fmt.Sprintf("Could not get examples %v", err))
    return
  }

  res := &svcdefaulttemplateapi.GetExamplesResponse{
    Examples: make([]*svcdefaulttemplateapi.GetExamplesResponse_Example, len(examples)),
  }

  for i, example := range examples {
    res.Examples[i] = &svcdefaulttemplateapi.GetExamplesResponse_Example{
      Id:       example.Id,
      MoreData: example.MoreData,
    }
  }
  // Note: Internal we prefer protobuf
  w.Header().Set("Content-Type", "application/json")

  err = protoutil.SendResp(r, w, res)
  if err != nil {
    httperror.WriteServerError(logger, w, err.Error())
  }

  logger.Printf( "Default e entities for orgample handler, please replace")
  logger.Printf( "fetched %d entities for org %s", len(examples), orgId)
}

func (h *appHandler) DeleteExample(ctx context.Context, orgId string, w http.ResponseWriter, r *http.Request) {
  logger := pclog.GetRequestLogger(ctx)
  idlogger.LogIds(ctx, &idlogger.CommonIds{
    CoordinatorId: []string{pmitccontext.GetValues(ctx).GetCorrelationId()},
    OrgId:         orgId,
  })

  vars := mux.Vars(r)
  exampleId := vars["exampleId"]

  err := h.exampleStore.DeleteByID(ctx, orgId, exampleId)
  if err != nil {
    httperror.WriteNotFound(logger, w, "Not Found")
    return
  }

  logger.Printf( "Default example handler, please replace")
  logger.Printf( "deleted entity id: %s for org %s", exampleId, orgId)
}
