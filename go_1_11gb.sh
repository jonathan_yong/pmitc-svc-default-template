#!/usr/bin/env bash

readonly TEST_RESULTS_DIR=${WORKSPACE}/test-results
readonly MAIN_DIR=app

# Import Dependencies
set -e
. ./git-hooks/log-functions
. ./git-hooks/util-functions
. ./git-hooks/fmt-functions
log::init "GO1_11GB.SH"
set +e

# Detect Intelj gopath override an re route tools to tmp file
# FIXME PATH broken
#if [ -z "$_INTELLIJ_FORCE_SET_GOPATH" ]; then
  readonly ORIGINAL_GOPATH=${GOPATH}
#else
#  log::info "Running in Intelj shell redirecting ORIGINAL_GOPATH to ${TMPDIR}go"
#  mkdir -p ${TMPDIR}go
#  readonly ORIGINAL_GOPATH=${TMPDIR}go
#fi

export GOPATH=${WORKSPACE}:${WORKSPACE}/vendor
log::info "GOPATH=$GOPATH"

###############################################################################
# CORE COMMANDS
###############################################################################

usage() {
  echo "Usage: ./make.sh <command> [arguments]

Commands:
usage:               Show this usage information.
generate:            Generate protobufs.
test:                Run unit tests. Arguments:
  - testpattern:     (optional) partial match of test function name(s)
integration-test/it: Run integration tests. Arguments:
  - testpattern:     (optional) partial match of test function name(s)
static-analysis/sa:  Run linting. Add new/n to only report new errors
deploy:              Deploys project to Google App Engine. Arguments:
  - projectid:       (required) ID of the GCP project.
serve:               Serve the application locally.
vendor-update:       Updates the vendors which match the optional name pattern
  - pattern:         (optional) A pattern to specify the packages to be updated
vendor-clean-fetch:  Deletes all packages from vendor and manifest, then refetches them all. This is VERY slow; use cautiously.
"
}

generate() {
    gen_protos
    return 0
}

test() {
    local readonly rawtestname=$1
    local readonly coverresults_dir=${WORKSPACE}/coverage-results
    local readonly unit_test_out=${WORKSPACE}/unitTestOut.txt

    # Locate the package for the test if a test name was provided
    local testpattern=
    local testpackage=
    if [[ "${rawtestname}" != "" ]]; then
        testpattern="-run=^(${rawtestname})$"
        testpackage=$(grep -rl "${rawtestname}" "${WORKSPACE}/src" | xargs dirname | sed -e "s|${WORKSPACE}/src/||")
    fi

    setup_env
    exec_wrapped_function generate
    run_format

    go clean -testcache
    mkdir -p ${coverresults_dir}
    rm -rf ${coverresults_dir}/*

    log::info "Running unit tests..."

    pushd ${WORKSPACE}/src/${modulename} > /dev/null
    go test -v -covermode=atomic -timeout 3m ${testpackage} ${testpattern} -coverprofile=${coverresults_dir}/cover.out ./... 2>&1 | tee ${unit_test_out} | fmt::colorize_test_output
    local readonly test_exit_code=${PIPESTATUS[0]}
    defer "log::info Delete unit test output && rm -v ${unit_test_out}"
    popd > /dev/null

    log::info "unit tests finished"
    defer "log::info Tests finished with code: ${test_exit_code}"
    go tool cover -html=${coverresults_dir}/cover.out -o ${coverresults_dir}/cover.html

    log::info "Writing report"
    mkdir -p ${TEST_RESULTS_DIR}
    cat ${unit_test_out} | go-junit-report > ${TEST_RESULTS_DIR}/unit-report.xml

    # Save the commit hash and the exit code, for use in the pre-push hook
    rm -fv ${UNIT_TEST_STATS}
    echo "export UNIT_TEST_HASH=$(util::hash_project)" > ${UNIT_TEST_STATS}
    echo "export UNIT_TEST_RESULT=${test_exit_code}" >> ${UNIT_TEST_STATS}

    return "${test_exit_code}"
}

integration_test() {
    setup_env
    exec_wrapped_function generate
    run_format

    local readonly integration_test_dir=${WORKSPACE}/src/integration_test

    if [ ! -d ${integration_test_dir} ]; then
        log::info "No Integration Tests found; fake the stat file"
        # When there are no integration tests, write a dummy stat file
        # Save the commit hash and the exit code, for use in the pre-push hook
        echo "export INTEGRATION_TEST_HASH=$(util::hash_project)" > ${INTEGRATION_TEST_STATS}
        echo "export INTEGRATION_TEST_RESULT=0" >> ${INTEGRATION_TEST_STATS}
        return 0
    fi

    source ${WORKSPACE}/startLocalPubsub.sh
    defer 'log::info Stopping pubsub && ${WORKSPACE}/stopLocalPubsub.sh'

    source ${WORKSPACE}/startLocalDatastore.sh
    defer 'log::info Stopping datastore && ${WORKSPACE}/stopLocalDatastore.sh'

    exec_wrapped_function write_app_config "localhost"

    log::info "Starting a service..."
    # Note: The Jenkins machines don't have lsof installed, and because we have
    # the '-e' flag set, the script will faile as the lsof command is not found.
    # We need to find a better way detect a running instance of the application.
    # if [ $(lsof -i :8080 -t | wc -l) -ne 0 ]; then
    #   log::info "An application is already running on port 8080. Run \"kill -9 \$(lsof -i :8080 -t)\" to terminate it"
    #   return 1
    # fi
    dev_appserver.py --clear_datastore=true --datastore_consistency_policy=consistent ${APP_YAML} 2>&1 | fmt::colorize_test_output &
    # Note: It seems like we do not need to manually kill the devappserver, as it's done automatically when this script terminates
    # defer 'log::info Kill service && kill -9 $(lsof -i :8080 -t)'

    # upcheck
    ${ORIGINAL_GOPATH}/bin/upcheck -timeout 15 -urlSource - << ENDURLS
        http://localhost:8080/_ah/health
        http://${PUBSUB_EMULATOR_HOST}/v1/projects/localhost/subscriptions
        http://${DATASTORE_EMULATOR_HOST}/
ENDURLS
    local readonly upcheck_result=$?
    if [ ${upcheck_result} -ne 0 ]; then
        log::info "Service failed to start"
        return ${upcheck_result}
    fi

    create_topics_and_subs

    log::info "Service started"
    log::info "Running integration tests. $1"

    local readonly integration_test_out=${WORKSPACE}/integrationTestOut.txt

    pushd ${integration_test_dir} > /dev/null
    go clean -testcache && INTEGRATION_TEST_RUNNER=true go test --run=$1 -v ./... 2>&1 | tee ${integration_test_out} | fmt::colorize_test_output
    local readonly test_exit_code=${PIPESTATUS[0]}
    defer "log::info Delete unit test output && rm -v ${integration_test_out}"
    popd > /dev/null

    log::info "Integration tests finished"
    defer "log::info Integration tests finished with code: ${test_exit_code}"
    log::info "Writing report"

    mkdir -p ${TEST_RESULTS_DIR}
    cat ${integration_test_out} | go-junit-report > ${TEST_RESULTS_DIR}/integration-report.xml

    # Save the commit hash and the exit code, for use in the pre-push hook
    rm -fv ${INTEGRATION_TEST_STATS}
    echo "export INTEGRATION_TEST_HASH=$(util::hash_project)" > ${INTEGRATION_TEST_STATS}
    echo "export INTEGRATION_TEST_RESULT=${test_exit_code}" >> ${INTEGRATION_TEST_STATS}

    return ${test_exit_code}
}

# Alias for integration-test
it() {
    integration_test $@
}

deploy() {
    local readonly project_id=${1}
    if [[ "${project_id}" == "" ]]; then
        log::info "ERROR: project_id must be supplied as argument"
        return 1
    fi

    setup_env
    exec_wrapped_function generate

    exec_wrapped_function write_app_config ${project_id}
    create_topics_and_subs ${project_id}

    shift
    log::info "Deploying application to the project (${project_id})"
    gcloud beta -q app deploy --project ${project_id} "$@" -v $(git rev-parse --short HEAD) ${APP_YAML}
    if [ "$?" -ne 0 ]; then
        log::error "failed to deploy"
        return 1
    fi
}

promote() {
    local readonly project_id=${1}
    if [[ "${project_id}" == "" ]]; then
        log::error "ERROR: project_id must be supplied as argument"
        return 1
    fi

    if [[ "${serviceName}" == "" ]]; then
        log::error "ERROR: serviceName must be supplied in app.sh to promote canary"
        return 1
    fi

    # NOTE: in future, don't migrate-traffic; we only need to mark the service as PostAlpha
    log::info "Promoting application version $(git rev-parse --short HEAD) to the project (${project_id})"
    set_canary_channel "PostAlpha"
    gcloud app -q --project ${project_id} services set-traffic ${serviceName} --splits $(git rev-parse --short HEAD)=1
    if [ "$?" -ne 0 ]; then
        log::error "failed to deploy"
        return 1
    fi
}


canary_deploy() {
    local readonly project_id=${1}
    if [[ "${project_id}" == "" ]]; then
        log::error "ERROR: project_id must be supplied as argument"
        return 1
    fi

    if [[ "${serviceName}" == "" ]]; then
        log::error "ERROR: serviceName must be supplied in app.sh to deploy canary"
        return 1
    fi

    exec_wrapped_function deploy "$project_id" "--no-promote"
    if [ $? -ne 0 ]; then
        log::error "Failed to deploy"
        exit 1
    fi

    set_canary_orgs
    set_canary_channel "Alpha"
}

alphaOrgs=(
#   Tracer Bullet
    'd984c1d8'
#    Dev Org
    '4725fffa'
#    E2E
    'f1fa9d79'
    '8eeaba5f'
    '43987d42'
)

#  FIXME do this properly maybe small go binary?
set_canary_orgs() {
  log::info "Setting Canary key for Canary.${serviceName} to version $(git rev-parse --short HEAD)"
    for org in "${alphaOrgs[@]}"
    do
    log::info "Setting ${org}"
        curl -d "{ \"Key\": { \"Id\": \"Canary.${serviceName}\", \"OrgId\": \"${org}\"}, \"Value\": \"$(git rev-parse --short HEAD)\"}" -H "Content-Type: application/json" -H "Accept: application/json" -X POST https://config-dot-pc-pmitc.appspot.com/internal/config/set-config/v1
    done
}

set_canary_channel() {
    local channelName="$1"
    if [[ "${channelName}" == "" ]]; then
        log::error "channel name was not provided!"
        exit 1
    fi
    local gitVersion=$(git rev-parse --short HEAD)

    log::info "Setting ${channelName} channel for ${serviceName} to ${gitVersion}"
    curl -d "{ \"channel\": \"${channelName}\", \"serviceName\": \"${serviceName}\", \"version\": \"${gitVersion}\"}" -H "Authorization: Bearer VJ2QY6mWZjhi0fafrekCyIdG0Zm1UNSyvPSfAxqD7ZCqNQ2sq2" -H "Content-Type: application/json" -H "Accept: application/json" -X POST https://test-gateway-dot-pc-pmitc.appspot.com/test/secure/set-channel-version/v1
}

static_analysis() {
  setup_env
  exec_wrapped_function generate

  local analysis_results_dir=${WORKSPACE}/analysis-results
  if [ -z "$CI" ]; then
    GOPATH=${ORIGINAL_GOPATH} go get -u github.com/golangci/golangci-lint/cmd/golangci-lint
    golangci-lint run --skip-dirs vendor $@ src/...
    local analysis_exit_code=$?
    defer "log::info Static analysis finished with code: ${analysis_exit_code}"

    # Save the commit hash and the exit code, for use in the pre-push hook
    rm -fv ${STATIC_ANALYSIS_STATS}
    echo "export STATIC_ANALYSIS_HASH=$(util::hash_project)" > ${STATIC_ANALYSIS_STATS}
    echo "export STATIC_ANALYSIS_RESULT=${analysis_exit_code}" >> ${STATIC_ANALYSIS_STATS}
  else
    curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s latest
    mkdir -p ${analysis_results_dir}
    rm -rf ${analysis_results_dir}/*
#    for now dont fail on static analysis
    ./bin/golangci-lint run --out-format=checkstyle $@ src/... > ${analysis_results_dir}/checkstyle-result.xml || log::warn "THIS LINTING FAILURE WILL FAIL THE BUILD IN THE FUTURE"
  fi
}

# Alias for static-analysis
sa () {
    static_analysis $@
}
static_analysis_new() {
 static_analysis --new-from-rev=HEAD~ $@
}
san() {
  static_analysis_new $@
}

publish() {
    if [ -z "${PROTO_NAME}" ]; then
        log::info "No proto to publish"
    else
        pmitc-proto-publisher -name=${PROTO_NAME} -version=${PROTO_VERSION} -publish
    fi
}

serve() {
    setup_env
    exec_wrapped_function generate

    source ${WORKSPACE}/startLocalDatastore.sh
    defer "log::info Stopping datastore && ${WORKSPACE}/stopLocalDatastore.sh"

    source ${WORKSPACE}/startLocalPubsub.sh
    defer "log::info Stopping pubsub && ${WORKSPACE}/stopLocalPubsub.sh"

    exec_wrapped_function write_app_config "localhost"
    create_topics_and_subs

    dev_appserver.py ${APP_YAML} 2>&1 | fmt::colorize_test_output
}

# After update, we need to copy the go1.9 specific git-hooks
post_update() {
  cp -rf ${LOCAL_REPO_DIR}/generators/app/templates/go9/git-hooks .
  chmod +x ${WORKSPACE}/git-hooks/*
}

###############################################################################
# CUSTOM COMMANDS
###############################################################################

vendor_update() {
    cat vendor/manifest| grep importpath | awk '{print $2}' | sed -e "s|[\",]||g" | grep "$1" | tee /dev/tty | xargs -n 1 -P 20 -I % sh -c 'gb vendor update % || log::error failed to update %'
    gb vendor purge
}

vendor_clean_fetch() {
    mv vendor vendor_old

    log::info "Scanning src directory for packages"
    # Locate packages we should actually vendor based on source code analysis
    local packages=()
    for f in $(find src -name "*.go"); do
        # Read file line by line. For each independent import statement
        # we can simply add the package. For grouped import statements
        # we need to maintain a flag that we are processing the grouped
        # imports.
        local in_import_group="false"
        while read -r line; do
            # Handle stuff inside import groups first
            if [[ "${in_import_group}" == "true" ]]; then
                if [[ "${line}" == "" ]]; then
                    continue
                fi
                if [[ "${line}" == ")" ]]; then
                    in_import_group="false"
                    continue
                fi

                # Only add the package if it's a quoted thing; we need to do this since there could be aliases and stuff
                if echo "${line}" | grep -e '\".*\"' > /dev/null; then
                    packages+=("$(echo "${line}" | grep -o -e '\".*\"')")
                    continue
                fi
            fi
            if [[ "${line}" == "import (" ]]; then
                in_import_group="true"
                continue
            fi

            # Handle independant imports
            if echo "${line}" | grep -e 'import \".*\"' > /dev/null; then
                packages+=("$(echo "${line} | grep -o -e '\".*\"'")")
                continue
            fi
        done < "${f}"
    done

    # Sanitise the packages list by only allowing the ones that are remotely pulled, like bitbucket.org or cloud.google.com.
    # Basically, any package that starts with a domain-like string; we test for the presence of a dot.
    local clean_packages=()
    for p in ${packages[@]}; do
        if ! echo "${p}" | grep -e '^"\w*\.' > /dev/null; then
            # echo "Not a remote package: [${p}]"
            continue
        fi
        # Remove extra double quotes in the package name
        clean_packages+=("${p//\"/}")
    done
    clean_packages=($(echo ${clean_packages[@]} | sort | uniq))

    # Vendor!
    for p in ${clean_packages[@]}; do
        # Check that the package we want to vendor isn't already vendored
        if grep "${p}" vendor/manifest > /dev/null; then
            log::info "Already vendored: ${p}"
            continue
        fi

        log::info "Fetching: ${p}"
        gb vendor fetch ${p}
    done
}

create_topics_and_subs() {
    local readonly project_id=$1

    # Define topics and subscribers like:
    # topic subscriptionName subscriberPushURL
    if [ "${pubsub}" = false ]; then
        return
    fi

    # Get current list of topics and subscriptions up front if running in cloud
    local gcp_topics=
    local gcp_subscriptions=
    if [[ "${project_id}" != "" ]]; then
        gcloud config set project ${project_id}
        gcp_topics=$(gcloud beta pubsub topics list | grep name)
        gcp_subscriptions=$(gcloud beta pubsub subscriptions list | grep name)
    fi


    log::info "creating topics and subscriptions: ${project_id}"

    for topicAndSub in "${topicsAndSubs[@]}"
    do
        sub=(${topicAndSub})

        topic=${sub[0]}
        subscriber=${sub[1]}
        pushURL=${sub[2]}

        log::info
        log::info "- topic      : [${topic}]"
        log::info "- subscriber : [${subscriber}]"
        log::info "- pushURL    : [${pushURL}]"
        log::info

        if [[ "${PUBSUB_EMULATOR_HOST}" != "" ]]; then
            localTopic="${PUBSUB_EMULATOR_HOST}/v1/projects/localhost/topics/${topic}"
            log::info "creating local topic at: ${localTopic}"
            curl -X PUT ${localTopic}
            log::info "Topic [${topic}] created"


            # Only create subscription if subscriber and pushURL are defined
            if [ -n "${subscriber}" ] && [ -n "${pushURL}" ]; then
                localSub="${PUBSUB_EMULATOR_HOST}/v1/projects/localhost/subscriptions/${subscriber}"
                log::info "local subscription request: ${localSub}"
                localPushURL="http://localhost:8080${pushURL}"
                subBody="{\"pushConfig\":{\"pushEndpoint\":\"${localPushURL}\"},\"topic\":\"projects/localhost/topics/${topic}\"}"
                curl -X PUT ${localSub} -H 'Content-Type: application/json' -d ${subBody}

                log::info "Local subscription ${subscriber} created. pushURL: ${localPushURL}"
            fi
        else
            gcloud config set project ${project_id}

            if [ ! $(echo ${gcp_topics} | grep "${topic}") ]; then
                log::info "Creating topic on project ${project_id}"
                gcloud beta pubsub topics create ${topic}
                log::info "Topic [${topic}] created"
            else
                log::info "Topic [${topic}] already exists"
            fi

            # Only create subscription if subscriber and pushURL are defined
            if [ -n "${subscriber}" ] && [ -n "${pushURL}" ]; then
                if [ ! $(echo ${gcp_subscriptions} | grep "${subscriber}") ]; then
                    gcloud beta pubsub subscriptions create ${subscriber} \
                    --topic=${topic} \
                    --push-endpoint=https://${serviceName}-dot-${project_id}.appspot.com${pushURL}
                    --expiration-period=never
                    log::info "Subscription ${subscriber} created. pushURL: ${pushURL}"
                else
                    log::info "Subscription [${subscriber}] already exists"
                fi
            fi
        fi
    done
}

check_deps() {
    # operating system
    log::info "OS: "`uname -a`

    # go version check
    go version
    if [ "$?" -gt 0 ]; then
        log::error "go sdk is required"
        exit 1
    fi

    # gcloud version check
    if ! command -v gcloud > /dev/null; then
        log::error "gcloud is required."
        exit 1
    fi

    log::info "build version: ${BUILD_VERS}"
}

get_deps() {
  GOPATH=${ORIGINAL_GOPATH} go get golang.org/x/tools/cmd/goimports
  GOPATH=${ORIGINAL_GOPATH} go get github.com/golang/protobuf/protoc-gen-go
  GOPATH=${ORIGINAL_GOPATH} go get github.com/jstemmer/go-junit-report
  GOPATH=${ORIGINAL_GOPATH} go get bitbucket.org/aprilayres/upcheck
}


setup_env() {
  log::info "running check and get deps..."
  check_deps
  get_deps
}

write_app_config() {
    local project_id=$1

    if [[ "${project_id}" == "" ]]; then
        log::info "RUNNING LOCAL APP"
        project_id="localhost"
    fi

    local readonly datastore_project_id="${project_id}"

    log::info "Writing application configuration"
    log::info "- PROJECT_ID: ${project_id}"
    log::info "- PMITC_SERVICE_NAME: ${serviceName}"
    log::info "- PMITC_SERVICE_VERSION: ${serviceVersion}"

    if [[ "$PUBSUB_EMULATOR_HOST" == *"localhost"* ]]; then
        log::info "- PUBSUB_EMULATOR_HOST: ${PUBSUB_EMULATOR_HOST}"
    fi

    if [[ "$DATASTORE_EMULATOR_HOST" == *"localhost"* ]]; then
        log::info "- DATASTORE_EMULATOR_HOST: ${DATASTORE_EMULATOR_HOST}"
    fi

    log::info

    local tmp=$(cat ${APP_YAML_TEMPLATE})

    if [[ "$PUBSUB_EMULATOR_HOST" == "" ]]; then
        # Remove emulator host entry
        tmp=$(echo "$tmp" | sed '/PUBSUB_EMULATOR_HOST/d')
    fi

    if [[ "${datastore_project_id}" != "localhost" ]]; then
        # Remove datastore emulator host entries when running on App Engine
        tmp=$(echo "$tmp" | sed '/DATASTORE_EMULATOR_HOST/d')
    fi

    local template=$(echo "$tmp")
    echo "$template" \
        | sed -e "s|@PROJECT_ID@|${project_id}|g" \
        | sed -e "s|@PMITC_SERVICE_NAME@|${serviceName}|g" \
        | sed -e "s|@PMITC_SERVICE_VERSION@|${serviceVersion}|g" \
        | sed -e "s|@DATASTORE_EMULATOR_HOST@|${DATASTORE_EMULATOR_HOST}|g" \
        | sed -e "s|@PUBSUB_EMULATOR_HOST@|${PUBSUB_EMULATOR_HOST}|g" \
        | sed -e "s|@DATASTORE_PROJECT_ID@|${datastore_project_id}|g" \
        | sed -e "s|@MEMORYSTORE_INSTANCE_ID@|${memorystoreInstanceId}|g" \
        | sed -e "s|@PMITC_SERVICE_LOCATION@|${serviceLocation}|g" \
        | sed -e "s|@VPC_CONNECTOR_NAME@|${vpcConnectorName}|g" \
        > ${APP_YAML}

    log::info
    log::info "app.yaml configuration:"
    log::info "---------------------------------------------------"
    cat ${APP_YAML}
    log::info "---------------------------------------------------"
}

run_format() {
    log::info "Running goimports..."
        find ${WORKSPACE}/src -name "*.pb.go" -prune -o -name "*.go" -exec goimports -w {} ";"

    local commentsToFind="Deprecated|FIXME|TODO"
    log::info "Scanning comments (${commentsToFind})..."
    log::info
    find ${WORKSPACE}/src -name "*.pb.go" -prune -o -name "*.go" | xargs egrep --colour=always -n -A 2 -i "${commentsToFind}" | awk '{$1=$1;print}'
    exitCode=$?

    log::info "-----------------------------------------------------"
    if [ ${exitCode} -gt 1 ]; then
        return $?
    elif [ ${exitCode} -eq 1 ]; then
        log::info "- Nothing found."
    else
        log::warn "- Some comments found."
        log::warn "- Please have a look at them before pushing your changes"
    fi
    log::info "-----------------------------------------------------"
}

gen_protos() {
    if [[ ${PROTO_NAME} == "" ]]; then
        log::info "PROTO_NAME not specified, so skipping generation"
        return 0
    fi

    if [[ "${PROTO_IN_DIR}" == "" || "${PROTO_OUT_DIR}" == "" ]]; then
        log::error "PROTO_IN_DIR and PROTO_OUT_DIR must be defined if PROTO_NAME is defined"
        return 1
    fi
    mkdir -p "${PROTO_OUT_DIR}"

    # Run protoc on each file individually. This allows us to have multiple proto files in different packages; in this
    # module, we have 'api' protobuf and 'events' protobuf files. We must process each individually as the protoc
    # compiler for GO requires all the protobuf files passed to it to have the same 'go_package' option; processing
    # the proto files individually bypasses this.
    for proto_file in `find ${PROTO_IN_DIR} -name *.proto`; do
        protoc --go_out=${PROTO_OUT_DIR} \
            --proto_path=${PROTO_IN_DIR} \
            ${proto_file}
    done

    log::info "Generated go struct from proto resources"
}

# Sets up git hooks, if they haven't already been set up
setup_git_hooks() {
    pushd ${WORKSPACE} > /dev/null

    local git_hooks_path=($(find . -maxdepth 1 -name git-hooks))
    if [ ${#git_hooks_path[*]} -ne 1 ]; then
        log::warn "Expected to find 1 git-hooks directory, but didn't"
        return 0
    fi
    # The hooksPath needs to be relative to the repo root.
    local prefix=$(git rev-parse --show-prefix)
    local hooks_path=${prefix}${git_hooks_path[0]}
    git config core.hooksPath "${hooks_path}"
    log::info "Git hooks set to [${hooks_path}]"

    popd > /dev/null
    return 0
}

intljSetup() {
echo $'<application>
    <component name="GoLibraries">
        <option name="urls">
            <list>
                <option value="file://$PROJECT_DIR$" />
                <option value="file://$PROJECT_DIR$/vendor" />
            </list>
        </option>
        <option name="useGoPathFromSystemEnvironment" value="false" />
    </component>
</application>' > ${WORKSPACE}/.idea/goLibraries.xml
}

is () {
    intljSetup $@
}

################################################################################
# EXECUTION
################################################################################

setup_git_hooks

# Minor workaround for 'monorepos' that contains dataflow and 'service' directory
if [ "$modulename" = "" ]; then
    modulename=service
fi

readonly APP_DIR=${WORKSPACE}/src/${modulename}/${MAIN_DIR}
readonly APP_YAML_TEMPLATE=${APP_DIR}/app.yaml.template
readonly APP_YAML=${APP_DIR}/app.yaml

# Vars used to force tests and static-analysis before pushing
readonly UNIT_TEST_STATS=${WORKSPACE}/.pmitc.unittest.stats
readonly INTEGRATION_TEST_STATS=${WORKSPACE}/.pmitc.integrationtest.stats
readonly STATIC_ANALYSIS_STATS=${WORKSPACE}/.pmitc.staticanalysis.stats

# Add .gitignore rule for .pmitc files
if ! grep "\.pmitc*" .gitignore; then
    log::info "Adding .gitignore rules for PMITC build files"
    echo -e "\n.pmitc*" >> .gitignore
fi

function_exists "topics_and_subs" && topics_and_subs || log::info "No topics_and_subs in app.sh"
if [ -z "$topicsAndSubs" ]; then
    log::info "No pub/sub subscriptions set"
else
    pubsub=true
fi
