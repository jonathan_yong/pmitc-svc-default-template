# PMITC svc-default-template service

PMITC user management backend service

- Prod : https://svc-default-template-dot-pc-pmitc.appspot.com

## Endpoints
- HealthCheck (GET): `/_ah/health` (Return JSON statistics on processed/routed requests)

## Commands
 * `./make.sh` will generate protobuf go files and run unit tests
 * `./make.sh serve` will run the local environment
    
    App will run in [http://localhost:8080](http://localhost:8080)
    
    Admin console will run in [http://localhost:8000](http://localhost:8000)
 
 * `./make.sh integration-test` will run integration tests locally
 * `./make.sh deploy pc-pmitc` will deploy to 'pc-pmitc'
