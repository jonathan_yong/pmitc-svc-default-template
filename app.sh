#!/bin/bash

# API for hooks
# $1: projectId, or '' for local

# Returning exit code != 0 will stop script

# Available Hooks (pre/post, you can also override actual function)
# e.g pre_test
# test
# integration_test
# serve
# deploy
# appUsage
# gen_protos
# topics_and_subs <- write to global var 'topicsAndSubs'
# e.g  topicsAndSubs=(
#        'user-invited           generate-link-id-user-invited               /_ah/push-handlers/user-mgmt/generate-link-id'
#        'link-id-generated      construct-invite-email-link-id-generated    /_ah/push-handlers/user-mgmt/construct-invite-email'
#    )

#pre_test(){
#    echo "blah"
#}

projectType='goService'
goRuntime="1.11"
modulename="service"
goModule="bitbucket.org/papercutsoftware/pmitc-svc-svc-default-template"
serviceName="svc-default-template"
serviceVersion="$(git rev-parse --short HEAD)"
PROTO_NAME='pmitc-svc-default-template'
PROTO_VERSION='0.0.1'
PROTO_IN_DIR="${WORKSPACE}/proto"
PROTO_OUT_DIR="${WORKSPACE}/src/api/svcdefaulttemplateapi"

#post_generate() {
    # copy generated protobuffers into vendor directory for client build.
#    local readonly target_dir=${WORKSPACE}/vendor/src/bitbucket.org/papercutsoftware/pmitc-svc-default-template/src/api/svcdefaulttemplateapi
#    mkdir -p ${target_dir}
#    cp -Rv ${PROTO_OUT_DIR}/* ${target_dir}
#}

