#!/usr/bin/env bash

echo
echo Check local pubsub service:
ps -ef|egrep "emulators pubsub|pubsub-emulator"|grep -v grep

ps -ef|egrep "emulators pubsub|pubsub-emulator"|grep -v grep|awk '{print $2}'|xargs kill -9
echo
echo Killed the local pubsub services:
ps -ef|egrep "emulators pubsub|pubsub-emulator"|grep -v grep

export PUBSUB_EMULATOR_HOST=""
