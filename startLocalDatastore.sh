#!/usr/bin/env bash

echo
echo Starting local datastore service:
gcloud beta emulators datastore start --consistency=1.0 --host-port=localhost:19698 &
sleep 2

export DATASTORE_DATASET=localhost
export DATASTORE_EMULATOR_HOST=localhost:19698
export DATASTORE_EMULATOR_HOST_PATH=localhost:19698/datastore
export DATASTORE_HOST=http://localhost:19698
export DATASTORE_PROJECT_ID=localhost
